<?php

// php artisan make:migration create_mod_announcement_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModAnnouncementTable extends Migration
{
    protected $table = 'mod_announcement';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' )->default( 0 );
                $table->integer( 'iMenuId' )->default( 0 );
                $table->integer( 'iType' )->default( 0 );
                $table->string( 'vLang', 255 )->default( "zh-tw" );
                $table->string( 'vTitle', 255 )->nullable();
                $table->string( 'vCss', 255 )->nullable();
                $table->text( 'vImages' )->nullable();
                $table->string( 'vUrl', 255 )->nullable();
                $table->string( 'vSummary', 255 )->nullable();
                $table->longText( 'vDetail' )->nullable();
                $table->integer( 'iDateTime' )->nullable();
                $table->integer( 'iStartTime' );
                $table->integer( 'iEndTime' );
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->integer( 'iRank' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );

            //
            $data_arr = [
                [
                    "iMenuId" => 15,
                    "iType" => 1,
                    "vTitle" => "活動標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/activity-1.jpg",
                    "vUrl" => "",
                    "vSummary" => "雲林縣政府主辦的門口埕有機農村市集，15日於古坑綠色隧道正式開賣，上午十一時舉辦開幕典禮，",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 15,
                    "iType" => 1,
                    "vTitle" => "活動標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/activity-2.jpg",
                    "vUrl" => "",
                    "vSummary" => "雲林縣政府主辦的門口埕有機農村市集，15日於古坑綠色隧道正式開賣，上午十一時舉辦開幕典禮，",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 15,
                    "iType" => 1,
                    "vTitle" => "活動標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/activity-3.jpg",
                    "vUrl" => "",
                    "vSummary" => "雲林縣政府主辦的門口埕有機農村市集，15日於古坑綠色隧道正式開賣，上午十一時舉辦開幕典禮，",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 15,
                    "iType" => 1,
                    "vTitle" => "活動標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/activity-4.jpg",
                    "vUrl" => "",
                    "vSummary" => "桃園伴手禮文創年貨市集重現博愛商圈風華推行綠色隧道正式開賣，上午十一時舉辦開幕典禮，",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 15,
                    "iType" => 1,
                    "vTitle" => "活動標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/activity-5.jpg",
                    "vUrl" => "",
                    "vSummary" => "全民瘋世大運！超過40家新北市伴手禮業者，在新新",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 15,
                    "iType" => 1,
                    "vTitle" => "活動標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/activity-6.jpg",
                    "vUrl" => "",
                    "vSummary" => "十六字級一行最多十二個字十六字級一行最多十二個字",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 16,
                    "iType" => 1,
                    "vTitle" => "文章標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/information-1.jpg",
                    "vUrl" => "",
                    "vSummary" => "中秋節將即，富豐社區為了與民眾分享社區的工藝文化之美，特別推出了中秋月桃禮盒以原木、月桃葉手工作成的禮盒，既可以依個人需求放入物品重複使用，更是兼具環保、實用的商品。",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 16,
                    "iType" => 1,
                    "vTitle" => "文章標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/information-2.jpg",
                    "vUrl" => "",
                    "vSummary" => "中秋企業贈禮首選，手工牛軋餅禮盒，品以純手工製作為主要訴求，儘管製作時耗時費工但是卻堅持提供給顧客香酥且不膩的美味口感。",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 16,
                    "iType" => 1,
                    "vTitle" => "文章標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/information-3.jpg",
                    "vUrl" => "",
                    "vSummary" => "彰化芬園老欉文旦甜又香，限量生產每年都搶購一空以純天然的套袋種植法，且幾乎每一汁",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 16,
                    "iType" => 1,
                    "vTitle" => "文章標題",
                    "vCss" => "",
                    "vImages" => "/portal_assets/htm/img/information-1.jpg",
                    "vUrl" => "",
                    "vSummary" => "這邊十六字級十六字級一行一行最多二十個字這邊十六字級十六字級一行一行最多二十個字",
                    "vDetail" => "",
                ],
                [
                    "iMenuId" => 70012,
                    "iType" => 1,
                    "vLang" => "zh-tw",
                    "vTitle" => "商城介紹",
                    "vCss" => "",
                    "vImages" => "",
                    "vUrl" => "",
                    "vSummary" => "商城介紹",
                    "vDetail" => "商城介紹"
                ],
                [
                    "iMenuId" => 70012,
                    "iType" => 1,
                    "vLang" => "zh-tw",
                    "vTitle" => "商城分會介紹",
                    "vCss" => "",
                    "vImages" => "",
                    "vUrl" => "",
                    "vSummary" => "商城分會介紹",
                    "vDetail" => "商城分會介紹"
                ],
                [
                    "iMenuId" => 70012,
                    "iType" => 1,
                    "vLang" => "zh-tw",
                    "vTitle" => "商城店家介紹",
                    "vCss" => "",
                    "vImages" => "",
                    "vUrl" => "",
                    "vSummary" => "商城店家介紹",
                    "vDetail" => "商城店家介紹"
                ],
                [
                    "iMenuId" => 70012,
                    "iType" => 1,
                    "vLang" => "zh-tw",
                    "vTitle" => "紅利兌換介紹",
                    "vCss" => "",
                    "vImages" => "",
                    "vUrl" => "",
                    "vSummary" => "紅利兌換介紹",
                    "vDetail" => "紅利兌換介紹"
                ],
                [
                    "iMenuId" => 70012,
                    "iType" => 1,
                    "vLang" => "zh-tw",
                    "vTitle" => "飛幣功能與使用功能",
                    "vCss" => "",
                    "vImages" => "",
                    "vUrl" => "",
                    "vSummary" => "飛幣功能與使用功能",
                    "vDetail" => "飛幣功能與使用功能"
                ],
            ];
            foreach ($data_arr as $key => $var) {
                $Dao = new \App\ModAnnouncement();
                $Dao->iMemberId = 1;
                $Dao->iMenuId = $var ['iMenuId'];
                $Dao->iType = $var ['iType'];
                $Dao->vTitle = $var ['vTitle'];
                $Dao->vCss = $var ['vCss'];
                $Dao->vLang = "zh-tw";
                $Dao->vImages = $var ['vImages'];
                $Dao->vUrl = url( $var ['vUrl'] );
                $Dao->vSummary = $var ['vSummary'];
                $Dao->vDetail = $var ['vDetail'];
                $Dao->iDateTime = time();
                $Dao->iStartTime = time();
                $Dao->iEndTime = time() + 180 * 86400 -1;
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iRank = $key + 1;
                $Dao->iStatus = 1;
                $Dao->save();
            }
        } else {
            if ( !Schema::hasColumn( $this->table, 'vImages' )) {
                Schema::table( $this->table, function( $table ) {
                    $table->text( 'vImages' )->change();
                } );
            } else {
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
