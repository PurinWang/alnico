<?php

// php artisan make:migration create_mod_recommend_category_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModRecommendCategoryTable extends Migration
{
    protected $table = 'mod_recommend_category';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMenuId' )->default( 0 );
                $table->integer( 'iMemberId' )->default( 0 );
                $table->integer( 'iType' )->default( 1 ); //首頁
                $table->string( 'vLang', 255 )->default( "zh-tw" );
                $table->integer( 'iCategoryType' )->default( 0 );
                $table->string( 'vCategoryNum', 255 )->nullable();
                $table->string( 'vCategoryName', 255 )->nullable();
                $table->string( 'vImages', 255 )->nullable();
                $table->string( 'vUrl', 255 )->nullable();
                $table->string( 'vNote', 255 )->nullable();
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->integer( 'iRank' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );
            //
            $data_arr = [

            ];

            for($i = 1, $j = 50001; $i <= 12; $i ++, $j ++) {
                $arr = [];
                $arr =
                    [
                        "iType" => $i,
                        "vCategoryNum" => "museum",
                        "vCategoryName" => "分館A" . $i,
                        "iStatus" => 1,
                        "iRank" => 1
                    ];
                array_push($data_arr, $arr);
                for($k = 1; $k <= 1; $k ++) {
                    $arr = [];
                    $arr =
                        //分館 排行專區
                        [
                            "iType" => $j . 4,
                            "vCategoryNum" => "rank",
                            "vCategoryName" => "熱銷排行" . $k,
                            "iRank" => $k
                        ];
                    array_push($data_arr, $arr);
                }
                for($k = 1; $k <= 10; $k ++) {
                    $arr = [];
                    $arr =
                        //分館A1 推薦專區
                        [
                            "iType" => $j . 5,
                            "vCategoryNum" => "recommend",
                            "vCategoryName" => "推薦列表" . $k,
                            "iRank" => $k
                        ];
                    array_push($data_arr, $arr);

                }
            }

            foreach ($data_arr as $key => $var) {
                $Dao = new \App\ModRecommendCategory();
                $Dao->iMemberId = 1;
                $Dao->iType = $var ['iType'];
                $Dao->vLang = "zh-tw";
                $Dao->vCategoryNum = $var ['vCategoryNum'];
                $Dao->vCategoryName = $var ['vCategoryName'];
                $Dao->vImages = isset( $var ['vImages'] ) ? $var ['vImages'] : "";
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iRank = $var ['iRank'];
                $Dao->iStatus = isset( $var ['iStatus'] ) ? $var ['iStatus'] : 1;
                $Dao->save();
            }
        } else {
            if ( Schema::hasTable( $this->table ) && !Schema::hasColumn( $this->table, 'vNote' )) {
                Schema::table( $this->table, function( Blueprint $table ) {
                    $table->string( 'vNote', 255 )->nullable();
                } );
            } else {

            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
