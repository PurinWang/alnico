<?php

// php artisan make:migration create_mod_banner_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModBannerTable extends Migration
{
    protected $table = 'mod_banner';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' )->default( 0 );
                $table->integer( 'iMenuId' )->default( 0 );
                $table->integer( 'iType' )->default( 0 );
                $table->string( 'vLang', 255 )->default( "zh-tw" );
                $table->string( 'vTitle', 255 )->nullable();
                $table->text( 'vImages' )->nullable();
                $table->string( 'vUrl', 255 )->nullable();
                $table->string( 'vNote', 255 )->nullable();
                $table->integer( 'iStartTime' );
                $table->integer( 'iEndTime' );
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->integer( 'iRank' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );

        } else {
            if ( !Schema::hasColumn( $this->table, 'vImages' )) {
                Schema::table( $this->table, function( $table ) {
                    $table->text( 'vImages' )->change();
                } );
            } else {
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
