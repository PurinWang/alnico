<?php

// php artisan make:migration create_sys_setting_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysSettingTable extends Migration
{
    protected $table = 'sys_setting';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iType' )->default( 1 );
                $table->string( 'vLang', 255 )->default( "zh-tw" );
                $table->string( 'vTitle', 255 )->nullable();
                $table->string( 'vUrl', 255 )->nullable();
                $table->string( 'vCss', 255 )->nullable();
                $table->string( 'vImage', 255 )->nullable();
                $table->longText( 'vDetail' )->nullable();
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
            } );

            //
            $data_arr = [
                [
                    "iType" => 11,
                    "vLang" => "zh-tw",
                    "vTitle" => "關於我們",
                    "vUrl" => "",
                    "vDetail" => "關於我們"
                ],
                [
                    "iType" => 21,
                    "vLang" => "zh-tw",
                    "vTitle" => "關於商城",
                    "vUrl" => "",
                    "vDetail" => "關於商城"
                ],
                [
                    "iType" => 22,
                    "vLang" => "zh-tw",
                    "vTitle" => "隱私權政策",
                    "vUrl" => "",
                    "vDetail" => "隱私權政策"
                ],
                [
                    "iType" => 23,
                    "vLang" => "zh-tw",
                    "vTitle" => "服務條款",
                    "vUrl" => "",
                    "vDetail" => "服務條款"
                ],
                [
                    "iType" => 24,
                    "vLang" => "zh-tw",
                    "vTitle" => "新手上路",
                    "vUrl" => "",
                    "vDetail" => "新手上路"
                ],
                [
                    "iType" => 31,
                    "vLang" => "zh-tw",
                    "vTitle" => "FB粉絲專頁",
                    "vUrl" => "",
                    "vDetail" => "FB粉絲專頁"
                ],
                [
                    "iType" => 32,
                    "vLang" => "zh-tw",
                    "vTitle" => "常見問題",
                    "vUrl" => "",
                    "vDetail" => "常見問題"
                ],
                [
                    "iType" => 41,
                    "vLang" => "zh-tw",
                    "vTitle" => "服務電話",
                    "vUrl" => "",
                    "vDetail" => "02-2269-6505"
                ],
                [
                    "iType" => 42,
                    "vLang" => "zh-tw",
                    "vTitle" => "Email",
                    "vUrl" => "",
                    "vDetail" => "service@pin2wall.com"
                ],
                [
                    "iType" => 43,
                    "vLang" => "zh-tw",
                    "vTitle" => "",
                    "vUrl" => "",
                    "vDetail" => "新北市土城區中央路四段51號E棟1樓"
                ],
                [
                    "iType" => 44,
                    "vLang" => "zh-tw",
                    "vTitle" => "",
                    "vUrl" => "",
                    "vDetail" => "EF., No.51, Sec. 4, Zhongyang Rd., Tucheng Dist., New Taipei City 236, Taiwan (R.O.C.)"
                ],
            ];
            foreach ($data_arr as $key => $var) {
                $Dao = new \App\SysSetting();
                $Dao->iType = $var ['iType'];
                $Dao->vLang = $var ['vLang'];
                $Dao->vTitle = $var ['vTitle'];
                $Dao->vUrl = $var ['vUrl'];
                $Dao->vDetail = $var ['vDetail'];
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iStatus = 1;
                $Dao->save();
            }

        } else {
            if ( !Schema::hasColumn( $this->table, 'iId' )) {
                Schema::table( $this->table, function( $table ) {
                    $table->increments( 'iId' );
                } );
            } else {
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
