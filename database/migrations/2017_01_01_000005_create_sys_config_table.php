<?php

// php artisan make:migration create_sys_config_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSysConfigTable extends Migration
{
    protected $table = 'sys_config';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iType' )->default( 1 );
                $table->string( 'vTitle', 255 )->default( '預設' );
                $table->string( 'vField', 255 )->nullable();
                $table->text( 'vValue' )->nullable();
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
            } );

            //
            $data_arr = [
                [
                    "iType" => 171,
                    "vTitle" => "網站名稱",
                    "vField" => "vTitle",
                    "vValue" => "交易中心",
                ],
                [
                    "iType" => 171,
                    "vTitle" => "網站簡介",
                    "vField" => "vDescription",
                    "vValue" => "交易中心",
                ],
                [
                    "iType" => 172,
                    "vTitle" => "網站LOGO",
                    "vField" => "vLogo",
                    "vValue" => asset( "/images/logo.png" ),
                ],
                [
                    "iType" => 172,
                    "vTitle" => "網站圖片",
                    "vField" => "vImage",
                    "vValue" => asset( "/images/logo.png" ),
                ],
                [
                    "iType" => 173,
                    "vTitle" => "GTM",
                    "vField" => "vGTM",
                    "vValue" => "GTM-XXXXX",
                ],
                [
                    "iType" => 173,
                    "vTitle" => "GA",
                    "vField" => "vGA",
                    "vValue" => "UA-XXXXXXX-1",
                ],
                [
                    "iType" => 174,
                    "vTitle" => "FB",
                    "vField" => "vFB",
                    "vValue" => "",
                ],
                [
                    "iType" => 174,
                    "vTitle" => "Line",
                    "vField" => "vLine",
                    "vValue" => "",
                ],
            ];
            foreach ($data_arr as $key => $var) {
                $Dao = new \App\SysConfig();
                $Dao->iType = $var ['iType'];
                $Dao->vTitle = $var ['vTitle'];
                $Dao->vField = $var ['vField'];
                $Dao->vValue = $var ['vValue'];
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iStatus = 1;
                $Dao->save();
            }
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
