<?php
// php artisan make:migration create_sys_member_table
// php artisan migrate
// php artisan migrate:refresh
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SysMember;

class CreateSysMemberTable extends Migration
{
    protected $table = 'sys_member';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->string( 'vAgentCode', 20 );
                $table->integer( 'iUserId' )->unique();
                $table->string( 'vUserCode', 255 )->unique();
                $table->integer( "iAcType" );
                $table->string( 'vAccount', 50 )->unique();
                $table->string( 'vPassword', 255 );
                $table->string( 'vCreateIP', 255 );
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->string( 'vSessionId' )->nullable();
                $table->integer( 'iLoginTime' )->default( 0 );
                $table->tinyInteger( 'bActive' )->default( 0 );
                $table->tinyInteger( 'iStatus' )->default( 0 );
            } );
            $data_arr = [
                [
                    "iAcType" => 1,
                    "vAccount" => "admin@pin2wall.com"
                ],
                [
                    "iAcType" => 2,
                    "vAccount" => "manager@pin2wall.com"
                ],
            ];
            $iUserId = 1000000001;
            foreach ($data_arr as $key => $var) {
                $str = md5( uniqid( mt_rand(), true ) );
                $uuid = substr( $str, 0, 8 ) . '-';
                $uuid .= substr( $str, 8, 4 ) . '-';
                $uuid .= substr( $str, 12, 4 ) . '-';
                $uuid .= substr( $str, 16, 4 ) . '-';
                $uuid .= substr( $str, 20, 12 );
                //
                $Dao = new SysMember ();
                $Dao->vAgentCode = "PTW10001";
                $Dao->iUserId = $iUserId;
                $Dao->vUserCode = $uuid;
                $Dao->iAcType = $var ['iAcType'];
                $Dao->vAccount = $var ['vAccount'];
                $Dao->vPassword = hash( 'sha256', $Dao->vAgentCode . md5( "abc123" ) . $Dao->vUserCode );
                $Dao->vCreateIP = Request::ip();
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->bActive = 1;
                $Dao->iStatus = 1;
                $Dao->save();
                $iUserId++;
            }
        } else {

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
