<?php

// php artisan make:migration create_mod_advertising_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModAdvertisingTable extends Migration
{
    protected $table = 'mod_advertising';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' )->default( 0 );
                $table->integer( 'iType' )->default( 1 ); //
                $table->integer( 'iAdvertisingType' )->default( 1 ); //3.單條Banner 4.雙條Banner
                $table->string( 'vTitle', 255 )->nullable();
                $table->text( 'vImages' )->nullable();
                $table->string( 'vUrl', 255 )->nullable();
                $table->string( 'vSummary', 255 )->nullable();
                $table->longText( 'vDetail' )->nullable();
                $table->integer( 'iStartTime' );
                $table->integer( 'iEndTime' );
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->integer( 'iRank' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );

            //
            $data_arr = [
                [
                    "iType" => 20016,
                    "iAdvertisingType" => 3,
                    "vTitle" => "單條Banner",
                    "vImages" => "/portal_assets/theme/main/img/sw1.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 20016,
                    "iAdvertisingType" => 3,
                    "vTitle" => "單條Banner",
                    "vImages" => "/portal_assets/theme/main/img/sw2.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 20016,
                    "iAdvertisingType" => 4,
                    "vTitle" => "雙條Banner",
                    "vImages" => "/portal_assets/theme/main/img/sw3.jpg;/portal_assets/theme/main/img/sw4.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 20016,
                    "iAdvertisingType" => 4,
                    "vTitle" => "雙條Banner",
                    "vImages" => "/portal_assets/theme/main/img/sw5.jpg;/portal_assets/theme/main/img/sw6.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 30016,
                    "iAdvertisingType" => 3,
                    "vTitle" => "單條Banner",
                    "vImages" => "/portal_assets/theme/main/img/sw1.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 30016,
                    "iAdvertisingType" => 3,
                    "vTitle" => "單條Banner",
                    "vImages" => "/portal_assets/theme/main/img/sw2.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 30016,
                    "iAdvertisingType" => 5,
                    "vTitle" => "Banner",
                    "vImages" => "/portal_assets/theme/matsu/img/ad01.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 30016,
                    "iAdvertisingType" => 5,
                    "vTitle" => "Banner",
                    "vImages" => "/portal_assets/theme/matsu/img/ad02.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 30016,
                    "iAdvertisingType" => 5,
                    "vTitle" => "Banner",
                    "vImages" => "/portal_assets/theme/matsu/img/ad03.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
                [
                    "iType" => 30016,
                    "iAdvertisingType" => 5,
                    "vTitle" => "Banner",
                    "vImages" => "/portal_assets/theme/matsu/img/ad04.jpg",
                    "vUrl" => "",
                    "vSummary" => "",
                    "vDetail" => "",
                ],
            ];
            foreach ($data_arr as $key => $var) {
                $Dao = new \App\ModAdvertising();
                $Dao->iMemberId = 1;
                $Dao->iType = $var ['iType'];
                $Dao->iAdvertisingType = $var ['iAdvertisingType'];
                $Dao->vTitle = $var ['vTitle'];
                $Dao->vImages = $var ['vImages'];
                $Dao->vUrl = url( $var ['vUrl'] );
                $Dao->vSummary = $var ['vSummary'];
                $Dao->vDetail = $var ['vDetail'];
                $Dao->iStartTime = $Dao->iEndTime = time();
                $Dao->iCreateTime = $Dao->iUpdateTime = time();
                $Dao->iRank = $key + 1;
                $Dao->iStatus = 1;
                $Dao->save();
            }
        } else {
            if ( !Schema::hasColumn( $this->table, 'vImages' )) {
                Schema::table( $this->table, function( $table ) {
                    $table->text( 'vImages' )->change();
                } );
            } else {
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
