<?php

// php artisan make:migration create_mod_recommend_product_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModRecommendProductTable extends Migration
{
    protected $table = 'mod_recommend_product';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iMemberId' )->default( 0 );
                $table->integer( 'iCategoryId' )->default( 1 ); //首頁
                $table->integer( 'iProductId' );
                $table->string( 'vTitle', 255 )->nullable();
                $table->string( 'vUrl', 255 )->nullable();
                $table->integer( 'iStartTime' );
                $table->integer( 'iEndTime' );
                $table->integer( 'iClick' )->default( 0 );
                $table->integer( 'iCreateTime' );
                $table->integer( 'iUpdateTime' );
                $table->integer( 'iRank' );
                $table->tinyInteger( 'iStatus' )->default( 0 );
                $table->tinyInteger( 'bDel' )->default( 0 );
            } );
        } else {
            if ( !Schema::hasColumn( $this->table, 'iId' )) {
                Schema::table( $this->table, function( $table ) {
                    $table->increments( 'iId' );
                } );
            } else {
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        if (env( 'DB_REFRESH', false )) {
            Schema::dropIfExists( $this->table );
        }
    }
}
