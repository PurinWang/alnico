<?php
// php artisan make:migration create_sys_menu_table
// php artisan migrate
// php artisan migrate:refresh

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\SysMenu;

class CreateSysMenuTable extends Migration
{
    protected $table = "sys_menu";

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up ()
    {
        if ( !Schema::hasTable( $this->table )) {
            //
            Schema::create( $this->table, function( Blueprint $table ) {
                $table->increments( 'iId' );
                $table->integer( 'iType' )->default( 0 );
                $table->string( 'vName', 255 );
                $table->string( 'vUrl', 255 );
                $table->string( 'vCss', 255 );
                $table->tinyInteger( 'bSubMenu' )->default( 0 );
                $table->integer( 'iParentId' )->default( 0 );
                $table->string( 'vAccess', 255 )->default( '0' );
                $table->tinyInteger( 'bOpen' )->default( 0 );
                $table->integer( 'iRank' )->default( 0 );
            } );
            $data_arr = [
                // admin
                [
                    //管理員功能
                    "iId" => 1,
                    "vName" => "admin",
                    "vUrl" => "",
                    "vCss" => "fa-power-off",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //首頁設置
                                "iId" => 11,
                                "vName" => "admin.setting",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 1,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" =>
                                    [
                                        [
                                            //header
                                            "iId" => 111,
                                            "vName" => "admin.setting.header",
                                            "vUrl" => "web/admin/setting/header",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 11,
                                            "vAccess" => "1,2",
                                            "bOpen" => 0,
                                        ],
                                        [
                                            //footer
                                            "iId" => 112,
                                            "vName" => "admin.setting.footer",
                                            "vUrl" => "web/admin/setting/footer",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 11,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                        [
                                            //information
                                            "iId" => 113,
                                            "vName" => "admin.setting.information",
                                            "vUrl" => "web/admin/setting/information",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 11,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //廣告區塊
                                "iId" => 17,
                                "vName" => "admin.advertising",
                                "vUrl" => "web/admin/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 1,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //滑動廣告區塊
                                "iId" => 18,
                                "vName" => "admin.banner",
                                "vUrl" => "web/admin/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 1,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                        ]
                ],
                [
                    //商城說明
                    "iId" => 7001,
                    "vName" => "intro",
                    "vUrl" => "",
                    "vCss" => "fa-book",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //輪播
                                "iId" => 70011,
                                "vName" => "intro.banner",
                                "vUrl" => "web/intro/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 7001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //商城說明
                                "iId" => 70012,
                                "vName" => "intro.index",
                                "vUrl" => "web/intro/index",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 7001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ]
                        ]
                ],

                /*
                 * 分館管理
                 */
                [
                    //分館A01
                    "iId" => 50001,
                    "vName" => "museum_a01",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500011,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a01/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500012,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000121,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a01/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500012,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500013,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a01/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500014,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a01/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500015,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a01/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500016,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a01/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500017,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50001,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000171,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a01/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500017,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000172,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a01/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500017,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000173,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a01/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500017,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000174,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a01/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500017,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a02
                    "iId" => 50002,
                    "vName" => "museum_a02",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500021,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a02/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500022,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000221,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a02/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500022,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500023,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a02/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500024,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a02/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500025,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a02/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500026,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a02/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500027,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50002,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000271,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a02/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500027,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000272,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a02/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500027,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000273,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a02/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500027,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000274,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a02/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500027,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a03
                    "iId" => 50003,
                    "vName" => "museum_a03",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500031,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a03/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500032,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000321,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a03/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500032,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500033,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a03/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500034,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a03/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500035,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a03/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500036,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a03/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500037,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50003,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000371,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a03/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500037,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000372,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a03/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500037,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000373,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a03/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500037,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000374,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a03/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500037,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a04
                    "iId" => 50004,
                    "vName" => "museum_a04",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500041,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a04/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500042,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000421,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a04/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500042,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500043,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a04/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500044,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a04/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500045,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a04/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500046,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a04/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500047,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50004,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000471,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a04/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500047,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000472,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a04/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500047,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000473,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a04/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500047,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000474,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a04/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500047,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a05
                    "iId" => 50005,
                    "vName" => "museum_a05",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500051,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a05/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500052,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000521,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a05/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500052,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500053,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a05/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500054,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a05/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500055,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a05/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500056,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a05/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500057,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50005,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000571,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a05/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500057,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000572,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a05/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500057,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000573,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a05/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500057,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000574,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a05/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500057,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a06
                    "iId" => 50006,
                    "vName" => "museum_a06",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500061,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a06/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500062,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000621,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a06/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500062,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500063,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a06/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500064,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a06/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500065,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a06/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500066,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a06/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500067,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50006,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000671,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a06/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500067,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000672,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a06/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500067,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000673,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a06/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500067,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000674,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a06/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500067,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a07
                    "iId" => 50007,
                    "vName" => "museum_a07",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500071,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a07/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500072,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000721,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a07/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500072,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500073,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a07/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500074,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a07/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500075,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a07/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500076,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a07/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500077,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50007,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000771,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a07/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500077,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000772,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a07/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500077,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000773,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a07/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500077,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000774,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a07/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500077,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a08
                    "iId" => 50008,
                    "vName" => "museum_a08",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500081,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a08/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500082,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000821,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a08/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500082,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500083,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a08/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500084,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a08/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500085,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a08/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500086,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a08/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500087,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50008,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000871,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a08/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500087,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000872,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a08/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500087,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000873,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a08/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500087,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000874,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a08/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500087,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a09
                    "iId" => 50009,
                    "vName" => "museum_a09",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500091,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a09/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500092,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5000921,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a09/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500092,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500093,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a09/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500094,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a09/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500095,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a09/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500096,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a09/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500097,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50009,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5000971,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a09/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500097,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5000972,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a09/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500097,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5000973,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a09/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500097,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5000974,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a09/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500097,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a10
                    "iId" => 50010,
                    "vName" => "museum_a10",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500101,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a10/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500102,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5001021,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a10/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500102,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500103,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a10/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500104,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a10/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500105,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a10/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500106,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a10/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500107,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50010,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5001071,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a10/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500107,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5001072,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a10/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500107,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5001073,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a10/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500107,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5001074,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a10/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500107,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a11
                    "iId" => 50011,
                    "vName" => "museum_a11",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500111,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a11/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500112,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5001121,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a11/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500112,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500113,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a11/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500114,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a11/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500115,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a11/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500116,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a11/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500117,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50011,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5001171,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a11/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500117,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5001172,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a11/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500117,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5001173,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a11/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500117,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5001174,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a11/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500117,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                [
                    //分館a12
                    "iId" => 50012,
                    "vName" => "museum_a12",
                    "vUrl" => "",
                    "vCss" => "fa-globe",
                    "bSubMenu" => 1,
                    "iParentId" => 0,
                    "vAccess" => "1,2",
                    "bOpen" => 1,
                    "child" =>
                        [
                            [
                                //圖片素材
                                "iId" => 500121,
                                "vName" => "museum.material",
                                "vUrl" => "web/museum_a12/material",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                            ],
                            [
                                //商品庫
                                "iId" => 500122,
                                "vName" => "museum.product",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 0,
                                "child" =>
                                    [
                                        [
                                            //商品
                                            "iId" => 5001221,
                                            "vName" => "museum.product.manage",
                                            "vUrl" => "web/museum_a12/product/manage",
                                            "vCss" => "",
                                            "bSubMenu" => 0,
                                            "iParentId" => 500122,
                                            "vAccess" => "1,2",
                                            "bOpen" => 1,
                                        ],
                                    ]
                            ],
                            [
                                //滑動廣告
                                "iId" => 500123,
                                "vName" => "museum.banner",
                                "vUrl" => "web/museum_a12/banner",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //熱門商品
                                "iId" => 500124,
                                "vName" => "museum.rank",
                                "vUrl" => "web/museum_a12/rank",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //推薦
                                "iId" => 500125,
                                "vName" => "museum.recommend",
                                "vUrl" => "web/museum_a12/recommend",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //廣告Banner區塊
                                "iId" => 500126,
                                "vName" => "museum.advertising",
                                "vUrl" => "web/museum_a12/advertising",
                                "vCss" => "",
                                "bSubMenu" => 0,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                            ],
                            [
                                //綜合廣告區塊
                                "iId" => 500127,
                                "vName" => "museum.advertising2",
                                "vUrl" => "",
                                "vCss" => "",
                                "bSubMenu" => 1,
                                "iParentId" => 50012,
                                "vAccess" => "1,2",
                                "bOpen" => 1,
                                "child" => [
                                    [
                                        //banner1
                                        "iId" => 5001271,
                                        "vName" => "museum.advertising2.banner1",
                                        "vUrl" => "web/museum_a12/advertising2/banner1",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500127,
                                        "vAccess" => "1,2",
                                        "bOpen" => 1,
                                    ],
                                    [
                                        //banner2
                                        "iId" => 5001272,
                                        "vName" => "museum.advertising2.banner2",
                                        "vUrl" => "web/museum_a12/advertising2/banner2",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500127,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner3 slider
                                        "iId" => 5001273,
                                        "vName" => "museum.advertising2.banner3",
                                        "vUrl" => "web/museum_a12/advertising2/banner3",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500127,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                    [
                                        //banner4
                                        "iId" => 5001274,
                                        "vName" => "museum.advertising2.banner4",
                                        "vUrl" => "web/museum_a12/advertising2/banner4",
                                        "vCss" => "",
                                        "bSubMenu" => 0,
                                        "iParentId" => 500127,
                                        "vAccess" => "1,2",
                                        "bOpen" => 0,
                                    ],
                                ]
                            ],
                        ]
                ],
                /*
                 * 分館管理END
                 */
            ];
            $maxRank = 0;
            foreach ($data_arr as $key => $var) {
                $Dao = new SysMenu();
                $Dao->iId = $var ['iId'];
                $Dao->iType = isset( $var ['iType'] ) ? $var ['iType'] : 0;
                $Dao->vName = $var ['vName'];
                $Dao->vUrl = $var ['vUrl'];
                $Dao->vCss = $var ['vCss'];
                $Dao->bSubMenu = $var ['bSubMenu'];
                $Dao->iParentId = $var ['iParentId'];
                $Dao->vAccess = $var ['vAccess'];
                $Dao->bOpen = $var ['bOpen'];
                $Dao->iRank = ( isset( $var ['iRank'] ) ) ? $var ['iRank'] : $maxRank + 1;
                $maxRank++;
                $Dao->save();
                if ($Dao->bSubMenu) {
                    $maxRank_child = 0;
                    foreach ($var['child'] as $key_child => $var_child) {
                        $DaoChild = new SysMenu();
                        $DaoChild->iId = $var_child ['iId'];
                        $DaoChild->iType = isset( $var_child ['iType'] ) ? $var_child ['iType'] : 0;
                        $DaoChild->vName = $var_child ['vName'];
                        $DaoChild->vUrl = $var_child ['vUrl'];
                        $DaoChild->vCss = $var_child ['vCss'];
                        $DaoChild->bSubMenu = $var_child ['bSubMenu'];
                        $DaoChild->iParentId = $var_child ['iParentId'];
                        $DaoChild->vAccess = $var_child ['vAccess'];
                        $DaoChild->bOpen = $var_child ['bOpen'];
                        $DaoChild->iRank = ( isset( $var_child ['iRank'] ) ) ? $var_child ['iRank'] : $maxRank_child + 1;
                        $DaoChild->save();
                        $maxRank_child++;
                        if ($DaoChild->bSubMenu) {
                            $maxRank_child2 = 0;
                            foreach ($var_child['child'] as $key_child2 => $var_child2) {
                                $DaoChild2 = new SysMenu ();
                                $DaoChild2->iId = $var_child2 ['iId'];
                                $DaoChild2->iType = isset( $var_child2 ['iType'] ) ? $var_child2 ['iType'] : 0;
                                $DaoChild2->vName = $var_child2 ['vName'];
                                $DaoChild2->vUrl = $var_child2 ['vUrl'];
                                $DaoChild2->vCss = $var_child2 ['vCss'];
                                $DaoChild2->bSubMenu = $var_child2 ['bSubMenu'];
                                $DaoChild2->iParentId = $var_child2 ['iParentId'];
                                $DaoChild2->vAccess = $var_child2 ['vAccess'];
                                $DaoChild2->bOpen = $var_child2 ['bOpen'];
                                $DaoChild2->iRank = ( isset( $var_child2 ['iRank'] ) ) ? $var_child2 ['iRank'] : $maxRank_child2 + 1;
                                $DaoChild2->save();
                                $maxRank_child2++;
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down ()
    {
        //
        Schema::dropIfExists( $this->table );
    }
}
