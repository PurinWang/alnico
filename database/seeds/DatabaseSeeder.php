<?php
// php artisan db:seed
// php artisan db:seed --class SysMenuTableSeeder

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SysMenuTableSeeder::class);
        $this->call(ModBannerTableSeeder::class);
    }
}
