<?php
// php artisan db:seed
// php artisan db:seed --class ModBannerTableSeeder

use Illuminate\Database\Seeder;
use App\SysMenu;

class ModBannerTableSeeder extends Seeder
{
    protected $table = 'mod_banner';

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run ()
    {
        //
        $data_arr = [
            [
                "iMenuId" => 17,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/add-1.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 17,
                "iType" => 4,
                "vImages" => "/portal_assets/htm/img/add-2.jpg;/portal_assets/htm/img/add-3.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 17,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/add-4.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 18,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/scorll-1989X799-001.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 18,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/scorll-1989X799-002.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 70011,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/slider-img.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 70011,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/slider-img.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],
            [
                "iMenuId" => 70011,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/slider-img.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ],

        ];

        for ($i = 1, $j = 50001 ; $i <= 12 ; $i++, $j++) {
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/01.jpg",
                "vTitle" => "全部類別",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/02.jpg",
                "vTitle" => "居家生活",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/03.jpg",
                "vTitle" => "美妝保健",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/04.jpg",
                "vTitle" => "運動用品",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/05.jpg",
                "vTitle" => "小農鮮乳",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/04.jpg",
                "vTitle" => "特色美食",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/03.jpg",
                "vTitle" => "特色美食",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 3,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/prodCateView/02.jpg",
                "vTitle" => "醉釀酒品",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 6,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/add-3.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
            $arr = [
                "iMenuId" => $j . 6,
                "iType" => 3,
                "vImages" => "/portal_assets/htm/img/add-4.jpg",
                "vTitle" => "",
                "vUrl" => "",
                "vNote" => "",
            ];
            array_push( $data_arr, $arr );
        }

        DB::table( $this->table )->delete();

        foreach ($data_arr as $key => $var) {
            $Dao = new \App\ModBanner();
            $Dao->iMemberId = 1;
            $Dao->iMenuId = $var ['iMenuId'];
            $Dao->iType = $var ['iType'];
            $Dao->vTitle = $var ['vTitle'];
            $Dao->vLang = "zh-tw";
            $Dao->vImages = $var ['vImages'];
            $Dao->vUrl = url( $var ['vUrl'] );
            $Dao->vNote = $var ['vNote'];
            $Dao->iStartTime = time();
            $Dao->iEndTime = time() + 180 * 86400 - 1;
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->iRank = $key + 1;
            $Dao->iStatus = 1;
            $Dao->save();
        }
    }
}
