$(function() {
    var hash = location.hash;
    $('.nav-tabs a[href="'+hash+'"]').tab('show');

    $("#inputUpload").fileinput({
        showPreview: false,
        showUpload: false,
        elErrorContainer: '#kartik-file-errors',
        allowedFileExtensions: ["jpg", "png", "gif"]
        //uploadUrl: '/site/file-upload-single'
    });
})

function copyFn() {
    var e = document.getElementById("addrInput");
    e.select();
    document.execCommand("Copy");
}