var startDate = new Date();
var endDateAry = ["4/20","4/25","4/30","5/10","5/20","5/31","6/15","6/30"];
var currentTarget = 0;
var focusDateNum = 0;
var dateNum;
var lockBoo = true;

function init() {
    var endDate = new Date('2018/4/27 00:00');
    var spantime = (endDate - startDate) / 1000;

    $(this).everyTime('1s', function(i) {
        spantime--;

        var d = Math.floor(spantime / (24 * 3600));
        var h = Math.floor((spantime % (24 * 3600)) / 3600);
        var m = Math.floor((spantime % 3600) / (60));
        var s = Math.floor(spantime % 60);

        if (spantime > 0) {
            $('.sDay').text(d);
            $('.sHour').text(h);
            $('.sMin').text(m);
            $('.sSec').text(s);
        } else { // 避免倒數變成負的
            $('.sDay').text(0);
            $('.sHour').text(0);
            $('.sMin').text(0);
            $('.sSec').text(0);
        }
    });

    $('#secF .items.focus .status').on('click',function(){
        console.log('test');
    })

    getActiveTarget(endDateAry);    
}
init();

function getActiveTarget(ary){    
    for (var i = 0; i < ary.length; i++) {
        var activeDate = new Date('2018/'+ary[i]+' 23:59');
        var num = (activeDate - startDate) / 1000
        dateNum = Math.floor(num / (24 * 3600));

        if(dateNum<0){
            $('#secF .items:nth-child('+(i+1)+')').css("opacity","0.5");
            $('#secF .items:nth-child('+(i+1)+') .time').html('Sold out');

            currentTarget++;
        }else if(lockBoo){
            focusDateNum = num;
            lockBoo = false;
        }
    }
    $('#secF .items:nth-child('+(currentTarget+1)+')').addClass('focus');

    $(this).everyTime('1s', function(i) {
        focusDateNum--

        var d = Math.floor(focusDateNum / (24 * 3600));
        var h = Math.floor((focusDateNum % (24 * 3600)) / 3600);
        var m = Math.floor((focusDateNum % 3600) / (60));
        var s = Math.floor(focusDateNum % 60);

        $('#secF .items:nth-child('+(currentTarget+1)+') .time').html(d+"："+h+"："+m+"："+s);
    });
}