<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(
    [
        'namespace' => '_Portal',
        'middleware' => [ 'CheckLang' ]
    ], function() {
    Route::get( '', 'IndexController@index' );
    //Route::get( 'invest', 'InvestController@index' )->middleware( [ 'CheckICODateTime' ] );
    Route::get( 'invest', 'IndexController@index' );
    Route::get( 'invest_create', 'IndexController@index' );
    Route::post( 'doInvest', 'IndexController@index' );
    Route::get( 'invest_pay/{invest_num}', 'IndexController@index' );
    //
//    Route::get( 'invest', 'InvestController@create' )->middleware( [ 'CheckICODateTime', 'CheckMallLogin' ] );
//    Route::get( 'invest_create', 'InvestController@create' )->middleware( [ 'CheckICODateTime', 'CheckMallLogin' ] );
//    Route::post( 'doInvest', 'InvestController@doInvest' )->middleware( 'CheckICODateTime', 'CheckMallLogin' );
//    Route::get( 'invest_pay/{invest_num}', 'InvestController@invest_pay' )->middleware( 'CheckICODateTime', 'CheckMallLogin' );
    Route::get( 'report', 'ReportController@index' );
    Route::get( 'articles', 'IndexController@articles' );
    Route::get( 'events', 'IndexController@events' );
    Route::get( 'login', 'LoginController@index' );
    Route::post( 'doLogin', 'LoginController@doLogin' );
    Route::get( 'coin_airdrops', 'CoinAirdropsController@index' );
    //
    Route::get( 'forgotpassword', 'LoginController@forgotpassword' );
    Route::post( 'doSendVerification', 'LoginController@doSendVerification' );
    Route::post( 'doResetPassword', 'LoginController@doResetPassword' );
    //
    Route::get( 'register', 'RegisterController@index' );
    Route::post( 'doRegister', 'RegisterController@doRegister' );
    Route::get( 'doActive/{usercode}', 'RegisterController@doActive' );
    //
    Route::get( 'logout', 'LogoutController@index' );
    Route::post( 'doLogout', 'LogoutController@doLogout' );
    Route::any( 'doSetLocale/{locale}', 'LocaleController@doSetLocale' );
    //
    Route::post( 'doAddEmail', 'IndexController@doAddEmail' );
    //
    Route::get( 'feedback', 'FeedbackController@index' );
    Route::post( 'doSendFeedback', 'FeedbackController@doSendFeedback' );
    //
    Route::group(
        [
            'middleware' => 'CheckMallLogin'
        ], function() {
        Route::post( 'upload_image', 'UploadController@doUploadImage' );
        Route::post( 'upload_image_base64', 'UploadController@doUploadImageBase64' );
    } );
    /********************************
     * 新聞公告
     ********************************/
    Route::group(
        [
            'prefix' => 'news',
            'namespace' => 'News'
        ], function() {
        Route::get( '', 'IndexController@index' );
        Route::get( 'detail/{code}', 'IndexController@detail' );
    } );
    /*********************************
     * 會員中心
     **********************************/
    Route::group(
        [
            'middleware' => 'CheckMallLogin',
            'prefix' => 'member',
            'namespace' => 'Member'
        ], function() {
        Route::get( '', 'InformationController@index' );
        Route::get( 'edit', 'InformationController@edit' );
        Route::post( 'dosave', 'InformationController@doSave' );
        Route::post( 'dosavepassword', 'InformationController@doSavePassword' );
        Route::post( 'dosaveinvest', 'InformationController@doSaveInvest' );
        Route::post( 'doaddmessage', 'InformationController@doAddMessage' );
        Route::get( '{error}', function() {
            return redirect( 'member' );
        } );
    } );
    Route::group(
        [
            'middleware' => 'CheckMallLogin',
            'prefix' => 'coin_airdrops',
            'namespace' => 'CoinAirdrops'
        ], function() {
        Route::post( 'dosend', 'IndexController@doSend' );
        Route::get( '{error}', function() {
            return redirect( 'member' );
        } );
    } );
    Route::get( 'R{userid}', function() {

    } )->middleware( 'CheckCoinAirdrops' );

    Route::any( '{error}', function() {
        return redirect( '' );
    } );
} );
