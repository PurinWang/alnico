@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/payment.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        @if($info->vType == "ETH")
            <div class="section" id="sec1">
                <div class="container">
                    <div class="articleCnt">
                        <div class="title">{{trans('invest_pay.title')}}</div>
                        <div class="todayVal">{{$info->iCount}} ETH = {{$info->iTotal}} ALLN</div>
                        <div class="allnVal">{{trans('invest_pay.allnVal')}}</div>
                        <br>
                        <div class="thumbnail">
                            <img src="/portal_assets/htm/img/ETH_qrcode.png">
                        </div>
                        <div class="titleS">{{trans('invest_pay.titleS')}}：</div>
                        <div class="input-group">
                            <input type="text" class="form-control" id="addrInput" placeholder="" value="0xe3019bfadfb41ed6c579e7dad4603b716d72082d">
                            <span class="input-group-btn">
                                <button class="btn copyBtn" type="button" onclick="copyFn();">copy</button>
                            </span>
                        </div>
                        <div class="note">
                            <div class="text">{{trans('invest_pay.note')}}：<br>
                                <ol class="text-left">
                                    <li>{{trans('invest_pay.note_1')}}</li>
                                    <li>{{trans('invest_pay.note_2')}}</li>
                                    <li>{{trans('invest_pay.note_3')}}</li>
                                    <li>{{trans('invest_pay.note_4')}}</li>
                                </ol>
                            </div>
                        </div>
                        <a href="{{url('member')}}#p2" class="btn sendBtn">{{trans('invest_pay.sendBtn')}}</a>
                    </div>
                </div>
            </div>
        @endif
        @if($info->vType == "BTC")
            <div class="section" id="sec1">
                <div class="container">
                    <div class="articleCnt">
                        <div class="title">{{trans('invest_pay.title')}}</div>
                        <div class="todayVal">{{$info->iCount}} BTC = {{$info->iTotal}} ALLN</div>
                        <div class="allnVal">{{trans('invest_pay.allnVal')}}</div>
                        <br>
                        <div class="thumbnail">
                            <img src="/portal_assets/htm/img/BTC_qrcode.jpg">
                        </div>
                        <div class="titleS">{{trans('invest_pay.titleS')}}：</div>
                        <div class="input-group">
                            <input type="text" class="form-control" id="addrInput" placeholder="" value="35dqh7KM1Cq84rKe3mYtCc4qYiwVGbMxEx">
                            <span class="input-group-btn">
                                <button class="btn copyBtn" type="button" onclick="copyFn();">copy</button>
                            </span>
                        </div>
                        <div class="note">
                            <div class="text">{{trans('invest_pay.note')}}：<br>
                                <ol class="text-left">
                                    <li>{{trans('invest_pay.note_1')}}</li>
                                    <li>{{trans('invest_pay.note_2')}}</li>
                                    <li>{{trans('invest_pay.note_3')}}</li>
                                    <li>{{trans('invest_pay.note_4')}}</li>
                                </ol>
                            </div>
                        </div>
                        <a href="{{url('member')}}#p2" class="btn sendBtn">{{trans('invest_pay.sendBtn')}}</a>
                    </div>
                </div>
            </div>
        @endif
        @if($info->vType == "USD")
            <div class="section" id="sec1">
                <div class="container">
                    <div class="articleCnt">
                        <div class="title">{{trans('invest_pay.title')}}</div>
                        {{--<div class="todayVal">{{$info->iCount}} USD = {{$info->iTotal}} ALLN</div>--}}
                        {{--<div class="allnVal">{{trans('invest_pay.allnVal')}}</div>--}}
                        <br>
                        <div class="titleS">Payment Link：
                            <a href="https://allntoken.shoplineapp.com/" class="btn btn-success" target="_blank">Pay</a></div>
                        <br>
                        <br>
                        <a href="{{url('member')}}#p2" class="btn sendBtn">{{trans('invest_pay.sendBtn')}}</a>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script type="text/javascript" src="/portal_assets/htm/js/payment.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            $(".btn-pay").click(function () {
                modal_show({title: '付款說明', content: '尚未開放'});
                return false;
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->