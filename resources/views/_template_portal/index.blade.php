@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/index.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <div class="canvasArea">
        <div id="particle-canvas"></div>
    </div>
    <!-- 內容區塊 -->
    <div class="content">
        <!-- 區塊／輪播／01-大圖輪播 -->
        <section class="section" id="sec1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="leftText">
                            <div class="title">{{trans('aln.sec1.title')}}</div>
                            {{--<div class="activityTime">--}}
                            {{--<div class="titleS">獲得<span>{{$activity->iBonus or 0}}%</span>獎金時間剩下</div>--}}
                            {{--</div>--}}
                            <div class="desc"><strong>PRE-ICO 优惠己结束</strong><br>
                                <small>让ALLN成为加密数字货币的VISA.MasterCard</small>
                            </div>

                            <div class="btnGroup">
                                <div class="btn lPaperBtn" data-toggle="modal" data-target="#lightPaperModal">簡易說明</div>
                                <div class="btn wPaperBtn" data-toggle="modal" data-target="#whitePaperModal"> 白皮书</div>
                                <div class="btn buyBtn btn-invest"><i class="fas fa-arrow-right"></i> Sign Up</div>
                            </div>
                            <div class="btnGroup center share">
                                <div class="title">Follow us：</div>
                                <div class="btn"><a href="javascript:void(0);" data-toggle="modal" data-target="#weChat"><i class="fab fa-weixin"></i></a></div>
                                <div class="btn"><a href="https://twitter.com/ALLNTOKEN" target="_blank"><i class="fab fa-twitter"></i></a></div>
                                <div class="btn"><a href="https://t.me/allntoken" target="_blank"><i class="fab fa-telegram-plane"></i></a></div>
                                <div class="btn"><a href="https://www.facebook.com/ALLNTOKEN" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
                                <div class="btn"><a href="https://medium.com/@allntoken" target="_blank"><i class="fab fa-medium-m"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rightText">
                            <div class="iframeCnt">
                                <iframe src="https://player.vimeo.com/video/265489249" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- 區塊／入口／02-類別入口 -->
        <section class="section" id="sec3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="leftText">
                            <div class="coin"><img src="/portal_assets/htm/img/coin.png" alt=""></div>
                            <div class="title">ALLN</div>
                            <div class="desc">Airline and Life Networking Token (ALLN) 是Huafu Enterprise Holdings Limited 所打造的去中心化实体消费应用平台</div>
                            <div class="btn seeBtn"><i class="fas fa-arrow-right"></i> see Benefits</div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="rightText">
                            <ul class="fa-ul">
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    透过远东航空的飞航服务、提升及扩大布局各区域航网密度的可能性
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    藉由加密数字货币结合大型企业支撑的实体消费生态圈
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    期望未来成为领先全球的区块链落地消费体系，期望未来成为领先全球的区块链落地消费体系
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    打兼具有消费、智能合约、资产配置等赋能式的消费服务功能与交易上的新渠道
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    促进加密数字货币在应用端的无限可能性
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="iframeCnt">
                            <iframe src="https://player.vimeo.com/video/265489249" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row cubeCnt">
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>9</span>年</div>
                                    <div class="desc">ALLN在香港成立9年</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>61</span>年</div>
                                    <div class="desc">主要策略夥伴FAT成立61年</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>2</span>萬</div>
                                    <div class="desc">主要策略伙伴FAT每年有2万次航班</div>
                                </div>
                            </div>
                        </div>
                        <div class="row cubeCnt">
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>4</span>年</div>
                                    <div class="desc">主要策略伙伴FAT美国世界航空维修指南2015-2018：专业维修认证殊荣</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>300</span>万次</div>
                                    <div class="desc">主要策略伙伴FAT载运了300万次</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>429</span>%</div>
                                    <div class="desc">主要策略伙伴FAT2014年获利年增429%</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="whiteArraw"></div>
        </section>
    {{--<section class="section" id="secA">--}}
    {{--<div class="container">--}}
    {{--<div class="articleCnt">--}}
    {{--<div class="title text-left"><span>ALLN</span> Acquire Aircrafts From</div>--}}
    {{--<div class="row">--}}
    {{--<div class="col-xs-12 col-sm-4">--}}
    {{--<div class="cube c1">--}}
    {{--<div class="buyIcon">--}}
    {{--<img src="/portal_assets/htm/img/ALLN_aircraft.png">&nbsp;&nbsp;Buy--}}
    {{--</div>--}}
    {{--<div class="circleCnt"></div>--}}
    {{--<table class="table">--}}
    {{--<tr>--}}
    {{--<td>预购机型</td>--}}
    {{--<td>737 MAX-8</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>售价</td>--}}
    {{--<td>117.1M USD</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-12 col-sm-4">--}}
    {{--<div class="cube c2">--}}
    {{--<div class="buyIcon">--}}
    {{--<img src="/portal_assets/htm/img/ALLN_aircraft.png">&nbsp;&nbsp;Buy--}}
    {{--</div>--}}
    {{--<div class="circleCnt"></div>--}}
    {{--<table class="table">--}}
    {{--<tr>--}}
    {{--<td>预购机型</td>--}}
    {{--<td>ATR72-600</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>售价</td>--}}
    {{--<td>27M USD</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-12 col-sm-4">--}}
    {{--<div class="cube c3">--}}
    {{--<div class="buyIcon">--}}
    {{--<img src="/portal_assets/htm/img/ALLN_aircraft.png">&nbsp;&nbsp;Buy--}}
    {{--</div>--}}
    {{--<div class="circleCnt"></div>--}}
    {{--<table class="table">--}}
    {{--<tr>--}}
    {{--<td>预购机型</td>--}}
    {{--<td>A320 NEO<br>A321 NEO</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>售价</td>--}}
    {{--<td>110.6M USD<br>129.5M USD</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    <!-- 區塊／入口／03-類別入口 -->
        <section class="section" id="secE">
            <div class="container">
                <div class="articleCnt">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="itemCnt">
                                <img src="/portal_assets/htm/img/plan.png">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="itemCnt">
                                <div class="title"><span>ALLN</span>的應用</div>
                                <div class="desc">在解决加密数字货币于实体应用作为核心发展时，ALLN在参考信用卡模式，建立属于应用端的「清算中心」，改善目前以加密数字货币兑换商品或服务坑长的等待时间，同时达到配合商家应用发展和便利性的模式。</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- 區塊／入口／03-類別入口 -->
        <section class="section" id="secB">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">持有<span>ALLN</span>的好处</div>
                    <div class="row cubeCnt">
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">1</div>
                                <div class="title">5.5~8.8折</div>
                                <div class="desc">以ALLN消费兑换远东航空机票可享高优惠折扣</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">2</div>
                                <div class="title">兑换机票</div>
                                <div class="desc">ALLN可于远东航空兑换机票与周边服务</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">3</div>
                                <div class="title">购物折扣</div>
                                <div class="desc">以ALLN消费机上购物可享高优惠折扣</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">4</div>
                                <div class="desc">远东航空关联公司旗下之旅行社、连锁商务酒店，将来可接受ALLN，透过实体产业链拓展方式，让ALLN于初期即被快速流通和应用并有大量用户。</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- 區塊／入口／03-類別入口 -->
        @if(count($start_time))
            <section class="section" id="secF">
                <div class="container">
                    <div class="title text-center">ALLN 优惠时程</div>
                    <div class="articleCnt">
                        @foreach( $start_time as $key => $item )
                            <div class="row items">
                                <div class="col-xs-4">{{$start_time[$key]}} - {{$end_time[$key]}}</div>
                                <div class="col-xs-4">{{($bonus[$key] > 0 )? $bonus[$key]:'??'}}% Bonus</div>
                                <div class="col-xs-4 status">
                                    <div class="sale">SALE ENDS IN</div>
                                    <div class="time">{{((strtotime($end_time[$key]) - strtotime($start_time[$key]))/3600/24)+1}} days</div>
                                    <div class="icon"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
    @endif
    <!-- 區塊／入口／03-類別入口 -->
    {{--<section class="section" id="secC">--}}
    {{--<div class="container">--}}
    {{--<div class="articleCnt">--}}
    {{--<div class="title"><span>ALLN</span>发行时程</div>--}}
    {{--<div class="row">--}}
    {{--<div class="itemCnt">--}}
    {{--<img src="/portal_assets/htm/img/time.png">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}

    <!-- 區塊／入口／03-類別入口 -->
        <section class="section" id="secD">
            <div class="container">
                <div class="articleCnt a1">
                    <div class="title">{{trans('aln.secD.title')}}</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CEO.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p1_name')}}
                                        <small>{{trans('aln.secD.p1_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p1_desc')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CFO.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p2_name')}}
                                        <small>{{trans('aln.secD.p2_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p2_desc')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/GC.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p3_name')}}
                                        <small>{{trans('aln.secD.p3_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p3_desc')}}</p>
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-xs-12 col-sm-6 col-md-3">--}}
                        {{--<div class="thumbnail">--}}
                        {{--<img src="/portal_assets/htm/img/CMO.jpg">--}}
                        {{--<div class="caption text-center">--}}
                        {{--<h3>{{trans('aln.secD.p4_name')}}--}}
                        {{--<small>{{trans('aln.secD.p4_title')}}</small>--}}
                        {{--</h3>--}}
                        {{--<p>{{trans('aln.secD.p4_desc')}}</p>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CPO.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p5_name')}}
                                        <small>{{trans('aln.secD.p5_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p5_desc')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CAO.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p6_name')}}
                                        <small>{{trans('aln.secD.p6_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p6_desc')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CDO.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p7_name')}}
                                        <small>{{trans('aln.secD.p7_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p7_desc')}}</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-3">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CTO.jpg">
                                <div class="caption text-center">
                                    <h3>{{trans('aln.secD.p8_name')}}
                                        <small>{{trans('aln.secD.p8_title')}}</small>
                                    </h3>
                                    <p>{{trans('aln.secD.p8_desc')}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="articleCnt a2">--}}
                {{--<div class="row">--}}
                {{--<div class="col-sm-offset-3 col-sm-6">--}}
                {{--<div class="title">顾问</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-xs-6 col-sm-6">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="/portal_assets/htm/img/professor.jpg" class="img-circle">--}}
                {{--<div class="caption text-center">--}}
                {{--<h3>{{trans('aln.secD.p21_name')}} {{trans('aln.secD.p21_title')}}</h3>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xs-6 col-sm-6">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="/portal_assets/htm/img/lawyer.jpg" class="img-circle">--}}
                {{--<div class="caption text-center">--}}
                {{--<h3>{{trans('aln.secD.p22_name')}} {{trans('aln.secD.p22_title')}}</h3>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-12">--}}
                {{--<div class="title">{{trans('aln.secD.p31_title')}}</div>--}}
                {{--<div class="thumbnail">--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners1.png"></div>--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners2.png"></div>--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners3.png"></div>--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners4.png"></div>--}}
                {{--<div class="caption text-center">--}}
                {{--<p>{{trans('aln.secD.p31_desc')}}</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="articleCnt a3">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="title">技术顾问</div>
                            <br><br>
                        </div>
                        <div class="col-sm-12 photo">
                            <div class="row">
                                <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-2">
                                    <div class="thumbnail">
                                        <img src="/portal_assets/htm/img/p5.jpg" class="img-circle">
                                        <div class="caption text-center">
                                            <h3>Eric Gu 元界创始人兼 CEO</h3>
                                            <p>加拿大、美国工作 20 多年，曾担任加拿大安大略省大型企业高级程序员及项目经理。<br>2013 年开始参与比特币和区块链社区，并加入许多早期的区块链国际项目和尖端研究。<br>撰写并翻译许多区块链相关文章和书籍，包括 Melanie Swan
                                                中文版“新经济区块链蓝图” (2015 年)。</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0">
                                    <div class="thumbnail">
                                        <img src="/portal_assets/htm/img/p6.png" class="img-circle">
                                        <div class="caption text-center">
                                            <h3>郭宏才(宝二爷)</h3>
                                            <p>著名的区块链投资<br>比特币早期投资</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- 區塊／資訊／01-固定六個資訊 -->
        <section class="section information num-1" id="sec4">
            <div class="container">
                <div class="title-region">{{trans('aln.sec4.title')}}</div>
                <div class="row">
                    @foreach($news as $key =>$item)
                        <div class="col-sm-4">
                            <div class="itemBox thumbnail">
                                <a href="{{$item->url}}" target="_blank">
                                    <div class="imgBox rImg"><img src="{{$item->vImages}}" alt=""></div>
                                    <div class="text-box">
                                        <div class="descBox ellipsis">{{$item->vTitle}}</div>
                                        <div class="more-box"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>{{trans('aln.sec4.link')}}</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- 區塊／入口／08-類別入口 -->
        <section class="section" id="sec8">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">成为第一个找到答案的人</div>
                    <div class="desc">获得早期访问权限和独家奖励。<br>获得最新的ALLN新闻等等。</div>
                    <div class="input-group input-group-lg">
                        <input type="email" class="form-control vEmail" placeholder="Email addres">
                        <span class="input-group-btn">
                            <button class="btn joinBtn" type="button">加入心愿单</button>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- modal -->
    <div class="modal fade" id="warningModel" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">重要信息澄清</h3>
                </div>
                <div class="modal-body text-white text-left">
                    近日发现不肖人士假借ALLN官方名义于中文社群公告ALLN发展情形，今与策略合作伙伴远东航空共同予以澄清如下：<br><br>
                    <ol>
                        <li>「远航币」、「远航ALLN币」、「航空币」等皆为诈欺行为，远东航空于2018年5月7日晚间由律师具状向台北市警察局刑事警察大队科技犯罪侦察队提出诈骗刑事告诉。</li>
                        <li>不实消息指称ALLN已获得包括：火币、币安签约投资并非事实。</li>
                        <li>ALLN尚未确认于任何交易所挂牌，相关事务仍在洽谈阶段。</li>
                        <li>ALLN为Huafu Enterprise Holdings Limited (Hong Kong)所发行之区块链应用项目，ALLN不保证任何倍数获利，参与者应了解区块链技术应用前提下理性参与。</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script type="text/javascript" src="/portal_assets/dist/timers/jquery.timers.min.js"></script>
    {{--<script type="text/javascript" src="/portal_assets/htm/js/index.js"></script>--}}
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    @include('_template_portal._js.index')
@endsection
<!-- ================== /inline-js ================== -->