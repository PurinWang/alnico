@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/member.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">会员中心</div>
                    <div class="activeTab" role="tabpanel">
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#p1" id="p1-tab" role="tab" data-toggle="tab" aria-controls="p1" aria-expanded="true">KYC</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#p2" role="tab" id="p2-tab" data-toggle="tab" aria-controls="p2" aria-expanded="false">投资记录</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#p3" role="tab" id="p3-tab" data-toggle="tab" aria-controls="p3" aria-expanded="false">空投任务</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="p1" aria-labelledby="p1-tab">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 control-label">{{trans('member.vUserName')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vUserName"
                                                   value="{{session('shop_member.info.vUserName')}}"
                                                   placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-3 control-label">{{trans('member.vUserEmail')}}</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control vUserEmail"
                                                   value="{{session('shop_member.info.vUserEmail')}}" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputRegion" class="col-sm-3 control-label">{{trans('member.vCountry')}}</label>
                                        <div class="col-sm-9">
                                            <select class="form-control vCountry" id="country"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCoinAddr" class="col-sm-3 control-label">{{trans('member.vUserID')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vUserID" id="vUserID" placeholder=""
                                                   value="{{session('shop_member.info.vUserID')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCoinAddr" class="col-sm-3 control-label">ERC20 {{trans('member.vERC')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vERC" id="inputCoinAddr" placeholder="{{trans('member.vERC_content')}}"
                                                   value="{{session('shop_member.info.vERC')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputBirthday" class="col-sm-3 control-label">{{trans('member.iUserBirthday')}}</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control iUserBirthday" id="inputBirthday" placeholder=""
                                                   value="{{date('Y-m-d',session('shop_member.info.iUserBirthday'))}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputSex" class="col-sm-3 control-label">{{trans('member.vUserTitle')}}</label>
                                        <div class="col-sm-9">
                                            <select class="form-control vUserTitle" id="inputSex">
                                                <option value="0">{{trans('member.choose')}}</option>
                                                <option value="Male" @if(session('shop_member.info.vUserTitle')=="Male")selected @endif>Male</option>
                                                <option value="Female" @if(session('shop_member.info.vUserTitle')=="Female")selected @endif>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAddr" class="col-sm-3 control-label">{{trans('member.vUserAddress')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vUserAddress" id="inputAddr" placeholder=""
                                                   value="{{session('shop_member.info.vUserAddress')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputTel" class="col-xs-12 col-sm-3 control-label">{{trans('member.vUserContact')}}</label>
                                        <div class="col-xs-4 col-sm-2">
                                            <input type="text" class="form-control vCountryCode" id="inputZipCode" placeholder="{{trans('member.vCountryCode')}}"
                                                   value="{{session('shop_member.info.vCountryCode')}}">
                                        </div>
                                        <div class="col-xs-8 col-sm-7">
                                            <input type="text" class="form-control vUserContact" id="inputTel" placeholder=""
                                                   value="{{session('shop_member.info.vUserContact')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPicF" class="col-sm-3 control-label">{{trans('member.vPassportF')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" name="vPassportF" id="vPassportF" style="display: none;">
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPicB" class="col-sm-3 control-label">{{trans('member.vPassportB')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" id="vPassportB" style="display: none;">
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPicT" class="col-sm-3 control-label">{{trans('member.vPassportT')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" id="vPassportT" style="display: none;">
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="note">
                                                <div class="text">{{trans('member.p1_note')}}</div>
                                            </div>
                                            <div class="btn saveBtn btn-save">{{trans('member.save')}}</div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p2" aria-labelledby="p2-tab">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>投资单号</th>
                                        <th>投资日期</th>
                                        <th>投资金额</th>
                                        <th>ALLN</th>
                                        <th>预计奖励</th>
                                        <th>状态</th>
                                        <th>上传截图</th>
                                        <th>ALLN统计</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invest as $key => $item)
                                        <tr>
                                            <td>{{$item->vInvestNum}}</td>
                                            <td>{{date('Y/m/d H:i:s',$item->iCreateTime)}}</td>
                                            <td>{{$item->iCount}} {{$item->vType}}</td>
                                            <td>{{number_format($item->iTotal)}}</td>
                                            <td>{{number_format($item->bonus)}}</td>
                                            <td>
                                                @if($item->vImages =="" && $item->iStatus == 0)
                                                    <div class="stepStatus red form-control">@if($item->vType=="USD")需24小時 審核 @else 未完成 @endif</div>
                                                @endif
                                                @if($item->vImages !="" && $item->iStatus == 0)
                                                    <div class="stepStatus yellow form-control">審核中</div>
                                                @endif
                                                @if($item->iStatus == 1)
                                                    <div class="stepStatus green form-control">已完成</div>
                                                @endif
                                            </td>
                                            <td>
                                                @if($item->iStatus == 1)
                                                @else
                                                    <div class="btn uploadBtn form-control btn-invest-images" data-invest_num="{{$item->vInvestNum}}">
                                                        <i class="fas fa-upload"></i> 上傳
                                                    </div>
                                                @endif
                                            </td>
                                            <td>{{number_format($item->iTotal+$item->bonus)}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div role="tabpanel" class="tab-pane fade text-left" id="p3" aria-labelledby="p3-tab">
                                <div class="d-flex">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <div class="btn btn-link text-light" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                                @if(isset($coin_airdrops[1]->vValue) && $coin_airdrops[1]->vValue)
                                                    <i class="fas fa-check-circle" style="color:#44aa54;"></i>
                                                @else
                                                    <i class="fas fa-times-circle" style="color:#e71032;"></i>
                                                @endif
                                                任务一：注册完成可获得{{$coin_airdrops[1]->iBonus or 0}}枚 ( 实际可获得 <span>{{$coin_airdrops[1]->iGetBonus or 0}}</span> 枚)
                                                <span class="angleIcon"><i class="fas fa-angle-up"></i></span>
                                            </div>
                                        </div>
                                        <div id="collapseOne" class="collapse in" aria-labelledby="headingOne">
                                            <div class="card-body">
                                                于2018-4-4 18:00 (GMT+8) 之前加入的会员于任务一得到ALLN奖励为30枚
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingTwo">
                                            <div class="btn btn-link text-light" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false"
                                                 aria-controls="collapseTwo">
                                                @if(isset($coin_airdrops[2]->bonus) && $coin_airdrops[2]->bonus->vValue && $coin_airdrops[2]->bonus->iStatus == 1)
                                                    <i class="fas fa-check-circle" style="color:#44aa54;"></i>
                                                @elseif(isset($coin_airdrops[2]->bonus) && $coin_airdrops[2]->bonus->vValue && $coin_airdrops[2]->bonus->iStatus != 1)
                                                    <i class="fas fa-exclamation-circle" style="color:#e5a913;"></i>
                                                @else
                                                    <i class="fas fa-times-circle" style="color:#e71032;"></i>
                                                @endif
                                                任务二：订阅ALLN电报可获得{{$coin_airdrops[2]->iBonus or 0}}枚 ( 实际可获得 <span>{{$coin_airdrops[2]->bonus->iGetBonus or 0}}</span> 枚)
                                                <span class="angleIcon"><i class="fas fa-angle-up"></i></span>
                                            </div>
                                        </div>
                                        <div id="collapseTwo" class="collapse in" aria-labelledby="headingTwo">
                                            <div class="card-body">
                                                加入ALLN电报：<a href="https://t.me/allntoken" target="_blank">https://t.me/allntoken</a><br>
                                                <form class="form-inline">
                                                    <div class="form-group mb-2">
                                                        电报帐号：
                                                    </div>
                                                    <div class="form-group mx-sm-3">
                                                        <label for="inputID" class="sr-only"></label>
                                                        <input type="text" class="form-control inputID" id="inputID" placeholder=""
                                                               value="{{$coin_airdrops[2]->bonus->vValue or ''}}">
                                                    </div>
                                                    @if($coin_airdrops[2])
                                                        <button type="button" class="btn btn-danger btn-send-lightenpaper" data-id="{{$coin_airdrops[2]->iId or 0}}">完成任务</button>
                                                    @endif
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header" id="headingThree">
                                            <div class="btn btn-link text-light" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false"
                                                 aria-controls="collapseThree">
                                                @if(isset($coin_airdrops[3]->friend) && $coin_airdrops[3]->friend)
                                                    <i class="fas fa-check-circle" style="color:#44aa54;"></i>
                                                @else
                                                    <i class="fas fa-times-circle" style="color:#e71032;"></i>
                                                @endif
                                                任务三：邀请好友并注册成功  ( 实际可获得 <span>{{$coin_airdrops[3]->iGetBonus or 0}}</span> 枚)
                                                <span class="angleIcon"><i class="fas fa-angle-up"></i></span>
                                            </div>
                                        </div>
                                        <div id="collapseThree" class="collapse in" aria-labelledby="headingThree">
                                            <div class="card-body">
                                                分享专属注册连结，邀请好友透过此连结注册。注册成功即可获得{{$coin_airdrops[3]->iBonus or 0}}枚ALLN奖励！<br>
                                                <form class="form-inline">
                                                    <div class="form-group mb-2">
                                                        您的专属注册连结：
                                                    </div>
                                                    <div class="form-group mx-sm-3">
                                                        <label for="inputID" class="sr-only"></label>
                                                        <input type="text" class="form-control col-lg-12" id="addrInput" placeholder=""
                                                               value="{{url('/R'.session('shop_member.iUserId'))}}">
                                                    </div>
                                                    <button type="button" class="btn btn-danger" onclick="copyFn();">copy</button>
                                                </form>
                                                <i class="fas fa-users"></i>&nbsp;&nbsp;您已分享<span> {{$coin_airdrops[3]->friend or 0}} </span>位好友
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel">上傳截圖</h4>
                </div>
                <div class="modal-body">
                    <div class="file-loading">
                        <input id="inputUpload" name="inputUpload" type="file">
                    </div>
                    <div id="kartik-file-errors"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-send-invest" title="Your custom upload logic">Save</button>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script type="text/javascript" src="/portal_assets/htm/js/member.js"></script>
    <script src="/_assets/country-state-select.js"></script>
    <!-- Plugin Customer-->
    <script type="text/javascript" src="/_assets/CryptoJS/rollups/md5.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    @include('_template_portal._js.member')
@endsection
<!-- ================== /inline-js ================== -->