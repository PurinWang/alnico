@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/member.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{{trans('member.title')}}</div>
                    <div class="activeTab" role="tabpanel">
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#p1" id="p1-tab" role="tab" data-toggle="tab" aria-controls="p1" aria-expanded="true">{{trans('member.p1_tab')}}</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#p2" role="tab" id="p2-tab" data-toggle="tab" aria-controls="p2" aria-expanded="false">{{trans('member.p2_tab')}}</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#p3" role="tab" id="p3-tab" data-toggle="tab" aria-controls="p3" aria-expanded="false">{{trans('member.p3_tab')}}</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#p4" role="tab" id="p4-tab" data-toggle="tab" aria-controls="p4" aria-expanded="false">{{trans('member.p4_tab')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="p1" aria-labelledby="p1-tab">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputName" class="col-sm-3 control-label">{{trans('member.vUserName')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vUserName"
                                                   value="{{session('shop_member.info.vUserName')}}"
                                                   placeholder="Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail" class="col-sm-3 control-label">{{trans('member.vUserEmail')}}</label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control vUserEmail"
                                                   value="{{session('shop_member.info.vUserEmail')}}" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputRegion" class="col-sm-3 control-label">{{trans('member.vCountry')}}</label>
                                        <div class="col-sm-9">
                                            <select class="form-control vCountry" id="country"></select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCoinAddr" class="col-sm-3 control-label">{{trans('member.vUserID')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vUserID" id="vUserID" placeholder=""
                                                   value="{{session('shop_member.info.vUserID')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCoinAddr" class="col-sm-3 control-label">ERC20 {{trans('member.vERC')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vERC" id="inputCoinAddr" placeholder="{{trans('member.vERC_content')}}"
                                                   value="{{session('shop_member.info.vERC')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputBirthday" class="col-sm-3 control-label">{{trans('member.iUserBirthday')}}</label>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control iUserBirthday" id="inputBirthday" placeholder=""
                                                   value="{{date('Y-m-d',session('shop_member.info.iUserBirthday'))}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputSex" class="col-sm-3 control-label">{{trans('member.vUserTitle')}}</label>
                                        <div class="col-sm-9">
                                            <select class="form-control vUserTitle" id="inputSex">
                                                <option value="0">{{trans('member.choose')}}</option>
                                                <option value="Male" @if(session('shop_member.info.vUserTitle')=="Male")selected @endif>Male</option>
                                                <option value="Female" @if(session('shop_member.info.vUserTitle')=="Female")selected @endif>Female</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputAddr" class="col-sm-3 control-label">{{trans('member.vUserAddress')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vUserAddress" id="inputAddr" placeholder=""
                                                   value="{{session('shop_member.info.vUserAddress')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputTel" class="col-xs-12 col-sm-3 control-label">{{trans('member.vUserContact')}}</label>
                                        <div class="col-xs-4 col-sm-2">
                                            <input type="text" class="form-control vCountryCode" id="inputZipCode" placeholder="{{trans('member.vCountryCode')}}"
                                                   value="{{session('shop_member.info.vCountryCode')}}">
                                        </div>
                                        <div class="col-xs-8 col-sm-7">
                                            <input type="text" class="form-control vUserContact" id="inputTel" placeholder=""
                                                   value="{{session('shop_member.info.vUserContact')}}">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPicF" class="col-sm-3 control-label">{{trans('member.vPassportF')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" name="vPassportF" id="vPassportF" style="display: none;">
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPicB" class="col-sm-3 control-label">{{trans('member.vPassportB')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" id="vPassportB" style="display: none;">
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPicT" class="col-sm-3 control-label">{{trans('member.vPassportT')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" id="vPassportT" style="display: none;">
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="note">
                                                <div class="text">{{trans('member.p1_note')}}</div>
                                            </div>
                                            <div class="btn saveBtn btn-save">{{trans('member.save')}}</div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p2" aria-labelledby="p2-tab">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="inputCurrentPW" class="col-sm-3 control-label">{{trans('member.inputCurrentPW')}}</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control vPassword" id="inputCurrentPW" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputNewPW" class="col-sm-3 control-label">{{trans('member.vPassword_new')}}</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control vPassword_new" id="inputNewPW" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputCheckNewPW" class="col-sm-3 control-label">{{trans('member.vPassword_confirm')}}</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control vPassword_confirm" id="inputCheckNewPW" placeholder="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="btn saveBtn btn-dosave-pw">{{trans('member.save')}}</div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p3" aria-labelledby="p3-tab">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label for="vInvestNum" class="col-sm-3 control-label">{{trans('member.invest_num')}}</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control vInvestNum" id="vInvestNum" placeholder="" value="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <!-- <div class="note"><div class="text">Go to “Willing-to-pay Record”, copy the number and paste here.</div></div> -->
                                            <div class="note">
                                                <div class="text">{{trans('member.invest_content')}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputDrawPic" class="col-sm-3 control-label">{{trans('member.inputDrawPic')}}</label>
                                        <div class="col-sm-9">
                                            <div class="input-group">
                                                <label class="input-group-btn">
                                                    <span class="btn selectBtn">
                                                        {{trans('member.uploadfile')}}&hellip; <input type="file" id="invest_img" style="display: none;" multiple>
                                                    </span>
                                                </label>
                                                <input type="text" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="note">
                                                <div class="text">{{trans('member.p3_note')}}</div>
                                            </div>
                                            <div class="btn sendBtn btn-send-invest">{{trans('member.send')}}</div>
                                        </div>
                                    </div>
                                    <br><br>
                                    <hr>
                                    <div class="form-group">
                                        <label for="inputMessage" class="col-sm-3 control-label">{{trans('member.inputMessage')}}</label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control vSummary" style="height:200px;"></textarea>
                                            <a href="javascript:void(0);" class="btn sendBtn btn-send-message">{{trans('member.btn_send_message')}}</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p4" aria-labelledby="p4-tab">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th>{{trans('member.p4_1')}}</th>
                                        <th>{{trans('member.p4_2')}}</th>
                                        <th>{{trans('member.p4_3')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($invest as $key => $item)
                                        <tr>
                                            <td>{{$item->vInvestNum}}</td>
                                            <td>{{date('Y/m/d H:i:s',$item->iCreateTime)}}</td>
                                            <td>{{$item->iCount}} {{$item->vType}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script type="text/javascript" src="/portal_assets/htm/js/member.js"></script>
    <script src="/_assets/country-state-select.js"></script>
    <!-- Plugin Customer-->
    <script type="text/javascript" src="/_assets/CryptoJS/rollups/md5.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        var vPassportUrl;
        $(document).ready(function () {
            //
            populateCountries("country");
            //
            $(".vCountry").val("{{session('shop_member.info.vCountry')}}");
            //
            $(".btn-save").click(function () {
                if ($(".vCountry").val() == -1 || !$(".vCountry").val()) {
                    modal_show({title: 'Country', content: '{{trans('register.country_empty')}}'});
                    return false;
                }
                var data = {"_token": "{{ csrf_token() }}"};
                data.vUserName = $(".vUserName").val();
                data.vUserTitle = $(".vUserTitle").val();
                data.vUserID = $(".vUserID").val();
                data.vUserEmail = $(".vUserEmail").val();
                data.vCountryCode = $(".vCountryCode").val();
                data.vUserContact = $(".vUserContact").val();
                data.iUserBirthday = $(".iUserBirthday").val();
                data.vUserAddress = $(".vUserAddress").val();
                data.vCountry = $(".vCountry").val();
                data.vERC = $(".vERC").val();
                if (document.getElementById('vPassportF').files[0]) {
                    upload_file(document.getElementById('vPassportF').files[0]);
                    data.vPassportF = vPassportUrl;
                }
                if (document.getElementById('vPassportB').files[0]) {
                    upload_file(document.getElementById('vPassportB').files[0]);
                    data.vPassportB = vPassportUrl;
                }
                if (document.getElementById('vPassportT').files[0]) {
                    upload_file(document.getElementById('vPassportT').files[0]);
                    data.vPassportT = vPassportUrl;
                }
                if (!reg_Email.test(data.vUserEmail)) {
                    modal_show({title: 'Email', content: 'Email Error'});
                    return false;
                }
                $.ajax({
                    url: "{{url('member/dosave')}}",
                    data: data,
                    type: "POST",
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            setTimeout(function () {
                                //location.href = rtndata.rtnurl;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            });
            //
            $(".btn-dosave-pw").click(function () {
                var obj = $(this).closest('#p2');
                if (obj.find(".vPassword").val() == "") {
                    modal_show({title: 'Password', content: 'Password empty'});
                    obj.find(".vPassword").focus();
                    return false;
                }
                if (obj.find(".vPassword_new").val() == "") {
                    modal_show({title: 'Password', content: 'Password empty'});
                    obj.find(".vPassword_new").focus();
                    return false;
                }
                if (obj.find(".vPassword_new").val() != obj.find(".vPassword_confirm").val()) {
                    modal_show({title: 'ConfirmPassword', content: 'Confirm Password Not Good'});
                    return false;
                }
                var data = {"_token": "{{ csrf_token() }}"};
                data.vPassword = CryptoJS.MD5(obj.find(".vPassword").val()).toString(CryptoJS.enc.Base64);
                data.vPasswordNew = CryptoJS.MD5(obj.find(".vPassword_new").val()).toString(CryptoJS.enc.Base64);
                $.ajax({
                    url: "{{url('member/dosavepassword')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            setTimeout(function () {
                                location.href = rtndata.rtnurl;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            })
            //
            $(".btn-send-invest").click(function () {
                var data = {"_token": "{{ csrf_token() }}"};
                if (document.getElementById('invest_img').files[0]) {
                    upload_file(document.getElementById('invest_img').files[0]);
                    data.vInvestNum = $(".vInvestNum").val();
                    data.vImages = vPassportUrl;
                    $.ajax({
                        url: "{{url('member/dosaveinvest')}}",
                        data: data,
                        type: "POST",
                        success: function (rtndata) {
                            if (rtndata.status) {
                                modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                                setTimeout(function () {
                                    location.reload();
                                }, 1000)
                            } else {
                                modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            }
                        }
                    });
                }
            })
            //
            $(".btn-send-message").click(function () {
                if ($('.vSummary').val() == '') {
                    toastr.info("{{trans('member.summary_empty')}}", "{{trans('_web_alert.notice')}}")
                    return false;
                }
                //
                swal({
                    title: "{{trans('member.message_title')}}",
                    text: "{{trans('member.message_content')}}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{trans('_web_alert.cancel')}}",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('_web_alert.ok')}}",
                    closeOnConfirm: true
                }, function () {
                    var data = {"_token": "{{ csrf_token() }}"};
                    data.vSummary = $(".vSummary").val();
                    $.ajax({
                        url: "{{url('member/doaddmessage')}}",
                        type: "POST",
                        data: data,
                        success: function (rtndata) {
                            switch (rtndata.status) {
                                case 1:
                                    toastr.success(rtndata.message, "{{trans('_web_alert.notice')}}");
                                    location.href = "{{url('member')}}/#p3";
                                    break;
                                default:
                                    toastr.error(rtndata.message, "{{trans('_web_alert.notice')}}");
                                    break
                            }
                        }
                    });
                });
            });
        });
        function upload_file(file) {
            var data = new FormData();
            data.append("_token", "{{ csrf_token() }}");
            data.append("files", file);
            $.ajax({
                data: data,
                type: "POST",
                url: "{{url('upload_image')}}",
                async: false,
                contentType: false,
                processData: false,
                success: function (rtndata) {
                    if (rtndata.status) {
                        vPassportUrl = rtndata.files[0].url;
                    } else {
                        vPassportUrl = "";
                    }
                }
            });
        }
    </script>
@endsection
<!-- ================== /inline-js ================== -->