@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/forgotPassword.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{{trans('forgotpassword.title')}}</div>
                    <div class="desc">{{trans('forgotpassword.desc')}}</div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control input-lg email" placeholder="{{trans('forgotpassword.email')}}">
                        </div>
                        <div class="form-group">
                            <div class="note">
                                <div class="text">{{trans('forgotpassword.note')}}</div>
                            </div>
                            <div class="btn sendPwBtn btn-verification"> {{trans('forgotpassword.sendPwBtn')}}</div>
                        </div>
                    </form>
                    <form id="verification" class="form-horizontal" style="display: none;">
                        <div class="form-group">
                            <label for="inputTemporaryPassword" class="col-sm-3 control-label">{{trans('forgotpassword.verification')}}</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control vVerification" id="inputTemporaryPassword" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputNewPassword" class="col-sm-3 control-label">{{trans('forgotpassword.password')}}</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control password" id="inputNewPassword" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputNewPasswordAgain" class="col-sm-3 control-label">{{trans('forgotpassword.confirm_password')}}</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control confirm_password" id="inputNewPasswordAgain" placeholder="">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn sendBtn btn-send">{{trans('forgotpassword.send')}}</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script type="text/javascript" src="/portal_assets/htm/js/forgotPwd.js"></script>
    <script type="text/javascript" src="/_assets/CryptoJS/rollups/md5.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            $(".btn-verification").click(function () {
                if ($(".email").val() == "") {
                    modal_show({title: 'Email', content: $(".email").attr('placeholder') + '{{trans('forgotpassword.error')}}'});
                    $(".email").focus();
                    return false;
                }
                $(".btn-verification").attr('disabled', 'disabled');
                var data = {"_token": "{{ csrf_token() }}"};
                data.vAccount = $(".email").val();
                $.ajax({
                    url: "{{url('doSendVerification')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            $(".email").attr('disabled', 'disabled');
                            $(".btn-verification").attr('disabled', 'disabled');
                            $("#verification").css('display', 'block');
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            $(".btn-verification").removeAttr('disabled');
                        }
                    }
                });
            })
            //
            $(".btn-send").click(function () {
                if ($(".password").val() == "") {
                    modal_show({title: 'Password', content: $(".password").attr('placeholder')});
                    $(".password").focus();
                    return false;
                }
                if ($(".password").val() != $(".confirm_password").val()) {
                    modal_show({title: 'ConfirmPassword', content: $(".confirm_password").attr('placeholder')});
                    return false;
                }
                var data = {"_token": "{{ csrf_token() }}"};
                data.vVerification = $(".vVerification").val();
                data.vPassword = CryptoJS.MD5($(".password").val()).toString(CryptoJS.enc.Base64);
                $.ajax({
                    url: "{{url('doResetPassword')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            setTimeout(function () {
                                location.href = rtndata.rtnurl;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->