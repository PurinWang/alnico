@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/login.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{{trans('login.title')}}</div>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control email" id="exampleInputEmail1" placeholder="{{trans('login.email')}}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password" id="exampleInputPassword1" placeholder="{{trans('login.password')}}">
                        </div>
                        <div class="form-group">
                            <div class="forgotCnt">
                                <a href="{{url('forgotpassword')}}" class="pull-left">{{trans('login.forgotpassword')}}?</a>
                                <a href="{{url('register')}}" class="pull-right">{{trans('login.register')}}</a>
                                <div class="text pull-right">{{trans('login.not_sign')}}</div>
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="btn form-control btn-login">{{trans('login.login')}}</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <!-- Plugin Customer-->
    <script type="text/javascript" src="/_assets/CryptoJS/rollups/md5.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            $('.email').val(localStorage.getItem('account'));
            $('.password').val(localStorage.getItem('password'));
            if (localStorage.getItem('remember') == 'true') {
                $('input[name=remember]').prop("checked", true);
            } else {
                $('input[name=remember]').prop("checked", false);
            }
            //
            $(".btn-login").click(function () {
                if ($(".email").val() == "") {
                    modal_show({title: 'Email', content: $(".email").attr('placeholder') + '{{trans('login.错误')}}'});
                    $(".email").focus();
                    return false;
                }
                if ($(".password").val() == "") {
                    modal_show({title: 'Password', content: $(".password").attr('placeholder') + '{{trans('login.错误')}}'});
                    $(".password").focus();
                    return false;
                }
                var data = {"_token": "{{ csrf_token() }}"};
                data.vAccount = $(".email").val();
                data.vPassword = CryptoJS.MD5($(".password").val()).toString(CryptoJS.enc.Base64);
                $.ajax({
                    url: "{{url('doLogin')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('login.notice')}}', content: rtndata.message});
                            if ($('input[name=remember]').prop("checked")) {
                                localStorage.setItem('account', $(".email").val());
                                localStorage.setItem('password', $(".password").val());
                                localStorage.setItem('remember', true);
                            } else {
                                localStorage.setItem('account', '');
                                localStorage.setItem('password', '');
                                localStorage.setItem('remember', false);
                            }
                            setTimeout(function () {
                                location.href = rtndata.rtnurl;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('login.notice')}}', content: rtndata.message});
                        }
                    }
                });
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->