@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/invest.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!-- 內容區塊 -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{{trans('invest.title')}}</div>
                    <div class="cubeBtn">
                        <a href="{{url('member')}}#p3">
                            <div class="titleS">{{trans('invest.title_1')}}</div>
                            <div class="name blue">{{trans('invest.title_2')}}</div>
                        </a>
                    </div>
                    <div class="cubeBtn">
                        <a href="{{url('invest_create')}}">
                            <div class="titleS">{{trans('invest.title_3')}}</div>
                            <div class="name red">{{trans('invest.title_4')}}</div>
                        </a>
                    </div>
                    <div class="text">{{trans('invest.note_1')}}</div>
                    <div class="text">{{trans('invest.note_2')}}</div>
                    <div class="text">{{trans('invest.note_3')}}</div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->