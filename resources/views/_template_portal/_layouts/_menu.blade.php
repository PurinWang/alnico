<menu>
    <ul class="floatMenu">
        <li class="member">
            <p class="title">會員</p>
            <a href="{{url('member_center')}}"><i class="fa fa-user fa-lg" aria-hidden="true"></i></a>
        </li>
        <li class="share">
            <p class="title">分享</p>
            <a href="javascript:;"><i class="fa fa-share-alt fa-lg" aria-hidden="true"></i></a>
            <div class="linkGroup">
                <a href="javascript:website_share('fb');"><i class="fa fa-facebook-square fa-lg" aria-hidden="true"></i></a>
                <a href="javascript:website_share('gplus');"><i class="fa fa-google-plus-square fa-lg" aria-hidden="true"></i></a>
            </div>
        </li>
        <li class="shopCar">
            <p class="title">購物車</p>
            <a href="{{url('member_center/cart')}}"><i class="fa fa-shopping-cart fa-lg" aria-hidden="true"></i></a>
        </li>
        <li class="goTop">
            <p class="dirTitle">Top</p>
            <a href="javascript:;"><i class="fa fa-chevron-circle-up fa-lg" aria-hidden="true"></i></a>
        </li>
    </ul>
</menu>