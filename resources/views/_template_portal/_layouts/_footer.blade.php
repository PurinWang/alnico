<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-4">
                <div class="fBlock">
                    <div class="title">{{trans('aln.footer.title_1')}}</div>
                    <div class="fContent">
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#riskDesc">{{trans('aln.footer.title_2')}}</a>&nbsp;&nbsp;
                        {{--<a href="javascript:void(0);" data-toggle="modal" data-target="#issueProgram">{{trans('aln.footer.title_3')}}</a>--}}
                        <br>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#privacyDesc">{{trans('aln.footer.title_4')}}</a>&nbsp;&&nbsp;
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#serviceDesc">{{trans('aln.footer.title_5')}}</a><br>
                        <a href="javascript:void(0);" data-toggle="modal" data-target="#whitePaperModal">{{trans('aln.footer.title_6')}}</a>
                    </div>
                </div>
            </div>
            @if(session('locale') == 'zh-cn')
                <div class="col-xs-12 col-sm-4">
                    <div class="fBlock">
                        <div class="title">联络我们</div>
                        <div class="fContent">
                            寻求合作：<a href="mailto:b2b@allntoken.io">b2b@allntoken.io</a><br>
                            服务信箱：<a href="mailto:service@allntoken.io">service@allntoken.io</a><br><br>
                            {{--联络电话：+886-932-018-050 (华文客服）<br>服务时间：09:00至17:00 (香港时间)--}}
                        </div>
                    </div>
                </div>
            @else
                <div class="col-xs-12 col-sm-4">
                    <div class="fBlock">
                        <div class="title">Contact us</div>
                        <div class="fContent">
                            Business：<a href="mailto:b2b@allntoken.io">b2b@allntoken.io</a><br>
                            Service：<a href="mailto:service@allntoken.io">service@allntoken.io</a>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col-xs-12 col-sm-4">
                <div class="fBlock">
                    <div class="title">{{trans('aln.footer.title_10')}}</div>
                    <div class="fContent">
                        Your feedback is important to us. Please <a href="mailto:">email us</a> or useing <a href="{{url('feedback')}}">this form</a> for any suggestions.
                    </div>
                </div>
                <div class="fBlock">
                    <div class="title">{{trans('aln.footer.title_9')}}</div>
                    <div class="fContent">
                        <div class="btn weixinBtn"><a href="javascript:void(0);" data-toggle="modal" data-target="#weChat"><i class="fab fa-weixin"></i></a></div>
                        <div class="btn"><a href="https://twitter.com/ALLNTOKEN" target="_blank"><i class="fab fa-twitter"></i></a></div>
                        <div class="btn"><a href="https://t.me/allntoken" target="_blank"><i class="fab fa-telegram-plane"></i></a></div>
                        <div class="btn"><a href="https://www.facebook.com/ALLNTOKEN" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
                        <div class="btn"><a href="https://medium.com/@allntoken" target="_blank"><i class="fab fa-medium-m"></i></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <hr>
        </div>
    </div>
    <div class="container">
        <div class="copyright">Copyright © 2018 Airline and Life Networking.</div>
    </div>
</footer>