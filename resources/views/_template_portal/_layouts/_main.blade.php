<!DOCTYPE HTML>
<html>

<head>
    <title>ALLN</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">
    <meta name="description" content="Airline and Life Networking Token彻底颠覆加密货币的不可实用性，和您开启加密数字货币与实体消费应用对接的关键。消费要无所不在，才有价值。价值须无可取代，才有未来。"/>

    <meta property="og:title" content="ALLN (Airline and Life Networking)"/>
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="630">
    <meta property="og:image" content="{{asset('/portal_assets/htm/img/alln_ico.jpg')}}"/>
    <meta property="og:description" content="Airline and Life Networking Token彻底颠覆加密货币的不可实用性，和您开启加密数字货币与实体消费应用对接的关键。消费要无所不在，才有价值。价值须无可取代，才有未来。"/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content="ALLN (Airline and Life Networking)"/>

    <link rel="Shortcut Icon" type="image/x-icon" href="/images/coin.jpg">
    <link rel="stylesheet" href="/portal_assets/dist/package/bootstrap3/css/bootstrap.min.css">
    <link rel="stylesheet" href="/portal_assets/dist/package/fontAwesome/svg-with-js/css/fa-svg-with-js.css">
    <link rel="stylesheet" href="/portal_assets/htm/css/main.css">

    <!-- Sweet Alert -->
    <link href="/css/sweetalert/sweetalert.css" rel="stylesheet">
    <!-- Toastr style -->
    <link href="/css/toastr/toastr.min.css" rel="stylesheet">
    <!-- ================== page-css ================== -->
@yield('page-css')
<!-- ================== /page-css ================== -->
    <!-- Global site tag (gtag.js) - AdWords: 950645459 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-950645459"></script>
    <script> window.dataLayer = window.dataLayer || [];
        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'AW-950645459'); </script>
</head>

<body>
<div class="canvasArea">
    <div id="particle-canvas"></div>
</div>
@include('_template_portal._layouts._header')
<div class="airDropBtn">
    <a href="{{url('coin_airdrops')}}"><img src="/portal_assets/htm/img/promo_img.png"></a>
    <div class="shineCube"></div>
</div>
<!-- MAIN CONTENT -->
@yield('content')
<!-- END MAIN CONTENT -->
@include('_template_portal._layouts._footer')
<div class="modal fade" id="modal_alert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="modal-title">
                    {{--Title--}}
                </h4>
            </div>
            <div class="modal-body text-center">
                <div class="modal-text" id="modal-content">
                    {{--Content--}}
                </div>
                <div class="modal-textS" id="modal-alert"> {{--alert--}}</div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade" id="lightPaperModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="title">{{trans('aln.white_paper.title')}}</div>
                <div class="desc">{{trans('aln.white_paper.desc')}}</div>
                <div class="row">
                    @foreach($light_paper as $key => $item)
                        @if($item->iStatus)
                            <div class="col-xs-6 col-sm-4">
                                <div class="btn dBtn form-control"><a href="{{$item->vValue}}" target="_blank">{{$item->vTitle}}</a></div>
                            </div>
                        @else
                            <div class="col-xs-6 col-sm-4">
                                <div class="btn dBtn form-control disabled">{{$item->vTitle}}</div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!-- modal -->
<div class="modal fade" id="whitePaperModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="title">{{trans('aln.white_paper.title')}}</div>
                <div class="desc">{{trans('aln.white_paper.desc')}}</div>
                <div class="row">
                    @foreach($white_paper as $key => $item)
                        @if($item->iStatus)
                            <div class="col-xs-6 col-sm-4">
                                <div class="btn dBtn form-control"><a href="{{$item->vValue}}" target="_blank">{{$item->vTitle}}</a></div>
                            </div>
                        @else
                            <div class="col-xs-6 col-sm-4">
                                <div class="btn dBtn form-control disabled">{{$item->vTitle}}</div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

@if(session('locale') == 'zh-cn')
    <div class="modal fade" id="riskDesc" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">风 险 说 明</div>
                    <div class="descS">阅读本发行者编制的白皮书文档时，请仔细阅读以下公告。此公告适用于所有阅读本文档人员。<br><br>(1) 本文文件只用于对要求了解方案信息之特定对像传达信息使用，并不构成未来任何投资指导意见，也不是任何形式上的合约或承诺。<br>(2)
                        参与者一旦参与本发行方案，即表示了解并接受本发行方案风险，并愿意个人为此承担一切相应后果。<br>(3) 本发行方案明确表示不承诺任何回报，不承担任何方案造成直接或间接损失。<br>(4)
                        本发行方案之Token是一个在交易环节中使用的加密数字编码，不代表项目公司之股权、资产所有权、公司债，或任何型态之收益权或控制权。<br>(5)
                        由于加密数字货币本身存在很多不确定性(包括但不限于：各国对待加密数字货币监管的大环境、行业激励竞争及加密数字货币本身的技术漏洞)，我们无法保证本发行方案一定能够成功，本发行方案有一定的失败风险，所发行之Token也有归零的风险。<br>(6)
                        虽然团队会努力解决方案推进过程中可能遇到的问题，但未来依然存在政策不确定性，参与者务必在支持之前了解加密数字货币的风险前提下理性参与。<br><br>免 责 声 明<br><br>(1) 购买加密数字货币时，请详阅本白皮书。<br>(2) 本白皮书仅为Huafu Enterprise Holdings
                        Limited对ALLN之ICO所准备。<br>(3) 依照本白皮书，公司股份及其他证券在任何司法管辖权内不被要约认购或出售。<br>(4) 白皮书不构成任何人认购或购买本司股份、资产、权利或任何其他具有证券性质之要约或邀请。<br>(5)
                        投资一定有风险，中华人民共和国人且居住于中国境内者、美国国籍及台湾籍人士不得购买。<br><br>前 瞻 性 陈 述<br><br>(1)
                        白皮书中的一些陈述，包括了商业战略和未来计划观点的前瞻性陈述，此陈述针对ALLN团队（下简称本团队）所经营的社群、团队、部门、组织、机构和行业，陈述中类似预期、预估、希望、期望、打算、规划、计划、相信、坚信、项目、方案、预计、将要、将、目标、宗旨、可能、愿意、可以、可、开启、继续的字眼，均是对未来性或前瞻性的陈述或描述。<br>(2)
                        所有前瞻性陈述都涉及相关风险和不确定因素。<br>(3) 因此存在或将会有其他因素导致实际业绩与本报告所述结果大不相同。<br>(4) 白皮书中的任何前瞻性陈述反映了本团队对于未来事件的当前观点。<br>(5) 这些前瞻性陈述仅以截至白皮书作出当日为准。<br>(6)
                        根据行业可接受信息披露和透明度规则及其行业惯例，无论是由于新信息、未来发展还是其他原因，本团队不承担公开更新或审查任何前瞻性陈述的责任。<br>(7) ALLN潜在购买者在作出购买决定之前，应详阅本白皮书，并了解风险因素。并且白皮书中的任何声明都不应被解释为本团队对当前或未来几年盈利的暗示。
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="riskDesc" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">Risk Disclosure Statement</div>
                    <div class="descS">
                        <strong>Risk Disclosure Statement</strong><br>
                        When reading the white paper prepared by the Issuer, please consult the following statement carefully. This statement applies to all who read this document.<br><br>
                        <ol>
                            <li>This document is only for the purpose of conveying the information to specific readers who require knowledge of the information regarding the plan.
                                It does not accomplish the constituent elements of any future investment advice, nor is it any form of contract or commitment.
                            </li>
                            <li>Any participant that gets involved in the issuance plan comprehends and accepts the risks of the given plan, and is willing to bear all the
                                according consequences personally.
                            </li>
                            <li>The issuance plan distinctly states that it does not promise any return, and does not assume any direct or indirect losses caused by any project or
                                plan.
                            </li>
                            <li>The Token of this issuance plan is an encrypted digital code used in the transactions. It does not represent the equity, asset ownership, corporate
                                debt, or any type of income rights or control rights, of the Issuer or Project Company.
                            </li>
                            <li>Since many uncertainties exist in the cryptocurrency itself (including but not limited to: the environment for the administration of the
                                cryptocurrency in various countries, incentive competition among industries, and the technical loopholes in the cryptocurrency itself), we do not
                                promise that this issuance plan will surely be successful. This issuance plan contains a certain degree of failure risk, and the Token issued also
                                involves a risk of returning to zero.
                            </li>
                            <li>Although the team will work hard to solve the problems that may be encountered in the process of the plan or project, policy uncertainties will
                                still exist in the future. Participants must understand the risks of the cryptocurrency before rationally involving and supporting it.
                            </li>
                        </ol>
                        <br>
                        <strong>Disclaimer</strong><br>
                        <ol>
                            <li>Please consult this white paper before purchasing the digital currency.</li>
                            <li>This white paper is prepared for ALLN's ICO by Huafu Enterprise Holdings Limited.</li>
                            <li>Pursuant to this White Paper, no company shares or other securities are subject to the solicitation for subscription or sale in any jurisdiction.
                            </li>
                            <li>The White Paper does not does not accomplish the constituent elements of an offering or solicitation to subscribe or purchase the shares, assets,
                                rights or any other securities-related products.
                            </li>
                            <li>The participate is required to provide the Ethereum ERC20 cryptocurrency wallet. The own responsibility of participate for the loss if provides the
                                wrong wallet.
                            </li>
                            <li>The investment must be involved in a certain degree of risk, and in this case, the purchasers who are citizens of the People's Republic of China,
                                residing in China, or possessing US or Taiwan citizenship are excluded.
                            </li>
                        </ol>
                        <br>
                        <strong>Forward-Looking Statement</strong><br>
                        <ol>
                            <li>Some statements in the white paper contain forward-looking statements on the perspectives of business strategies and plans for the future. With
                                regard to the communities, teams, departments, organizations, institutions, and industries operated by the ALLN team (hereinafter referred to as
                                “the Team”), the words, of which the meaning is similar to anticipations, estimates, hopes, expectations, arrangements, plans, intentions,
                                suppositions, beliefs, projects, proposals, predictions, will, be ready to, goals, objectives, possibilities, willingness, availability,
                                feasibility, opening, and continuation, provide a future or forward-looking statement or description.
                            </li>
                            <li>All forward-looking statements involve relevant risks and uncertainties.</li>
                            <li>Therefore, other factors have existed or will emerge that cause the significant discrepancy between the actual performance and the results stated in
                                this report.
                            </li>
                            <li>Any forward-looking statement in the White Paper reflects the current views of the Team on future events.</li>
                            <li>These forward-looking statements are based on current plans and estimates, and speak only as of the date they are made.</li>
                            <li>In light of industrially accepted information disclosure and transparency rules as well as the according industry practices, the Team undertakes no
                                obligations to update or revise any forward-looking statement whether it is due to new information, future events or other causes.
                            </li>
                            <li>Prior to making a purchase decision, perspective buyers of the ALLN should read this white paper and know their risk factors.</li>
                            <li>No statement in the white paper should be interpreted as an indication from the Team on the profitability for the current or future years.</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(session('locale') == 'zh-cn')
    <div class="modal fade" id="issueProgram" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">发 行 方 案</div>
                    <div class="descS">
                        <table class="table">
                            <tr>
                                <td colspan="2">代币名</td>
                                <td>Airline and Life Networking Token</td>
                            </tr>
                            <tr>
                                <td colspan="2">代币代码</td>
                                <td>ALLN</td>
                            </tr>
                            <tr>
                                <td colspan="2">发行管理者</td>
                                <td>Huafu Enterprise Holdings Limited</td>
                            </tr>
                            <tr>
                                <td colspan="2">代币类型</td>
                                <td>Ethereum ERC20</td>
                            </tr>
                            <tr>
                                <td colspan="2">接受入金</td>
                                <td>以太币(ETH)、刷卡(现金)</td>
                            </tr>
                            <tr>
                                <td colspan="2">总代币数量</td>
                                <td>10,000,000,000枚ANL。(永不增发)</td>
                            </tr>
                            <tr>
                                <td rowspan="6">本<br>次<br>IC<br>O</td>
                                <td>发行数量</td>
                                <td>3,500,000,000枚 ALLN。</td>
                            </tr>
                            <tr>
                                <td>代币发行价格</td>
                                <td>1枚 ALLN = 0.1美元。(ICO 期间)</td>
                            </tr>
                            <tr>
                                <td>公开发行</td>
                                <td>2,275,000,000枚(65%)</td>
                            </tr>
                            <tr>
                                <td>原始股东团队</td>
                                <td>525,000,000枚(15%)</td>
                            </tr>
                            <tr>
                                <td>远东航空</td>
                                <td>350,000,000枚(10%)</td>
                            </tr>
                            <tr>
                                <td>储备Token</td>
                                <td>350,000,000枚(10%)</td>
                            </tr>
                            <tr>
                                <td colspan="2">代币合作交易所</td>
                                <td>主流交易所</td>
                            </tr>
                        </table>
                        <br>
                        (1) 本次ICO软帽(Soft Cap，即发行成功最低门槛)：<br>I. 本次ICO公开发行数量的20%，即455,000,000枚。<br>II. 发行认购数量未能达软帽时，扣除相关税款及费用后，认购金依原路径退回。<br><br>(2) 锁定期：<br>I.
                        发行之ALLN于ICO完成结算日起原始股东团队、远东航空开始进行为期1年的锁定期。<br>II. 锁定期间，不允许销售、转移Token。<br><br>未 发 行 Token 之 运 用 说
                        明<br><br>本次保留尚未对外发行的65%总发行数量(即6,500,000,000枚)ALLN将于未来再进行公开发行。<br>而后续发行之目的在持续达成阶段性购置新机队、周边资产及ALLN推广应用上，以促进远东航空在扩增航线、提升飞航服务及加密数字货币于实体消费中普及化的核心目标。
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="issueProgram" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">Issuing and Offering Plan</div>
                    <div class="descS">
                        <strong>Initial Offering Plan</strong>
                        <table class="table">
                            <tr>
                                <td colspan="2">Token Name</td>
                                <td>Airline and Life Networking Token</td>
                            </tr>
                            <tr>
                                <td colspan="2">Token Code</td>
                                <td>ALLN</td>
                            </tr>
                            <tr>
                                <td colspan="2">Issuer and Administrator</td>
                                <td>Huafu Enterprise Holdings Limited</td>
                            </tr>
                            <tr>
                                <td colspan="2">Type of Token</td>
                                <td>Ethereum ERC20</td>
                            </tr>
                            <tr>
                                <td colspan="2">Acceptable Deposits</td>
                                <td>Ethereum(ETH)、Pay by Credit Card(Cash)</td>
                            </tr>
                            <tr>
                                <td colspan="2">Total Number of Issued Tokens</td>
                                <td>10,000,000,000 tokens of ALLN. (No Additional Issuance)</td>
                            </tr>
                            <tr>
                                <td rowspan="6">Initial<br>Offering<br>Plan</td>
                                <td>Number of Issued Tokens</td>
                                <td>3,500,000,000 tokens of ALLN.</td>
                            </tr>
                            <tr>
                                <td>Issue Price of Token</td>
                                <td>1 ALLN Coin= 0.1USD。(During the ICO)</td>
                            </tr>
                            <tr>
                                <td>Public Offering</td>
                                <td>2,275,000,000 tokens (65%)</td>
                            </tr>
                            <tr>
                                <td>Original Team</td>
                                <td>525,000,000 tokens (15%)</td>
                            </tr>
                            <tr>
                                <td>FAT</td>
                                <td>350,000,000 tokens (10%)</td>
                            </tr>
                            <tr>
                                <td>Reserve of Token</td>
                                <td>350,000,000 tokens (10%)</td>
                            </tr>
                            <tr>
                                <td colspan="2">Cooperative Exchange</td>
                                <td>Mainstream Exchange</td>
                            </tr>
                        </table>
                        <br>
                        (1) The soft cap of this ICO case (Soft Cap, the lowest threshold of successful issuing and offering):<br>
                        <ol type="I">
                            <li>Twenty percent (20%) of the quantity of public offerings for the ICO case, ie 455,000,000 tokens.</li>
                            <li>In the case that the subscription amount of the issuance and offering case fails to reach the soft cap, the amount of subscription payment deducting
                                relevant taxes and fees will be refunded via the original route.
                            </li>
                        </ol>
                        <br>
                        (2) Lock-up period:<br>
                        <ol type="I">
                            <li>The original shareholder and FAT are subject to a lock-up period of a year from the settlement and completion date of the ICO of the ALLN Tokens.
                            </li>
                            <li>Sales and transfer of the tokens are not allowed during lock-up period.</li>
                        </ol>
                        <br>
                        <strong>Arrangement for the Unissued Token</strong><br>
                        An amount of 65% of the total issued number of ALLN Tokens (ie, 6,500,000,000 tokens) have been unissued and preserved for future issuance. The purpose of
                        follow-on issuance is to consecutively fulfill the phased acquisition of new fleets, peripheral assets, and to promote the application of ALLN, in order to
                        fulfill the objectives of facilitating FAT to expand its aviation routes and improve flight services and popularizing the application of cryptocurrencies in
                        real consumption.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(session('locale') == 'zh-cn')
    <div class="modal fade" id="privacyDesc" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">隐 私 权 条 款</div>
                    <div class="descS">非常欢迎您光临「allntoken.io」（以下简称ALLN），为了让您能够安心使用ALLN的各项服务与资讯 ，特此向您说明ALLN的隐私权保护政策，以保障您的权益，请您详阅下列内容：<br><br>一、隐私权保护政策的适用范围<br>
                        隐私权保护政策内容，包括ALLN如何处理在您使用网站服务时收集到的个人识别资料。隐私权保护政策不适用于ALLN以外的相关连结网站，也不适用于非ALLN所委托或参与管理的人员。<br><br>二、个人资料的搜集、处理及利用方式<br>
                        <ul>
                            <li>当您造访ALLN或使用ALLN所提供之功能服务时，我们将视该服务功能性质，请您提供必要的个人资料，并在该特定目的范围内处理及利用您的个人资料，非经您书面同意，ALLN不会将个人资料用于其他用途。</li>
                            <li>ALLN在您使用时，会保留您所提供的姓名、电子邮件地址、联络方式及使用时间等。</li>
                            <li>于一般浏览时，伺服器会自行记录相关行为，包括您使用连线设备的IP位址、使用时间、使用的浏览器、浏览及点选资料记录等，做为我们增进网站服务的参考依据，此记录为内部应用，决不对外公布。</li>
                            <li>为了使用我们的服务，特别是注册程序，您必须先输入您的电子邮件地址和居住国家并选择密码进行注册。</li>
                            <li>在注册过程中，您将被要求输入个人资料。如果需要完成令牌销售，我们可能会将您的个人数据转移给其他服务提供商。这包括向KYC（“了解您的客户”）和支付服务提供商所必需的数据传输。</li>
                            <li>我们收集您填写在线表格的数据，如联系表格。您提供给我们的信息将记录在我们的IT系统中。</li>
                        </ul>
                        <br>
                        三、资料之保护<br>
                        <ul>
                            <li>ALLN主机均设有防火墙，防毒系统等相关的各项资讯安全设备及必要的安全防护措施，加以保护网站及您的个人资料采用严格的保护措施，只由经过授权的人员才能接触您的个人资料，相关处理人员皆签有保密合约，如有违反保密义务者，将会受到相关的法律处分。</li>
                            <li>如因业务需要有必要委托其他单位提供服务时，ALLN亦会严格要求其遵守保密义务，并且采取必要检查程序以确定其将确实遵守。</li>
                        </ul>
                        <br>
                        四、网站对外的相关连结<br>ALLN的网页提供其他网站的网路连结，您也可经由ALLN所提供的连结，点选进入其他网站。但该连结网站不适用ALLN的隐私权保护政策，您必须参考该连结网站中的隐私权保护政策。<br><br>五、与第三人共用个人资料之政策<br>
                        ALLN绝不会提供、交换、出租或出售任何您的个人资料给其他个人，团体、私人企业或公务机关，但有法律依据或合约义务者，不在此限。<br>前项但书之情形包括不限于：<br>
                        <ul>
                            <li>经由您书面同意。</li>
                            <li>法律明文规定。</li>
                            <li>为免除您生命、身体、自由或财产上之危险。</li>
                            <li>与公务机关或学术研究机构合作，基于公共利益为统计或学术研究而有必要，且资料经过提供者处理或搜集着依其揭露方式无从识别特定之当事人。</li>
                            <li>当您在网站的行为，违反服务条款或可能损害或妨碍网站与其他使用者权益或导致任何人遭受损害时，经网站管理单位研析揭露您的个人资料是为了辨识，联络或采取法律行动所必要者。</li>
                            <li>有利于您的权益。</li>
                            <li>ALLN委托厂商协助搜集、处理或利用您的个人资料时，将对委外厂商或个人善尽监督管理之责。</li>
                        </ul>
                        <br>
                        六、Cookie之使用<br>
                        为了提供您最佳的服务，ALLN会在您的电脑中放置并取用我们的Cookie，若您不愿接受的Cookie的写入，您可在您使用的浏览器功能项中设定隐私权等级为高，即可拒绝Cookie的写入，但可能会导至网站某些功能无法正常执行。<br><br>
                        七、Google Analytics之使用<br>
                        本网站使用Google Analytics（一种由Google，Inc.（“Google”）提供的网络分析服务）。 Google
                        Analytics使用Cookie。由cookie生成的关于您使用本网站（包括您的IP地址）的信息将被传输至Google并存储在美国的服务器上。Google将使用这些信息来评估您对网站的使用情况，为网站运营商编制网站活动报告以及提供与网站活动和互联网使用有关的其他服务。Google也可能会根据法律要求将此信息转移给第三方，或者由第三方代表Google处理信息。Google不会将您的IP地址与Google持有的任何其他数据相关联。有关使用条款和条件以及数据隐私的更多信息，请访问<a
                                href="http://www.google.com/analytics/terms/gb.html">http://www.google.com/analytics/terms/gb.html</a>。<br><br>
                        八、访问和信息的权利<br>
                        <ul>
                            <li>有关处理您的数据的任何问题，请通过以下地址与我们联系：service@allntoken.io</li>
                            <li>如果您希望通过电子邮件与我们联系，请注意，电子邮件将采用未加密的形式，因此易受通常与此类通信方法相关的安全风险的影响。</li>
                        </ul>
                        <br>
                        九、修改，阻止和删除/撤销同意<br>
                        <ul>
                            <li>如果您希望修改，封锁或删除任何数据，请以书面或电子邮件的形式与我们联系，并附上护照或身份证复印件，地址如下：service@allntoken.io</li>
                            <li>如果由于法定或合同原因或商业法或税法不允许删除任何数据，特别是法定保留期，数据将被封锁而不是删除。</li>
                        </ul>
                        <br>
                        十、隐私权保护政策之修正<br>
                        ALLN隐私权保护政策将因应需求随时进行修正，修正后的条款将刊登于网站上。
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="privacyDesc" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">Privacy policy</div>
                    <div class="descS">Welcome to "allntoken.io" (hereinafter referred to as ALLN). In order to enable you to use ALLN's services and information with confidence,
                        we hereby inform you of ALLN's privacy protection policy to protect your rights. Please read the following content carefully:<br><br>
                        1、The Scope of the Privacy Protection Policy<br>
                        The content of the Privacy Protection Policy includes how the personal identification data collected during your utilization of ALLN website will be
                        processed. The Privacy Protection Policy does not apply to related external websites linked to ALLN Website and individuals who are not entrusted or
                        involved in the management of ALLN.<br><br>
                        2、The Collection of Personal Information<br>
                        <ul>
                            <li>The ALLN website will request you to fill in some personal information when you visit the ALLN website or use some ALLN specific services, and only
                                handles and uses your personal data in particular purpose. ALLN will not use your personal data on other purpose, unless ALLN acquires your
                                permission to do so or the law demands ALLN to do so.
                            </li>
                            <li>The ALLN website server generates the record when you are using any service (e.g. your name, Email address, contact information and the time of your
                                use, etc.)
                            </li>
                            <li>The ALLN server generates the record by itself: when you are browsing the website (including the IP address of your connecting device, the time of
                                your use, the browser you use and the record about items you browse and click on), and only uses the record as a reference for enhancing our
                                website. The record will only be used internally will never publicize.
                            </li>
                            <li>In order to use ALLN services, the visitor needs to fill in e-mail address, country of residence and choose a password to register.</li>
                            <li>In the course of the registration, you will be asked to input the personal information. If it is necessary to complete the sale of the token, we may
                                transfer your personal data to other service provider, including the transfer of data to KYC (“Understand your customers”) and payment services
                                providers.
                            </li>
                            <li>ALLN will collects the information that you fill in the website (e.g. contact information), and records in IT system.</li>
                        </ul>
                        <br>
                        3、Privacy Protection<br>
                        <ul>
                            <li>All the hosts of the ALLN website are equipped with various kinds of equipment of information security and necessary measures of safety protection
                                as firewalls, anti-virus systems, etc. Strict protection measures are taken for securing the website and your personal data. Only authorized
                                personnel can access your personal data, and relevant processing personnel all signed with a treaty of confidentiality.
                            </li>
                        </ul>
                        <br>
                        4、Link to other website<br>
                        <ul>
                            <li>ALLN website provides links of other websites to visitors to entry. However, those websites does not adopt to ALLN privacy policy, visitors must
                                read the privacy statements posted on those websites.
                            </li>
                        </ul>
                        <br>
                        5、The Sharing and Using of Data<br>
                        ALLN will absolutely not provide, exchange, rent or sell any of your personal data to other individuals, groups, enterprises or government organizations
                        unless the Website acquires your permission to do so or the law demands the website to do so.<br>
                        <ul>
                            <li>With your written consent.</li>
                            <li>With the provisions of laws or regulations.</li>
                            <li>To prevent the dangers of your life, body, freedom or property.</li>
                            <li>To cooperate with a government organization or a research institute that it is necessary for statistics or an academic research based on public
                                interests and that the data is processed by the provider or those collecting information depending on the manner of disclosure so that there has
                                been no way to identify the interested party.
                            </li>
                            <li>Where your behaviors on the website breach the terms of services, or may damage or hinder this website and other users’ rights and interests or
                                cause a third party losses, the administrator of the website will disclose your personal data necessary for identifying, contacting or taking a
                                legal action.
                            </li>
                            <li>Benefit to your rights and interests.</li>
                            <li>ALLN will be liable to supervise a factor or a individual authorized as a good administrator when authorizing the factor to collect, handle or use
                                your personal information.
                            </li>
                        </ul>
                        <br>
                        6、Use of Cookie<br>
                        In order to provide good services to you, ALLN will put our Cookie in your computer and take our Cookie for use. If you do not want to receive the Cookie,
                        you can choose to block Cookies by updating your web browser privacy settings to a high level. However, if you block cookies, it may cause some
                        functionality of the services on the website unable to perform.<br><br>
                        7、Use of Google Analytics<br>
                        This website uses Google Analytics (a service for analysis offered by Google, Inc. (“Google”)). Google Analytics uses Cookies. The information generated by
                        the cookies about your use of our website, including your IP address, will be transmitted to and keep in Google servers in the United States. Google will
                        use the information generated by the cookies for the purpose of evaluating the use of our website, compiling reports on website activity providing us with
                        these reports for analytical purposes and providing other services related to the activities on the website and the use of the Internet. Google may transfer
                        this information to third parties in case of a statutory obligation or if a third party processes data on behalf of Google. Under no circumstances, Google
                        will combine or associate your IP address with other data stored at Google. For more information about the terms and conditions of services the privacy of
                        data, please visit <a href="http://www.google.com/analytics/terms/gb.html">http://www.google.com/analytics/terms/gb.html</a>.<br><br>
                        8、Right to visit and information<br>
                        <ul>
                            <li>If you have any questions about the data processing, Please contact us through the following address: service@allntoken.io</li>
                            <li>If you would like to contact us by e-mail, you should be noted that by sending e-mail messages which are not encrypted, you will undertake the risk
                                of such uncertainty and possible lack of security over the Internet.
                            </li>
                        </ul>
                        <br>
                        9、Modify, block and delete/ withdraw agreement<br>
                        <ul>
                            <li>If you would like to modify, block or delete any data, please contact us in writing, or by an e-mail with a copy of passport or ID card through the
                                following e-mail address: service@allntoken.io
                            </li>
                            <li>If any data may not be granted to be deleted due to a statutory reason or an agreement or according to a business act or a tax act, especially the
                                statutory period, the data will be blocked, not deleted.
                            </li>
                        </ul>
                        <br>
                        10、Policy Amendment<br>
                        ALLN’s privacy policy will be amended from time to time if necessary, and ALLN will update the latest version on the page of the website.
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
@if(session('locale') == 'zh-cn')
    <div class="modal fade" id="serviceDesc" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">服务条款</div>
                    <div class="descS">
                        本协议详述您在Airline and Life Networking（以下简称「ALLN」）网站或系统使用服务功能所须遵守的条款和条件。ALLN网站或系统的所有权和运营权归ALLN所有。如您有任何疑问，请及时联系本公司。<br><br>您只有完全同意下列所有服务条款并完成注册程序，才能成为ALLN网站或系统功能的使用者并使用相应服务。您注册成为ALLN网站或系统的使用者前，必须仔细阅读本使用者协定和隐私权政策，一旦您注册成功即表示您已经阅读，同意并接受本使用者协议和隐私权政策中所含的所有条款和条件。<br><br>您只要使用了ALLN网站或系统提供的服务，即视为您已了解并完全同意本服务条款各项内容，包括ALLN网站或系统对服务条款随时做的任何修改。如您不同意该修订，您必须终止您与ALLN系统或网站的会员用户关系。<br><br>
                        1、服务内容<br>
                        <ul>
                            <li>ALLN网站或系统提供访问了解ALLN的基本资讯、服务内容，并让会员使用者可试用甚至产生自己所需的网路商务功能。</li>
                            <li>ALLN网站或系统的具体服务内容由本公司根据实际情况提供具体功能与服务。本公司保留变更，调整或终止部分服务内容的权利。</li>
                            <li>本公司保留根据实际情况随时调整ALLN网站或系统提供的服务种类、形式的权利。本公司不承担因业务调整给用户造成的损失。</li>
                        </ul>
                        <br>
                        2、隐私保护<br>
                        <ul>
                            <li>保护用户隐私是ALLN网站或系统的重点原则，本公司通过技术手段，提供隐私保护服务功能，强化内部管理等办法充分保护用户的个人资料安全。</li>
                            <li>本公司保证不对外公开或向第三方厂商提供用户注册的个人资料，以及使用者在使用服务时存储的非公开内容。具体请参考“隐私权政策”。</li>
                        </ul>
                        <br>
                        3、服务准则<br>
                        <ul>
                            <li>使用者在申请使用ALLN网站时，必须提供真实的个人资料，并且不断实实更新注册资料。如果因注册资讯不真实而引起的问题及其后果，ALLN不承担任何责任。</li>
                            <li>使用者在使用ALLN服务过程中，必须遵循国家的（当地的）相关法律法规，不得使用ALLN网路服务功能，发布危害国家安全、色情、暴力等非法内容;不得利用ALLN网路服务功能发布含有虚假、有害、胁迫、侵害他人隐私、骚扰、侵害、中伤、粗俗、或其它道德上令人反感的内容。</li>
                            <li>使用者使用本服务的行为若有任何违反国家法律法规或侵犯任何协力厂商的合法权益的情形时，本公司有权直接删除该等违反规定之内容。</li>
                            <li>除非使用者已获得第三方厂商授权，否则不得将第三方厂商内容用于商业目的。</li>
                            <li>使用者不可以通过自动程式等方式批量创建多个帐户，也不可以对帐户使用自动系统执行操作。</li>
                            <li>使用者影响系统总体稳定性或完整性的操作可能会被暂停或终止使用权利，直到问题得到解决。</li>
                            <li>用户不可违规或恶意使用ALLN网站或系统的开放介面，否则本公司有权随时终止该使用者服务。如用户由于不当违反本公司造成损失，用户应予赔偿。</li>
                        </ul>
                        <br>
                        5、智慧财产权声明<br>
                        <ul>
                            <li>
                                ALLN所有提供的网路服务的相关着作权、专利权、商标权、商业秘密及其它任何所有权或权利，均属本公司所有。非经本公司的同意，任何人或用户均不得擅自进行下载、复制、传输、改编、编辑等任何侵犯本公司利益的行为，否则应负所有法律责任。本公司拥有向使用者提供网路服务过程中所产生并储存于ALLN伺服器中的任何资料资讯的所有权。使用者不得在未获得本公司允许的情况下，私自通过不法途径获取软体原始程式码。软体原始程式码归本公司所有。
                            </li>
                        </ul>
                        <br>
                        6、免责声明<br>
                        <ul>
                            <li>
                                互联网（网际网路）是一个开放平台，使用者将照片等个人资料上传到互联网上，有可能会被其他组织或个人复制，转载，删除或做其它非法用途，使用者必须充分意识此类风险的存在使用者明确同意其使用ALLN网站或系统服务所存在的风险将完全由其自己承担;因其使用ALLN服务而产生的一切后果也由其自己承担，本公司对用户不承担任何责任。
                            </li>
                            <li>ALLN网站或系统不保证服务一定能满足所有使用者的要求，因架设于云端服务器上，也不保证服务不会中断，对服务的及时性，安全性，准确性也都对应不作绝对保证。对于非因ALLN自身原因造成的网路服务中断或其他缺陷，本公司不承担任何责任。</li>
                            <li>使用者在软体制作中若因侵犯他人智慧财产权而引发纠纷，所有责任由用户自行承担，本公司不承担任何责任。</li>
                        </ul>
                        <br>
                        7、服务变更，中断或终止<br>
                        <ul>
                            <li>如因系统维护或升级而需暂停网路服务，服务功能调整，ALLN将尽可能事先在网站上进行通告。</li>
                            <li>如发生下列任何一种情形，本公司有权单方面中断或终止向使用者提供服务而无需通知使用者;
                                <ul>
                                    <li>使用者提供的个人资讯不真实;</li>
                                    <li>使用者违反本服务条款中规定的使用与服务规则;</li>
                                    <li>未经本公司同意，将ALLN网站或系统用于非法目的;</li>
                                    <li>用户违反智慧财产权声明，做出侵犯本公司合法权益的行为时。</li>
                                </ul>
                            </li>
                        </ul>
                        <br>
                        8、特别约定<br>
                        <ul>
                            <li>使用者使用本服务的行为若有任何违反国家法律法规或侵犯任何第三方厂商的合法权益的情形时，本公司有权直接删除该等违反规定的资讯，并可以暂停或终止向该使用者提供服务。</li>
                            <li>若使用者利用本服务从事任何违法或侵权行为，由使用者自行承担全部责任，因此给本公司或任何协力厂商造成任何损失，用户应负责全额赔偿。</li>
                            <li>本公司可以自行在使用者软件访问介面上增加频道或栏目等资讯，使用者同意接受本公司的此一安排，而本公司亦应尽可能不让此一安排对使用者软体的使用造成负面影响。</li>
                        </ul>
                        <br>
                        9、争议解决方式<br>
                        <ul>
                            <li>凡因本合同引起的或与本合同有关的任何争议，均应提交相关仲裁与法律诉讼单位，按照申请仲裁时该该现行有效的仲裁规则进行仲裁。仲裁裁决是终局的，对双方均有约束力。</li>
                        </ul>
                        <br>
                        10、法律适用<br>
                        <ul>
                            <li>与本协议有关的所有事宜适用用户所在地的法律。</li>
                        </ul>
                        <br>
                        11、协议修改和解释<br>
                        <ul>
                            <li>本公司会有权依照服务器所在地政府有关法律法规和互联网的发展，不时地修改本公司服务条款。委托人在使用本公司平台服务时，可能会被提示对最新的本公司服务条款进行重新确认，当发生有关争议时，以最新的服务协定为准。</li>
                            <li>本协议条款的最终解释权归本公司所有。</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="modal fade" id="serviceDesc" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="title">Terms of service</div>
                    <div class="descS">
                        These terms specify the terms and conditions you shall obey when using the services on the website or in the system of Airline and Life Networking (“ALLN”).
                        The ownership and operation concession of the website or system of ALLN belong to ALLN. If you have any questions, please contact us in time.<br><br>You may
                        only become a user of the website or system of ALLN and use the services on the website or in the system by entirely agreeing the following terms of service
                        and completing the registration process. You must read this terms and the privacy policy carefully before registering to become a user of the website or
                        system of ALLN. Once registering successfully, it signifies that you have read, agreed and accepted all of the terms and conditions of this agreement and
                        privacy policy.<br><br>Once using the services in the website or the system of ALLN, it implies that you have comprehended and agreed to these terms of
                        service, including the amendment of these terms from time to time. If you disagree to the amendment of these terms, you have to terminate your membership
                        with the system or website of ALLN.<br><br>
                        1、Contents of service<br>
                        <ul>
                            <li>The website or system of ALLN provides access service to understand the basic information and services of ALLN, even to generate functions of online
                                business in needs.
                            </li>
                            <li>Our company will base on the practical condition to provide the specific functions and services in the website or system of ALLN and reserve the
                                right to modify, amend or terminate any part of services.
                            </li>
                            <li>According to the condition, our company reserves the right to modify the kind and form of the services in the website or system of ALLN. Our company
                                does not assume users’ losses arising from any modification hereunder.
                            </li>
                        </ul>
                        <br>
                        2、Privacy policy<br>
                        <ul>
                            <li>The main principle of the website or system of ALLN is to protect users’ privacy. We provide the function of the protection of privacy through
                                technology and strengthen our internal management adequately to protect the securities of users’ personal information.
                            </li>
                            <li>Our company guarantees that we do not provide users’ personal data for registration and the non-disclosure contents stored when user uses the
                                services to a third party. Please refer to the “Privacy Policy” for the specific contents.
                            </li>
                        </ul>
                        <br>
                        3、Standard of service<br>
                        <ul>
                            <li>A user shall provide the true personal data when applying for use the website of ALLN and update the data for registration from time to time. ALLN
                                will not be response for any problems and results arising from the untrue information for registration.
                            </li>
                            <li>A user must obey the relevant laws of the located country in the course of using ALLN’s services. A user may not use the services on the website of
                                ALLN to publish illegal contents, such as to jeopardize the safety of country, about eroticism or violence and may not use the services on the
                                website of ALLN to publish the information containing false, harmful, threatening, infringing a third party’s privacy, harassed, violate, defaming,
                                vulgar contents or other morally offensive content.
                            </li>
                            <li>We have the right to delete directly the contents breached hereunder where a user violates the laws of the country or infringes the legal interests
                                of our partners when using our services.
                            </li>
                            <li>A user may not use a third party’s contents for the purpose of business, unless otherwise obtaining the partner’s authorization.</li>
                            <li>A user may not create several accounts by the manner of through automatic programs and may not use automatic system to operate the account.</li>
                            <li>A user affecting the overall stability or integrity of operation of the system may be suspended or terminated the right of use until the problem is
                                resolved.
                            </li>
                            <li>A user may not violate these terms or malevolently use the open interface of the website of ALLN; otherwise we have the right to terminate services
                                for such user at any time. A user shall be liable to compensate our losses due to the user’s breach hereof.
                            </li>
                        </ul>
                        <br>
                        5、Intellectual property<br>
                        <ul>
                            <li>The copyright, patents, trademarks, trade secret and other ownership or rights related to the online services provided by ALLN belong to us. Without
                                our prior consent, anyone or any user may not have any behavior, such as downloading, copying, transfer, adaptation and editing, etc. to infringe
                                our interests, otherwise he/she will be liable for all legal responsibilities. We own the ownership right of data and information generated and
                                stored in the ALLN’s server in the course of providing the online services to users. A user may not obtain the source code of the software through
                                illegal way without our grant. Such source code of the software belongs to us.
                            </li>
                        </ul>
                        <br>
                        6、Disclaimers<br>
                        <ul>
                            <li>The Internet is an open platform. A user uploads the personal data, such as photos, on the Internet, these data may be copied, transferred, deleted
                                or used to other illegal purpose by other organization or individual. Thus, a user must be fully aware of such risk and actually agree to use ALLN’s
                                services at the user’s own risk. The user shall assume for any results arising from the use of ALLN’s services, and our company does not assume any
                                responsibilities for any user.
                            </li>
                            <li>Our company does not guarantee that the services provided on the website or in the system of ALLN will be satisfied every user’s request and that
                                the services will be uninterrupted and the services will be instant, safe and accurate because the services are stored on the cloud. Our company
                                does not assume any responsibilities for any interruption or other faults in the online services not due to ALLN’s causes.
                            </li>
                            <li>A user will be liable for any disputes arising from the infringement of a third party’s intellectual property rights when creating software and we
                                do not assume any responsibilities concerned.
                            </li>
                        </ul>
                        <br>
                        7、Change, interruption or termination of service<br>
                        <ul>
                            <li>If the online services need to be suspended due to maintenance or upgrade of the system, ALLN will give prior notice of the change of the services
                                on the website as far as possible.
                            </li>
                            <li>If any of the following circumstances happens, we have the right to interrupt or terminate unilaterally the services provided to users without
                                notifying users:
                                <ul>
                                    <li>The personal information provided by a user is not true;</li>
                                    <li>A user breaches the terms of use and services hereof;</li>
                                    <li>A user uses the website or system of ALLN for the illegal purposes without our consent;</li>
                                    <li>A user has a behavior to breach the term of Intellectual Property and infringe our legal rights and interests.</li>
                                </ul>
                            </li>
                        </ul>
                        <br>
                        8、Specific agreement<br>
                        <ul>
                            <li>We have the right to delete directly the information breached hereunder and to suspend or terminate the services to a user where the user violates
                                the laws of the country or infringes the legal interests of a third party when using our services.
                            </li>
                            <li>If a user uses our services involving in any illegal or infringing behavior, the user will be liable for all responsibilities and also fully
                                compensate for our or any partner’s losses arising therefrom.
                            </li>
                            <li>Our company may increase information, such as a frequency channel or column, on the interface of software in our sole discretion, and users agree
                                and accept our arrangement, and we will not let this management to cause users’ use of software any negative impact as far as possible.
                            </li>
                        </ul>
                        <br>
                        9、Settlement of disputes<br>
                        <ul>
                            <li>Any dispute arising from or related to these terms will be settled by arbitration to an arbitrative or juridical entity according to the existing
                                arbitration law when the arbitration is filled, whose award shall be final and binding on both parties.
                            </li>
                        </ul>
                        <br>
                        10、Governing Law<br>
                        <ul>
                            <li>Any matter related hereto is governed by the laws of place where the user is located.</li>
                        </ul>
                        <br>
                        11、Amendment and interpretation of these terms<br>
                        <ul>
                            <li>We will update our terms of services from time to time in accordance with the laws of country where the server is located and the development of the
                                Internet. When an authorized person uses our services on the platform, our latest terms of services will be appeared for users to make sure again.
                                In case of disputes, the latest version shall always prevail.
                            </li>
                            <li>We have the right to the final interpretation of these terms.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
<div class="modal fade" id="weChat" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="title">微信QR code</div>
                <img src="/portal_assets/htm/img/S__17367255.jpg">
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="stepDesc" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="title">{{trans('aln.stepDesc.title')}}</div>
                @if(session('locale') == 'zh-cn')
                    <div class="descS">
                        <dl>
                            <dt>
                            <h4><strong>step1：</strong></h4></dt>
                            <dd>擇一選擇投資方式
                                <ol>
                                    <li>以太幣</li>
                                    <li>美金</li>
                                </ol>
                            </dd>
                            <br>
                            <dt>
                            <h4><strong>step2：</strong></h4></dt>
                            <dd>在畫面上試算"意願投資額度可得的ALLN Tokens"</dd>
                            <br>
                            <dt>
                            <h4><strong>step3：</strong></h4></dt>
                            <dd>勾選同意所有規範(風險說明、發行方案、隱私權條款、服務條款、白皮書)</dd>
                            <br>
                            <dt>
                            <h4><strong>step4：</strong></h4></dt>
                            <dd>送出投資意願後，系統將為您記錄投資意願歷史</dd>
                            <br>
                            <dt>
                            <h4><strong>step5：</strong></h4></dt>
                            <dd>系統將出現ALLN團隊用於收款的QRcode和鏈接(位址)。<br>若您用以太幣支付，請採用ERC20錢包掃描付款，若您採用信用卡線上付款，請開啟鏈接進行刷卡付款。<br>付款時請輸入實際要投金額<br><br>注意：您匯款所使用的錢包，請與收款錢包相同</dd>
                        </dl>
                    </div>
                @else
                <!-- 英文版 -->
                    <div class="descS">
                        <dl>
                            <dt>
                            <h4><strong>step1：</strong></h4></dt>
                            <dd>Choose one of the investment methods below:
                                <ol>
                                    <li>Ethereum</li>
                                    <li>USD</li>
                                </ol>
                            </dd>
                            <br>
                            <dt>
                            <h4><strong>step2：</strong></h4></dt>
                            <dd>ALLN Tokens"<br>Insert the amount that you’re willing to invest and see how many ALLN tokens you will get on the calculator.</dd>
                            <br>
                            <dt>
                            <h4><strong>step3：</strong></h4></dt>
                            <dd>Please check all the checkboxes if you agree to “Risk Disclosure Statement & Issuing”,“Offering Plan”,“Privacy Terms”,“Service Terms” and “White
                                Papers”.
                            </dd>
                            <br>
                            <dt>
                            <h4><strong>step4：</strong></h4></dt>
                            <dd>After sending, the system will record the investment history for you.</dd>
                            <br>
                            <dt>
                            <h4><strong>step5：</strong></h4></dt>
                            <dd>The QR code and the address for our team to receive your funding will be displayed below soon.if you’re paying with ETH, please use your ERC20
                                wallet to scan and pay; if you’re using a credit card, please click on the link and proceed to pay.Please insert the ALLN tokens amount that you
                                wish to purchase.<br><br>Please insert the ALLN tokens amount that you wish to purchase.
                            </dd>
                        </dl>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="/portal_assets/dist/package/bootstrap3/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/portal_assets/dist/package/fontAwesome/svg-with-js/js/fontawesome-all.min.js"></script>
<script type="text/javascript" src="/portal_assets/dist/package/particle/particles.min.js"></script>
<script type="text/javascript" src="/portal_assets/dist/package/particle/lib/stats.js"></script>
<!-- <script type="text/javascript" src="../dist/js/_theme.js"></script> -->
<script type="text/javascript" src="/portal_assets/htm/js/main.js"></script>
<!-- Sweet alert -->
<script src="/js/sweetalert/sweetalert.min.js"></script>
<!-- Toastr script -->
<script src="/js/toastr/toastr.min.js"></script>
<!-- ================== page-js ================== -->
@yield('page-js')
<!-- ================== /page-js ================== -->
<!-- Public var -->
@include('_template_portal._js.var')
<!-- end -->
<!-- Public commit -->
@include('_template_portal._js.commit')
<!-- end -->
<!-- ================== inline-js ================== -->
@yield('inline-js')
<!-- ================== /inline-js================== -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116168207-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-116168207-1');
</script>
</html>