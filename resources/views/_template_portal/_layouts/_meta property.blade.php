<meta property="og:url" content="{{$og['url'] or ""}}"/>
<meta property="og:type" content="{{$og['type'] or ""}}"/>
<meta property="og:title" content="{{$og['title'] or ""}}"/>
<meta property="og:description" content="{{$og['description'] or ""}}"/>
<meta property="og:image" content="{{$og['images'] or ""}}"/>