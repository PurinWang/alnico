<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navTarget" aria-expanded="false"><i class="fas fa-bars fa-2x"></i></button>
            <div id="logo"><a href="{{url('')}}"><img style="width:100%;" src="/portal_assets/htm/img/new logo.png" alt=""></a></div>
        </div>
        <div class="collapse navbar-collapse" id="navTarget">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="javascript:mClick('1');">{{trans('aln.nav.menu_1')}}</a></li>
                <!-- <li><a href="javascript:mClick('2');">{{trans('aln.nav.menu_2')}}</a></li> -->
                <li><a href="javascript:mClick('3');">{{trans('aln.nav.menu_3')}}</a></li>
                <li><a href="javascript:mClick('F');">{{trans('aln.nav.menu_F')}}</a></li>
                <li><a href="javascript:mClick('C');">{{trans('aln.nav.menu_C')}}</a></li>
                <li><a href="javascript:mClick('4');">{{trans('aln.nav.menu_4')}}</a></li>
                <li><a href="{{url('member')}}#p2"><i class="fas fa-user-circle"></i></a></li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{trans('aln.nav.lang')}}<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li class="btn-locale" data-locale="zh-cn"><a href="javascript:void(0);">简体中文</a></li>
                        <li class="btn-locale" data-locale="en"><a href="javascript:void(0);">English</a></li>
                    </ul>
                </li>
                <li class="buy"><a href="{{url('register')}}">Sign Up</a></li>
                @if(session('shop_member.iId'))
                    <li>
                        <a href="#" class="logout">
                            <i class="fas fa-sign-out-alt"></i>Sign Out
                        </a>
                    </li>
                @endif
            </ul>
        </div>
    </div>
</nav>