@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/invest.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">我要投資</div>
                    <div class="cubeBtn btn-report">
                        <div class="titleS">已經投資</div>
                        <div class="name blue">回傳投資結果</div>
                    </div>
                    <div class="cubeBtn btn-create">
                        <div class="titleS">我要投資</div>
                        <div class="name red">新增投資</div>
                    </div>
                    <div class="text">注意：進行投資前，請先完成會員中心個人資訊填寫。</div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            $(".btn-report").click(function () {
                location.href = "{{url('report')}}"
            })
            //
            $(".btn-create").click(function () {
                location.href = "{{url('invest_create')}}"
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->