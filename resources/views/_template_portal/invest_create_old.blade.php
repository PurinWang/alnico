@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/newInvestiment.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{{trans('invest_create.title')}}</div>
                    <div class="stepBtn"><a href="javascript:void(0);" data-toggle="modal" data-target="#stepDesc">{{trans('invest_create.stepBtn')}}</a></div>
                    <div class="todayVal"> 1 ETH = {{$ETH_Rate*10}} ALLN</div>
                    <div class="allnVal">( 1 ALLN = 0.1 USD )</div>
                </div>
                <div class="articleCnt">
                    <div class="titleS">{{trans('invest_create.titleS')}}</div>
                    <br>
                    <div class="activeTab" role="tabpanel">
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="type active" data-type="ETH">
                                <a href="#p1" id="p1-tab" role="tab" data-toggle="tab" aria-controls="p1" aria-expanded="true">{{trans('invest_create.p1_title')}}</a>
                            </li>
                            <li role="presentation" class="" data-type="USD">
                                <a href="#" role="tab" id="p2-tab" data-toggle="tab" aria-controls="p2" aria-expanded="false">{{trans('invest_create.p2_title')}}</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="p1" aria-labelledby="p1-tab">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control inputETH" id="inputETH" placeholder="" onkeyup="this.value=this.value.replace(/\D/g,'')"
                                               onafterpaste="this.value=this.value.replace(/\D/g,'')" value="0"> ETH =
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control ETH2ALLN" id="inputALLN" placeholder="0"> ALLN
                                    </div>
                                </form>
                                <div class="titleS">{{trans('invest_create.payment')}}：</div>
                                <div class="cubeBtn c1"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p2" aria-labelledby="p2-tab">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <input type="text" class="form-control inputUSD" id="inputUSD" placeholder="" onkeyup="this.value=this.value.replace(/\D/g,'')"
                                               onafterpaste="this.value=this.value.replace(/\D/g,'')" value="0"> USD =
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control USD2ALLN" id="inputALLN2 disabled" placeholder="0"> ALLN
                                    </div>
                                    <div class="form-group">
                                        {{trans('invest_create.inputFEE')}}：<input type="number" class="form-control" id="inputFEE" placeholder="" value="0" disabled>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        {{trans('invest_create.inputTotal')}}：<input type="number" class="form-control" id="inputTotal" placeholder="" value="0" disabled>
                                    </div>
                                </form>
                                <div class="titleS">{{trans('invest_create.payment')}}：</div>
                                <div class="cubeBtn c2"></div>
                            </div>
                        </div>
                    </div>
                    <form class="form-inline">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cb2" class="i-agree-riskDesc">{!! trans('invest_create.riskDesc') !!}
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cb2" class="i-agree-privacyDesc">{!! trans('invest_create.privacyDesc') !!}
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cb2" class="i-agree-downloadModal">{!! trans('invest_create.downloadModal') !!}
                            </label>
                        </div>
                    </form>
                    <div class="note">
                        <div class="text">{{trans('invest_create.note')}}：<br><br>
                            <ol class="text-left">
                                <li>{{trans('invest_create.note_1')}}</li>
                                <li>{{trans('invest_create.note_2')}}</li>
                                <li>{{trans('invest_create.note_3')}}</li>
                                <li>{{trans('invest_create.note_4')}}</li>
                            </ol>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="btn form-control btn-send">{{trans('invest_create.pay')}}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        var type = "ETH";
        var ETH_Rate = parseInt("{{$ETH_Rate}}");
        $(document).ready(function () {
            //
            $("#p2-tab").click(function () {
                modal_show({title: 'Notice', content: 'Coming Soon'});
            });
            //
            $(".type").click(function () {
                type = $(this).data('type');
            })
            //
            $('.inputETH').on('change', function () {
                var count = parseInt($(this).val());
                $('.ETH2ALLN').val(count * ETH_Rate * 10);
            });
            //
            $('.inputUSD').on('change', function () {
                var count = parseInt($(this).val());
                $('.USD2ALLN').val(count * 10)
            });
            //
            $(".btn-send").click(function () {
                if ($('.i-agree-riskDesc').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('invest_create.riskDesc_title')}}', content: '{{trans('invest_create.riskDesc_content')}}'});
                    return false;
                }
                if ($('.i-agree-privacyDesc').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('invest_create.privacyDesc_title')}}', content: '{{trans('invest_create.privacyDesc_content')}}'});
                    return false;
                }
                if ($('.i-agree-downloadModal').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('invest_create.downloadModal_title')}}', content: '{{trans('invest_create.downloadModal_content')}}'});
                    return false;
                }

                var invest_value = 0;
                switch (type) {
                    case 'ETH':
                        invest_value = $(".inputETH").val();
                        break;
                    case 'USD':
                        invest_value = $(".inputUSD").val();
                        break;
                    default:
                        type = "ETH";
                        invest_value = $(".inputETH").val();
                }
                var data = {"_token": "{{ csrf_token() }}"};
                data.type = type;
                data.invest_value = invest_value;
                if (invest_value == 0) {
                    modal_show({title: '{{trans('invest_create.zero_title')}}', content: '{{trans('invest_create.zero_content')}}'});
                    return false;
                }
                $.ajax({
                    url: "{{url('doInvest')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            setTimeout(function () {
                                location.href = "{{url('invest_pay')}}/" + rtndata.invest_num;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            })


        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->