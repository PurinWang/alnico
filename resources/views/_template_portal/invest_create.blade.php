@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/newInvestiment.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{{trans('invest_create.title')}}</div>
                    <div class="stepBtn"><a href="javascript:void(0);" data-toggle="modal" data-target="#stepDesc"> {{trans('invest_create.stepBtn')}} </a></div>
                    <div class="todayVal">1 ALLN = {{number_format(1/($ETH_Rate*10),8,'.',',')}} ETH = {{number_format(1/($BTC_Rate*10),8,'.',',')}} BTC = 0.1 USD</div>
                </div>
                <div class="articleCnt">
                    <div class="titleS">{{trans('invest_create.titleS')}}</div>
                    <br>
                    <div class="activeTab" role="tabpanel">
                        <ul class="nav nav-tabs nav-justified" role="tablist">
                            <li role="presentation" class="type active" data-type="ETH">
                                <a href="#p1" id="p1-tab" role="tab" data-toggle="tab" aria-controls="p1" aria-expanded="true"><i class="fab fa-ethereum"></i>&nbsp;&nbsp;ETH</a>
                            </li>
                            <li role="presentation" class="type" data-type="BTC">
                                <a href="#p2" role="tab" id="p2-tab" data-toggle="tab" aria-controls="p2" aria-expanded="false"><i class="fab fa-bitcoin"></i>&nbsp;&nbsp;BTC</a>
                            </li>
                            <li role="presentation" class="type" data-type="USD">
                                <a href="#p3" role="tab" id="p3-tab" data-toggle="tab" aria-controls="p3" aria-expanded="false"><i
                                            class="fas fa-dollar-sign"></i>&nbsp;&nbsp;USD</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active" id="p1" aria-labelledby="p1-tab">
                                <table class="table">
                                    <tr>
                                        <td width="50%">
                                            <input class="form-control inputETH" id="inputETH" placeholder="ETH"
                                                   onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"
                                                   onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}">
                                        </td>
                                        <td> =</td>
                                        <td class="ConvertNum alln">0</td>
                                        <td>ALLN</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">+Bonus ( {{$bonus or 0}}% ) =</td>
                                        <td class="ConvertNum bonus">0</td>
                                        <td>ALLN</td>
                                    </tr>
                                    <tr style="border-top:1px #ddd solid;">
                                        <td>Total</td>
                                        <td colspan="2" class="ConvertNum total">0</td>
                                        <td>ALLN</td>
                                    </tr>
                                </table>
                                <div class="titleS">{{trans('invest_create.payment')}}：</div>
                                <div class="cubeBtn c1"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p2" aria-labelledby="p2-tab">
                                <table class="table">
                                    <tr>
                                        <td width="50%">
                                            <input class="form-control inputBTC" id="inputBTC" placeholder="BTC" type="number"  step="0.1" min="0" onkeyup="clearNoNum(this)">
                                        </td>
                                        <td> =</td>
                                        <td class="ConvertNum alln">0</td>
                                        <td>ALLN</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">+Bonus ( {{$bonus or 0}}% ) =</td>
                                        <td class="ConvertNum bonus">0</td>
                                        <td>ALLN</td>
                                    </tr>
                                    <tr style="border-top:1px #ddd solid;">
                                        <td>Total</td>
                                        <td colspan="2" class="ConvertNum total">0</td>
                                        <td>ALLN</td>
                                    </tr>
                                </table>
                                <div class="titleS">{{trans('invest_create.payment')}}：</div>
                                <div class="cubeBtn c2"></div>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="p3" aria-labelledby="p3-tab">
                                <table class="table">
                                    {{--<tr>--}}
                                        {{--<td width="50%">--}}
                                            {{--<input class="form-control inputUSD" id="inputUSD" placeholder="USD"--}}
                                                   {{--onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"--}}
                                                   {{--onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}">--}}
                                        {{--</td>--}}
                                        {{--<td> =</td>--}}
                                        {{--<td class="ConvertNum alln">0</td>--}}
                                        {{--<td>ALLN</td>--}}
                                    {{--</tr>--}}
                                    {{--<tr>--}}
                                        {{--<td colspan="2">+Bonus ( {{$bonus or 0}}% ) =</td>--}}
                                        {{--<td class="ConvertNum bonus">0</td>--}}
                                        {{--<td>ALLN</td>--}}
                                    {{--</tr>--}}
                                    {{--<tr style="border-top:1px #ddd solid;">--}}
                                        {{--<td>Total</td>--}}
                                        {{--<td colspan="2" class="ConvertNum total">0</td>--}}
                                        {{--<td>ALLN</td>--}}
                                    {{--</tr>--}}
                                </table>
                                <div class="titleS">{{trans('invest_create.payment')}}：</div>
                                <div class="cubeBtn c3"></div>
                                <div class="usdText">
                                    <h3>{{trans('invest_create.usd_note_1')}}</h3>
                                    <ol class="text-left">
                                        <li>{{trans('invest_create.usd_note_2')}}</li>
                                        <li>{{trans('invest_create.usd_note_3')}}</li>
                                        <li>{{trans('invest_create.usd_note_4')}}</li>
                                        <li>{{trans('invest_create.usd_note_5')}}</li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form class="form-inline">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cb2" class="i-agree-riskDesc">{!! trans('invest_create.riskDesc') !!}
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cb2" class="i-agree-privacyDesc">{!! trans('invest_create.privacyDesc') !!}
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="cb2" class="i-agree-downloadModal">{!! trans('invest_create.downloadModal') !!}
                            </label>
                        </div>
                    </form>
                    <div class="note">
                        <div class="text">{{trans('invest_create.note')}}：<br><br>
                            <ol class="text-left">
                                <li>{{trans('invest_create.note_1')}}</li>
                                <li>{{trans('invest_create.note_2')}}</li>
                                <li>{{trans('invest_create.note_3')}}</li>
                                <li>{{trans('invest_create.note_4')}}</li>
                                <li>{{trans('invest_create.note_5')}}</li>
                            </ol>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="btn payBtn form-control btn-send">{{trans('invest_create.pay')}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        var type = "ETH";
        var ETH_Rate = parseInt("{{$ETH_Rate}}");
        var BTC_Rate = parseInt("{{$BTC_Rate}}");
        $(document).ready(function () {
            //
//            $("#p3-tab").click(function () {
//                modal_show({title: 'Notice', content: 'Coming Soon'});
//            });
            //
            $(".type").click(function () {
                type = $(this).data('type');
            });
            //
            $('.inputETH').on('change', function () {
                var count = parseInt($(this).val());
                var alln = count * ETH_Rate * 10;
                var bonus = (alln * parseInt({{$bonus or 0}})) /100;
                var total = alln+bonus;
                $("#p1").find(".alln").html(alln);
                $("#p1").find(".bonus").html(bonus);
                $("#p1").find(".total").html(total);
            });
            //
            $('.inputBTC').on('change', function () {
                var count = parseFloat($(this).val());
                var alln = count * BTC_Rate * 10;
                var bonus = (alln * parseFloat({{$bonus or 0}})) /100;
                var total = alln+bonus;
                $("#p2").find(".alln").html(alln);
                $("#p2").find(".bonus").html(bonus);
                $("#p2").find(".total").html(total);
            });
            //
            $('.inputUSD').on('change', function () {
                var count = parseInt($(this).val());
                var alln = count * 10;
                var bonus = (alln * parseInt({{$bonus or 0}})) /100;
                var total = alln+bonus;
                $("#p3").find(".alln").html(alln);
                $("#p3").find(".bonus").html(bonus);
                $("#p3").find(".total").html(total);
            });
            //
            $(".btn-send").click(function () {
                if ($('.i-agree-riskDesc').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('invest_create.riskDesc_title')}}', content: '{{trans('invest_create.riskDesc_content')}}'});
                    return false;
                }
                if ($('.i-agree-privacyDesc').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('invest_create.privacyDesc_title')}}', content: '{{trans('invest_create.privacyDesc_content')}}'});
                    return false;
                }
                if ($('.i-agree-downloadModal').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('invest_create.downloadModal_title')}}', content: '{{trans('invest_create.downloadModal_content')}}'});
                    return false;
                }

                var invest_value = 0;
                switch (type) {
                    case 'ETH':
                        invest_value = $(".inputETH").val();
                        break;
                    case 'BTC':
                        invest_value = $(".inputBTC").val();
                        break;
                    case 'USD':
                        //invest_value = $(".inputUSD").val();
                        invest_value = 0;
                        break;
                    default:
                        type = "ETH";
                        invest_value = $(".inputETH").val();
                }
                var data = {"_token": "{{ csrf_token() }}"};
                data.type = type;
                data.invest_value = invest_value;
                if (type != 'USD' && invest_value == 0) {
                    modal_show({title: '{{trans('invest_create.zero_title')}}', content: '{{trans('invest_create.zero_content')}}'});
                    return false;
                }
                $.ajax({
                    url: "{{url('doInvest')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            setTimeout(function () {
                                location.href = "{{url('invest_pay')}}/" + rtndata.invest_num;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            })
        });
        function clearNoNum(obj){
            obj.value = obj.value.replace(/[^\d.]/g,"");  //清除“数字”和“.”以外的字符
            obj.value = obj.value.replace(/\.{2,}/g,"."); //只保留第一个. 清除多余的
            obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
            obj.value = obj.value.replace(/^(\-)*(\d+)\.(\d).*$/,'$1$2.$3');//只能输入两个小数
            if(obj.value.indexOf(".")< 0 && obj.value !=""){//以上已经过滤，此处控制的是如果没有小数点，首位不能为类似于 01、02的金额
                obj.value= parseFloat(obj.value);
            }
        }
    </script>
@endsection
<!-- ================== /inline-js ================== -->