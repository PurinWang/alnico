@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/airDrop.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!-- 內容區塊 -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="coin text-center"><img src="portal_assets/htm/img/airdrop.png" alt=""></div>
                    <div class="title"><span>ALLN AirDrop</span></div>
                    <div class="desc">Limited 2,000,000 tokens for you since 4/15 22:00 (GMT+8)!</div>
                    <div class="row cubeCnt">
                        <div class="col-sm-4">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="title">Task 1</div>
                                <div class="desc">Get <span>{{$coin_airdrops[1]->iBonus or 0}}</span> tokens after Sign-up AND Log-in.</div>
                                @if($coin_airdrops[1])
                                    <div class="goBtn target btn-register">Get ready now!</div>
                                @else
                                    <div class="goBtn">Finished</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="title">Task 2</div>
                                <div class="desc">Get <span>{{$coin_airdrops[2]->iBonus or 0}}</span> tokens after joining and logging in ALLN Telegram.</div>
                                @if($coin_airdrops[2])
                                    <div class="goBtn target btn-member">Get ready now!</div>
                                @else
                                    <div class="goBtn">Finished</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="title">Task 3</div>
                                <div class="desc">Invite your friends to sign up and then get <span>{{$coin_airdrops[3]->iBonus or 0}}</span> tokens.</div>
                                @if($coin_airdrops[3])
                                    <div class="goBtn target btn-member">Get ready now!</div>
                                @else
                                    <div class="goBtn">Finished</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="text">Finish KYC first then get the rewards.</div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            $(".btn-register").click(function () {
                location.href = "{{url('register')}}"
            })
            //
            $(".btn-member").click(function () {
                location.href = "{{url('member')}}#p3"
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->