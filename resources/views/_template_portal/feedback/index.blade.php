@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/feedback.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title"><i class="fas fa-pencil-alt"></i><br>向我们提供意见反馈</div>
                    <div class="aRegion">
                        <div class="aDesc">请利用此表单将您宝贵的意见、建议、批评与指教告诉ALLN团队，感谢您协助我们不断前进！</div>
                        <input class="form-control input-lg vEmail" type="email" placeholder="Email address">
                        <textarea class="form-control input-lg vSummary" placeholder="Your feedback"></textarea>
                        <div class="btn sendBtn form-control btn-send">送出</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script src="/portal_assets/dist/js/opinion.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        var url_doadd = "{{url('doSendFeedback')}}";
        $(document).ready(function () {
            $(".btn-send").click(function () {
                if ($('.vSummary').val() == '') {
                    toastr.info("請填寫內容!!", "{{trans('_web_alert.notice')}}")
                    return false;
                }
                //
                if ($('.vEmail').val() == '') {
                    toastr.info("請填寫Email!!", "{{trans('_web_alert.notice')}}")
                    return false;
                }
                //
                if ($('.vEmail').val() != '' && !reg_Email.test($(".vEmail").val())) {
                    toastr.info("Email格式錯誤!!", "{{trans('_web_alert.notice')}}")
                    return false;
                }
                //
                swal({
                    title: "{{trans('member.message_title')}}",
                    text: "{{trans('member.message_content')}}",
                    type: "warning",
                    showCancelButton: true,
                    cancelButtonText: "{{trans('_web_alert.cancel')}}",
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "{{trans('_web_alert.ok')}}",
                    closeOnConfirm: true
                }, function () {
                    var data = {"_token": "{{ csrf_token() }}"};
                    data.vEmail = $(".vEmail").val();
                    data.vSummary = $(".vSummary").val();
                    $.ajax({
                        url: "{{url('doSendFeedback')}}",
                        type: "POST",
                        data: data,
                        success: function (rtndata) {
                            switch (rtndata.status) {
                                case 1:
                                    toastr.success(rtndata.message, "{{trans('_web_alert.notice')}}");
                                    location.reload();
                                    break;
                                default:
                                    toastr.error(rtndata.message, "{{trans('_web_alert.notice')}}");
                                    break
                            }
                        }
                    });
                });
            });
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->