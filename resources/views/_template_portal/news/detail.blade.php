@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/information.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">News</div>
                    <div class="aRegion">
                        <div class="aDate">{{date('Y/m/d',$info->iStartTime)}}</div>
                        <div class="aTitle">{{$info->vTitle}}</div>
                        @if($info->vUrl)
                            <div class="aTitle"><a href="{{$info->vUrl}}" target="_blank">{{trans('aln.sec4.link')}}</a></div>
                        @endif
                        <div class="aDesc"> {!! $info->vDetail !!}</div>
                    </div>
                    @if($pre)
                        <div class="prevBtn">
                            <div class="iconCube pull-left"><i class="fas fa-chevron-left"></i></div>
                            <div class="text pull-left">{{$pre->vTitle}}</div>
                        </div>
                    @endif
                    @if($next)
                        <div class="nextBtn">
                            <div class="iconCube pull-right"><i class="fas fa-chevron-right"></i></div>
                            <div class="text pull-right">{{$next->vTitle}}</div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            @if($pre)
            $(".prevBtn").click(function () {
                location.href = "{{url('news/detail/'.$pre->iId)}}"
            });
            @endif
             @if($next)
            $(".nextBtn").click(function () {
                location.href = "{{url('news/detail/'.$next->iId)}}"
            });
            @endif
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->