@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/informational_list.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <section>
        <div class="container">
            <div class="row">
                @include('_template_portal._layouts.breadcrumb')
                <div class="col-sm-3 active-Sidebar">
                    <ul>
                        <li class="title"><a href="#">所有分類</a></li>
                        <li><a href="#" class="btn-cate" data-id="all">所有公告</a></li>
                        @foreach( $category as $item)
                            <li><a href="#" class="btn-cate" data-id="{{$item->iId}}">{{$item->vCategoryName}}</a></li>
                        @endforeach
                    </ul>
                </div>
                <div class="col-sm-9">
                    <div class="wrapBox">
                        <table>
                            <tbody class="item">
                            </tbody>
                        </table>
                        <div class="pager">
                            <ul class="pagination">
                                <li><a href="#"><i class="fa fa-angle-double-left" aria-hidden="true"></i></a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            getlist({});
            $(".btn-cate").click(function () {
                getlist({"cate": $(this).data("id")});
            })
        });

        function getlist(data) {
            $.ajax({
                url: "{{url('api/news/getlist')}}",
                data: data,
                type: "GET",
                success: function (rtndata) {
                    var html_str = "";
                    if (rtndata.status) {
                        $(".item").html('');
                        for (var key in rtndata.aaData) {
                            var item = rtndata.aaData[key];
                            html_str += '<tr>';
                            html_str += '<td class="product-name">';
                            html_str += '<div class="imgbox"><a href="' + item.url + '"><img src="' + item.vImages + '" alt=""></a></div>';
                            html_str += '</td>';
                            html_str += '<td class="width45 txt-region">';
                            html_str += '<div class="cate">' + item.vCategoryName + '</div>';
                            html_str += '<div class="date"><i class="fa fa-bookmark" aria-hidden="true"></i>' + item.iStartTime + '</div>';
                            html_str += '<div class="name">' + item.vTitle + '</div>';
                            html_str += '</td>';
                            html_str += '</tr>';
                        }
                        $(".item").html(html_str);
                    }
                }
            });
        }
    </script>
@endsection
<!-- ================== /inline-js ================== -->