@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/index.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <div class="canvasArea">
        <div id="particle-canvas"></div>
    </div>
    <!-- 內容區塊 -->
    <div class="content">
        <!-- 區塊／輪播／01-大圖輪播 -->
        @if(session('locale') == 'zh-cn')
            <div class="content">
                <!-- 區塊／輪播／01-大圖輪播 -->
                <section class="section" id="sec1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7">
                                <div class="leftText">
                                    <div class="title">{{trans('aln.sec1.title')}}</div>
                                </div>
                                <div class="leftText">
                                    <div class="desc">{{trans('aln.sec1.desc_1')}}<br>{{trans('aln.sec1.desc_2')}}</div>
                                    <div class="btnGroup">
                                        <div class="btn downloadBtn" data-toggle="modal" data-target="#downloadModal"><i class="fas fa-download"></i> {{trans('aln.sec1.white_paper')}}</div>
                                        <div class="btn profileBtn"><i class="fas fa-arrow-right"></i> {{trans('aln.sec1.person_info')}}</div>
                                    </div>
                                    <div class="btnGroup center share">
                                        <div class="title">Follow us：</div>
                                        <div class="btn weixinBtn"><a href="javascript:void(0);" data-toggle="modal" data-target="#weChat"><i class="fab fa-weixin"></i></a></div>
                                        <div class="btn twitterBtn"><a href="https://twitter.com/ALLNTOKEN" target="_blank"><i class="fab fa-twitter"></i></a></div>
                                        <div class="btn telegramBtn"><a href="https://t.me/allntoken" target="_blank"><i class="fab fa-telegram-plane"></i></a></div>
                                        <div class="btn facebookBtn"><a href="https://www.facebook.com/ALLNTOKEN" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="rightText">
                                    <div class="backCube">
                                        <div class="insCube">{{trans('aln.sec1.insCube')}}</div>
                                        <div class="insCube time">
                                            <div class="cItem"><span class="sDay">00</span>{{trans('aln.day')}}</div>
                                            <div class="cItem"><span class="sHour">00</span>{{trans('aln.hour')}}</div>
                                            <div class="cItem"><span class="sMin">00</span>{{trans('aln.minute')}}</div>
                                        </div>
                                        <div class="insCube">
                                            <div class="iframeCnt">
                                                <iframe src="https://player.vimeo.com/video/261946237" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @else
                    <section class="section" id="sec1">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="leftText">
                                        <div class="title">Overturn the impracticability of virtual currency</div>
                                    </div>
                                    <div class="leftText">
                                        <div class="desc">Consumption constructs value, Value is the future.<br>
                                            Airline and Life Networking Token, Your key to Real-life Cryptocurrency Consumption.
                                        </div>
                                        <div class="btnGroup">
                                            <div class="btn downloadBtn" data-toggle="modal" data-target="#downloadModal"><i class="fas fa-download"></i> Download white paper</div>
                                            <div class="btn profileBtn"><i class="fas fa-arrow-right"></i> Buy tokens</div>
                                        </div>
                                        <div class="btnGroup center share">
                                            <div class="title">Follow us：</div>
                                            <div class="btn weixinBtn"><a href="javascript:void(0);" data-toggle="modal" data-target="#weChat"><i class="fab fa-weixin"></i></a></div>
                                            <div class="btn twitterBtn"><a href="https://twitter.com/ALLNTOKEN" target="_blank"><i class="fab fa-twitter"></i></a></div>
                                            <div class="btn telegramBtn"><a href="https://t.me/allntoken" target="_blank"><i class="fab fa-telegram-plane"></i></a></div>
                                            <div class="btn facebookBtn"><a href="https://www.facebook.com/ALLNTOKEN" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="rightText">
                                        <div class="backCube">
                                            <div class="insCube">End in</div>
                                            <div class="insCube time">
                                                <div class="cItem"><span class="sDay">00</span>Day</div>
                                                <div class="cItem"><span class="sHour">00</span>Hour</div>
                                                <div class="cItem"><span class="sMin">00</span>minute</div>
                                            </div>
                                            <div class="insCube">
                                                <div class="iframeCnt">
                                                    <iframe src="https://player.vimeo.com/video/261946237" frameborder="0" webkitallowfullscreen mozallowfullscreen
                                                            allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @endif
            <!-- 區塊／入口／02-類別入口 -->
                @if(session('locale') == 'zh-cn')
                    <section class="section" id="sec2">
                        <div class="container">
                            <div class="col-md-9">
                                <div class="title">{{trans('aln.sec2.title')}}：</div>
                                <div class="desc">{{trans('aln.sec2.desc')}}</div>
                                <div class="btn"><a href="#" data-toggle="modal" data-target="#riskDesc">{{trans('aln.sec2.btn_1')}}</a></div>
                                <div class="btn"><a href="#" data-toggle="modal" data-target="#issueProgram">{{trans('aln.sec2.btn_2')}}</a></div>
                                <div class="btn"><a href="#" data-toggle="modal" data-target="#serviceDesc">{{trans('aln.sec2.btn_3')}}</a></div>
                            </div>
                            <div class="col-md-3">
                                <div class="btn form-control goBtn btn-invest"><a href="#"><i class="fas fa-arrow-right"></i> {{trans('aln.sec2.invest')}}</a></div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="progressCnt">
                                <div class="date">{{trans('aln.sec2.date')}}<a href="{{url('register')}}">{{trans('aln.register')}}</a>{{trans('aln.sec2.fundraising')}}</div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width: {{$process}}%">{{$process}}%
                                    </div>
                                    <div class="airplane" style="left: {{$process}}%;"><img src="/portal_assets/htm/img/aircraft-clipart-16.png" alt=""></div>
                                    <div class="softCap">
                                        <div class="lighthouse"><img src="/portal_assets/htm/img/control tower.png" alt=""></div>
                                        <div class="text">Soft Cap</div>
                                    </div>
                                </div>
                                <div class="startNum">Departure</div>
                                <div class="endNum">Arrival</div>
                            </div>
                        </div>
                    </section>
                @else
                    <section class="section" id="sec2">
                        <div class="container">
                            <div class="col-md-9">
                                <div class="title">Total token number：</div>
                                <div class="desc">complete 2,275,000,000 tokens</div>
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#riskDesc">Risk Disclosure Statement</a>&nbsp;
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#issueProgram">Issuing and Offering Plan</a>&nbsp;
                                <a href="javascript:void(0);" data-toggle="modal" data-target="#serviceDesc">Service Terms</a>
                            </div>
                            <div class="col-md-3">
                                <div class="btn form-control goBtn btn-invest"><a href="#"><i class="fas fa-arrow-right"></i> Buy tokens</a></div>
                            </div>
                        </div>
                        <div class="container-fluid">
                            <div class="progressCnt">
                                <div class="date"><strong>Fundraising date：</strong><br>2018/3/28 00:00 ~ 2018/4/27 23:59&nbsp;<br>Welcome to <a href="{{url('register')}}">join
                                        us</a>.
                                </div>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-danger progress-bar-striped active" role="progressbar" aria-valuenow="2" aria-valuemin="0"
                                         aria-valuemax="100"
                                         style="width: {{$process}}%;">{{$process}}%
                                    </div>
                                    <div class="airplane" style="left: {{$process}}%;"><img src="/portal_assets/htm/img/aircraft-clipart-16.png" alt=""></div>
                                    <div class="softCap">
                                        <div class="lighthouse"><img src="/portal_assets/htm/img/control tower.png" alt=""></div>
                                        <div class="text">Soft Cap</div>
                                    </div>
                                </div>
                                <div class="startNum">Departure</div>
                                <div class="endNum">Arrival</div>
                            </div>
                        </div>
                    </section>
                @endif
            <!-- 區塊／入口／03-類別入口 -->
                <section class="section" id="sec3">
                    <div class="container">
                        <div class="articleCnt">
                            <div class="coin"><img src="/portal_assets/htm/img/coin.png" alt=""></div>
                            <div class="title">{!! trans('aln.sec3.title') !!}</div>
                            <div class="desc">{{trans('aln.sec3.desc')}}</div>
                            <div class="iframeCnt">
                                <iframe src="https://player.vimeo.com/video/261946237" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="cube">
                                        <div class="icon"><i class="fas fa-lightbulb"></i></div>
                                        <div class="titleS">{{trans('aln.sec3.cube_1_title')}}</div>
                                        <div class="desc">
                                            {{trans('aln.sec3.cube_1_desc')}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="cube">
                                        <div class="icon"><i class="fas fa-handshake"></i></div>
                                        <div class="titleS">{{trans('aln.sec3.cube_2_title')}}</div>
                                        <div class="desc">
                                            {{trans('aln.sec3.cube_2_desc')}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- 區塊／入口／03-類別入口 -->
                <section class="section" id="secA">
                    <div class="container">
                        <div class="articleCnt">
                            <div class="title"><span>ALLN</span>{{trans('aln.secA.title')}}</div>
                            <div class="desc">{{trans('aln.secA.desc')}}</div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="/portal_assets/htm/img/a1.png" alt="">
                                    <div class="desc">{{trans('aln.secA.desc_1')}}</div>
                                </div>
                                <div class="col-sm-4">
                                    <img src="/portal_assets/htm/img/a2.png" alt="">
                                    <div class="desc">{{trans('aln.secA.desc_2')}}</div>
                                </div>
                                <div class="col-sm-4">
                                    <img src="/portal_assets/htm/img/a3.png" alt="">
                                    <div class="desc">{{trans('aln.secA.desc_3')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- 區塊／入口／03-類別入口 -->
                <section class="section" id="secB">
                    <div class="container">
                        <div class="articleCnt">
                            <div class="title">{{trans('aln.secB.title_1')}}<span>ALLN</span>{{trans('aln.secB.title_2')}}</div>
                            <div class="desc">{{trans('aln.secB.desc')}}</div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <img src="/portal_assets/htm/img/advantage1.jpg" alt="">
                                    <div class="desc">{{trans('aln.secB.desc_1')}}</div>
                                </div>
                                <div class="col-sm-4">
                                    <img src="/portal_assets/htm/img/advantage2.jpg" alt="">
                                    <div class="desc">{{trans('aln.secB.desc_2')}}</div>
                                </div>
                                <div class="col-sm-4">
                                    <img src="/portal_assets/htm/img/advantage3.jpg" alt="">
                                    <div class="desc">{{trans('aln.secB.desc_3')}}</div>
                                </div>
                                <div class="col-sm-6">
                                    <img src="/portal_assets/htm/img/advantage4.jpg" alt="">
                                    <div class="desc">{{trans('aln.secB.desc_4')}}</div>
                                </div>
                                <div class="col-sm-6">
                                    <img src="/portal_assets/htm/img/advantage5.jpg" alt="">
                                    <div class="desc">{{trans('aln.secB.desc_5')}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- 區塊／入口／03-類別入口 -->
                <section class="section" id="secC">
                    <div class="container">
                        <div class="articleCnt">
                            <div class="title">{!!trans('aln.secC.title')!!}</div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="row item i1">
                                        <div class="col-sm-3 titleS">2017 Q4</div>
                                        <div class="col-sm-9 desc">{{trans('aln.secC.desc_1')}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row item i2">
                                        <div class="col-sm-3 titleS">2018 Q1</div>
                                        <div class="col-sm-9 desc">{{trans('aln.secC.desc_2')}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row item i3">
                                        <div class="col-sm-3 titleS">2018 Q2</div>
                                        <div class="col-sm-9 desc">{{trans('aln.secC.desc_3')}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row item i4">
                                        <div class="col-sm-3 titleS">2018 Q3</div>
                                        <div class="col-sm-9 desc">{{trans('aln.secC.desc_4')}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row item i5">
                                        <div class="col-sm-3 titleS">2019</div>
                                        <div class="col-sm-9 desc">{{trans('aln.secC.desc_5')}}</div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="row item i6">
                                        <div class="col-sm-3 titleS">2020</div>
                                        <div class="col-sm-9 desc">{{trans('aln.secC.desc_6')}}</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                @if(session('locale') == 'zh-cn')
                <!-- 區塊／入口／03-類別入口 -->
                    <section class="section" id="secD">
                        <div class="container">
                            <div class="articleCnt">
                                <div class="title">{{trans('aln.secD.title')}}</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CEO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p1_name')}}
                                                    <small>{{trans('aln.secD.p1_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p1_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CFO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p2_name')}}
                                                    <small>{{trans('aln.secD.p2_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p2_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/GC.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p3_name')}}
                                                    <small>{{trans('aln.secD.p3_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p3_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CMO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p4_name')}}
                                                    <small>{{trans('aln.secD.p4_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p4_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CPO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p5_name')}}
                                                    <small>{{trans('aln.secD.p5_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p5_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CAO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p6_name')}}
                                                    <small>{{trans('aln.secD.p6_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p6_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CDO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p7_name')}}
                                                    <small>{{trans('aln.secD.p7_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p7_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-3">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CTO.jpg">
                                            <div class="caption text-center">
                                                <h3>{{trans('aln.secD.p8_name')}}
                                                    <small>{{trans('aln.secD.p8_title')}}</small>
                                                </h3>
                                                <p>{{trans('aln.secD.p8_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="articleCnt a2">
                                <div class="row">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <div class="title">顾问</div>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6">
                                                <div class="thumbnail">
                                                    <img src="/portal_assets/htm/img/professor.jpg" class="img-circle">
                                                    <div class="caption text-center">
                                                        <h3>{{trans('aln.secD.p21_name')}} {{trans('aln.secD.p21_title')}}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6">
                                                <div class="thumbnail">
                                                    <img src="/portal_assets/htm/img/lawyer.jpg" class="img-circle">
                                                    <div class="caption text-center">
                                                        <h3>{{trans('aln.secD.p22_name')}} {{trans('aln.secD.p22_title')}}</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="title">{{trans('aln.secD.p31_title')}}</div>
                                        <div class="thumbnail">
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners1.png"></div>
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners2.png"></div>
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners3.png"></div>
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners4.png"></div>
                                            <div class="caption text-center">
                                                <p>{{trans('aln.secD.p31_desc')}}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                @else
                    <section class="section" id="secD">
                        <div class="container">
                            <div class="articleCnt a1">
                                <div class="title">Introduction to ALLN Team</div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail boss">
                                            <img src="/portal_assets/htm/img/CEO.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">TSENG,CHIN-CHIH
                                                    <small>CEO</small>
                                                </h3>
                                                <div class="item">
                                                    Founder of the Issuer<br>
                                                    <strong>Education:</strong>
                                                    <br>Master of Applied Chemistry, National Chiao Tung University<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>COO, Far Eastern Air Transport Corporation<br>Managing the group's aviation, finance, hotel, and tourism business, assessing
                                                    real estate development investment, and planning operations
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CFO.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">JOSHUA CHEN
                                                    <small>CFO</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Master of Business Administration, Northumbria University, UK<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>Chief of Financial Section, EVA AIRWAYS CORPORATION<br>Assistant Vice President of Financial Section, Far Eastern Air
                                                    Transport Corporation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/GC.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">MARLENE DU
                                                    <small>GC</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Bachelor of Law, National Taiwan University<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>Managing Partner, Mingzhan Commercial Law Firm,<br>Deputy Minister, Financial Law Commission, Taiwan Bar Association<br>Junior
                                                    Partner, PCL TransAsia Law Offices
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CMO.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">JAMES YANG
                                                    <small>CMO</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Master of Design, National Taiwan Normal University<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>CSO(Chief Strategy Officer), Far Eastern Air Transport Corporation <br>General Manager, Shopping Mall Business Group,<br>Specializing
                                                    in developing, operating and brand marketing for department stores and shopping malls
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CPO.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">PATTY LU
                                                    <small>CPO</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Bachelor of Industrial Engineering and Management, National Taipei University of Technology<br>Maintaining relationships
                                                    with the media, holding press conferences, releasing news, conducting dynamic analysis on the media, interviewing with
                                                    reporters, arranging special reports on enterprises, engaging in other communication with the media, and monitoring the effects
                                                    of advertisements<br><br>
                                                    <strong>Current Position:</strong>
                                                    <br>Vice President, Far Eastern Air Transport Corporation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CAO.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">JOSEPH LEE
                                                    <small>CAO</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Bachelor of German Language and Culture, Fu Jen Catholic University<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>Lecturer of English and German, YMCA,<br>Representative in Europe, Japan, and Mainland China, <br>President, Cambodia
                                                    Airlines,<br>President, Great Star Travel,<br>Representative, YKT CORPORATION, Japan, Chairman, Shuo Li Interior Design and
                                                    Engineering Co., Ltd.<br><br>
                                                    <strong>Current Position:</strong>
                                                    <br>President, Far Eastern Air Transport Corporation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CDO.jpg">
                                            <div class="caption text-center">
                                                <h3 class="text-center">LORITA WANG
                                                    <small>CDO</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Bachelor of Computer Science and Information Engineering, Ming Chuan University<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>Vice President of Services, TransAsia Airways,<br>Specializing in air and ground crews’ operations and the according
                                                    standard operation procedure, ground handling agent business, and private operation of business aircraft<br><br>
                                                    <strong>Current Position:</strong>
                                                    <br>Assistant Vice President of Information Department, Far Eastern Air Transport Corporation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <img src="/portal_assets/htm/img/CTO.jpg">
                                            <div class="caption">
                                                <h3 class="text-center">EDDY CHEN
                                                    <small>CTO</small>
                                                </h3>
                                                <div class="item">
                                                    <strong>Education:</strong>
                                                    <br>Master of Computer Science, Institute of Data Science and Engineering, National Chiao Tung University<br><br>
                                                    <strong>Experience:</strong>
                                                    <br>Manager, ORACLE CORPORATION,<br>Manager, International Integrated Systems, Inc.<br>Manager, Sun Microsystems, Inc.<br>Manager,
                                                    Deloitte Touche Tohmatsu Limited<br>Specializing in integration and development of information system, <br>information security,
                                                    and aviation e-commerce<br><br>
                                                    <strong>Current Position:</strong>
                                                    <br>Assistant Vice President of Information Department, Far Eastern Air Transport Corporation
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="articleCnt a2">
                                <div class="row">
                                    <div class="col-sm-offset-3 col-sm-6">
                                        <div class="title">Adviser</div>
                                        <div class="row">
                                            <div class="col-xs-6 col-sm-6">
                                                <div class="thumbnail">
                                                    <img src="/portal_assets/htm/img/professor.jpg" class="img-circle">
                                                    <div class="caption text-center">
                                                        <h3>Ruei-Shan Lu Blockchain Professor</h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-6">
                                                <div class="thumbnail">
                                                    <img src="/portal_assets/htm/img/lawyer.jpg" class="img-circle">
                                                    <div class="caption text-center">
                                                        <h3>Warren Pan Lawer</h3>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="title">Strategic Partners</div>
                                        <div class="thumbnail">
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners1.png"></div>
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners2.png"></div>
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners3.png"></div>
                                            <div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners4.png"></div>
                                            <div class="caption text-center">
                                                <p>Based on the development plan, ALLN will select and invite the leading enterprises in global emerging industries to build
                                                    strategic cooperation relationships and participate in the ALLN application system. It is the strategic cooperative partners
                                                    that develop and execute the projects for practical operations of the ALLN.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

            @endif

            <!-- 區塊／資訊／01-固定六個資訊 -->
                <section class="section information num-1" id="sec4">
                    <div class="container">
                        <div class="title-region">{{trans('aln.sec4.title')}}</div>
                        <div class="row">
                            @foreach($news as $key =>$item)
                                <div class="col-sm-4">
                                    <div class="itemBox thumbnail">
                                        <a href="{{$item->url}}" target="_blank">
                                            <div class="imgBox rImg"><img src="{{$item->vImages}}" alt=""></div>
                                            <div class="text-box">
                                                <div class="descBox ellipsis">{{$item->vTitle}}</div>
                                                <div class="more-box"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>{{trans('aln.sec4.link')}}</div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </section>
                <!-- 區塊／資訊／02-輪播資訊 -->
                <!-- <section class="section information num-3" id="sec5">
                    <div class="container">
                        <div class="title-region">活動訊息</div>
                        <div class="informationNum-3">
                            <ul>
                                <li class="ellipsis"><a href="#">這邊十五字級十五字級一行一行最多二十幾字二十<span class="date">2018/3/14</span></a></li>
                                <li class="ellipsis"><a href="#">這邊十五字級十五字級一行一行最多二十幾字二十<span class="date">2018/3/14</span></a></li>
                                <li class="ellipsis"><a href="#">這邊十五字級十五字級一行一行最多二十幾字二十<span class="date">2018/3/14</span></a></li>
                            </ul>
                        </div>
                    </div>
                </section>    -->
                <!-- 區塊／入口／07-類別入口 -->
                <section class="section" id="sec7">
                    <div class="container">
                        <div class="articleCnt">
                            <div class="title">{{trans('aln.sec7.title')}}</div>
                            <div class="desc">{{trans('aln.sec7.desc')}} <span class="sDay"></span>{{trans('aln.day')}} <span class="sHour"></span>{{trans('aln.hour')}}
                                <span class="sMin"></span>{{trans('aln.minute')}}</div>
                            <div class="btnGroup">
                                <div class="btn facebookBtn share-facebook"><a href="javascript:void(0)"><i class="fab fa-facebook-f"></i><br>share Facebook</a></div>
                                <div class="btn twitterBtn share-twitter"><a href="javascript:void(0)"><i class="fab fa-twitter"></i><br>share Twitter</a></div>
                                <div class="btn googleBtn share-googleplus"><a href="javascript:void(0)"><i class="fab fa-google-plus-g"></i><br>share Google+</a></div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- 區塊／入口／08-類別入口 -->
                <section class="section" id="sec8">
                    <div class="container">
                        <div class="articleCnt">
                            <div class="title">{{trans('aln.sec8.title')}}</div>
                            <div class="desc">{{trans('aln.sec8.desc')}}</div>
                            <div class="btnGroup">
                                <div class="btn downloadBtn" data-toggle="modal" data-target="#downloadModal"><i class="fas fa-download"></i> {{trans('aln.sec8.white_paper')}}
                                </div>
                                <div class="btn profileBtn"><i class="fas fa-arrow-right"></i> {{trans('aln.sec8.register')}}</div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        @endsection
        <!-- ================== page-js ================== -->
    @section('page-js')
        <!--  -->
            <script type="text/javascript" src="/portal_assets/dist/timers/jquery.timers.min.js"></script>
            <script type="text/javascript" src="/portal_assets/htm/js/index.js"></script>
    @endsection
    <!-- ================== /page-js ================== -->
        <!-- ================== inline-js ================== -->
    @section('inline-js')
        <!--  -->
            <script>
                $(document).ready(function () {
                    //
                    $(".btn-invest").click(function () {
                        location.href = "{{url('invest')}}";
                    });
                    //
                    $(".profileBtn").click(function () {
                        location.href = "{{url('invest')}}";
                    })
                    //
                    $(".share-facebook").click(function () {
                        website_share('fb');
                    });
                    //
                    $(".share-twitter").click(function () {
                        website_share('twitter');
                    })
                    //
                    $(".share-googleplus").click(function () {
                        website_share('gplus');
                    })
                });
            </script>
    @endsection
    <!-- ================== /inline-js ================== -->