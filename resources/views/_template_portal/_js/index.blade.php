<script>
    $(document).ready(function () {
        //
        //$('#warningModel').modal();
        //
        $('.joinBtn').click(function () {
            if ($(".vEmail").val() == "") {
                modal_show({title: 'Email', content: $(".vEmail").attr('placeholder')});
                $(".vEmail").focus();
                return false;
            } else {
                if (!reg_Email.test($(".vEmail").val())) {
                    modal_show({title: 'Email', content: '{{trans('register.email_content')}}'});
                    $(".vEmail").val().focus();
                    return false;
                }
            }

            var data = {"_token": "{{ csrf_token() }}"};
            data.vEmail = $(".vEmail").val();

            $.ajax({
                url: "{{url('doAddEmail')}}",
                data: data,
                type: "POST",
                resetForm: true,
                success: function (rtndata) {
                    if (rtndata.status) {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        setTimeout(function () {
                            location.reload();
                        }, 1000)
                    } else {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                    }
                }
            });
        });
        //
        $(".btn-invest").click(function () {
{{--            location.href = "{{url('invest')}}";--}}
            location.href = "{{url('register')}}";
        });
        //
        $(".seeBtn").click(function () {
            location.href = "{{url('')}}#secB";
        });
        //
        $(".profileBtn").click(function () {
            {{--location.href = "{{url('invest')}}";--}}
            location.href = "{{url('register')}}";
        })
        //
        $(".share-facebook").click(function () {
            website_share('fb');
        });
        //
        $(".share-twitter").click(function () {
            website_share('twitter');
        })
        //
        $(".share-googleplus").click(function () {
            website_share('gplus');
        })

        //
        var NowDate = new Date();
        var aStartAry = {!! $start_time !!};
        var aEndAry = {!! $end_time !!};
        var currentTarget = 0;
        var focusDateNum = 0;
        var lockBoo = true;

        function init() {
            var endDate = new Date('{{date('Y/m/d H:i:s',$current_activity['end_time'])}}');
            var spantime = (endDate - NowDate) / 1000;

            $(this).everyTime('1s', function (i) {
                spantime--;

                var d = Math.floor(spantime / (24 * 3600));
                var h = Math.floor((spantime % (24 * 3600)) / 3600);
                var m = Math.floor((spantime % 3600) / (60));
                var s = Math.floor(spantime % 60);

                if (spantime > 0) {
                    $('.sDay').text(d);
                    $('.sHour').text(h);
                    $('.sMin').text(m);
                    $('.sSec').text(s);
                } else { // 避免倒數變成負的
                    $('.sDay').text(0);
                    $('.sHour').text(0);
                    $('.sMin').text(0);
                    $('.sSec').text(0);
                }
            });

            getActiveTarget();
        }

        init();

        function getActiveTarget() {
            for (var i = 0; i < aStartAry.length; i++) {
                var startDate = new Date('2018/' + aStartAry[i] + ' 00:00:00');
                var endDate = new Date('2018/' + aEndAry[i] + ' 23:59:59');

                if (startDate < NowDate && endDate > NowDate) {
                    currentTarget = i + 1;
                    $('#secF .items:nth-child(' + (i + 1) + ')').addClass('focus');
                    focusDateNum = (endDate - NowDate) / 1000;
                    break;
                }
                if (startDate < NowDate && endDate < NowDate) {
                    $('#secF .items:nth-child(' + (i + 1) + ')').css("opacity", "0.5");
                    $('#secF .items:nth-child(' + (i + 1) + ') .time').html('sold out');
                }
            }

            $(this).everyTime('1s', function (i) {
                focusDateNum--

                var d = Math.floor(focusDateNum / (24 * 3600));
                var h = Math.floor((focusDateNum % (24 * 3600)) / 3600);
                var m = Math.floor((focusDateNum % 3600) / (60));
                var s = Math.floor(focusDateNum % 60);

                $('#secF .items:nth-child(' + (currentTarget) + ') .time').html(d + "：" + h + "：" + m + "：" + s);
            });
        }
    });
</script>