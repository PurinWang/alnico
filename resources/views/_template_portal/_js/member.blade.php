<script>
    var vPassportUrl;
    $(document).ready(function () {
        //
        populateCountries("country");
        //
        $(".vCountry").val("{{session('shop_member.info.vCountry')}}");
        //
        $(".btn-save").click(function () {
            if ($(".vCountry").val() == -1 || !$(".vCountry").val()) {
                modal_show({title: 'Country', content: '{{trans('register.country_empty')}}'});
                return false;
            }
            var data = {"_token": "{{ csrf_token() }}"};
            data.vUserName = $(".vUserName").val();
            data.vUserTitle = $(".vUserTitle").val();
            data.vUserID = $(".vUserID").val();
            data.vUserEmail = $(".vUserEmail").val();
            data.vCountryCode = $(".vCountryCode").val();
            data.vUserContact = $(".vUserContact").val();
            data.iUserBirthday = $(".iUserBirthday").val();
            data.vUserAddress = $(".vUserAddress").val();
            data.vCountry = $(".vCountry").val();
            data.vERC = $(".vERC").val();
            if (document.getElementById('vPassportF').files[0]) {
                upload_file(document.getElementById('vPassportF').files[0]);
                data.vPassportF = vPassportUrl;
            }
            if (document.getElementById('vPassportB').files[0]) {
                upload_file(document.getElementById('vPassportB').files[0]);
                data.vPassportB = vPassportUrl;
            }
            if (document.getElementById('vPassportT').files[0]) {
                upload_file(document.getElementById('vPassportT').files[0]);
                data.vPassportT = vPassportUrl;
            }
            if (!reg_Email.test(data.vUserEmail)) {
                modal_show({title: 'Email', content: 'Email Error'});
                return false;
            }
            $.ajax({
                url: "{{url('member/dosave')}}",
                data: data,
                type: "POST",
                success: function (rtndata) {
                    if (rtndata.status) {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        setTimeout(function () {
                            //location.href = rtndata.rtnurl;
                        }, 1000)
                    } else {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                    }
                }
            });
        });
        //
        $(".btn-dosave-pw").click(function () {
            var obj = $(this).closest('#p2');
            if (obj.find(".vPassword").val() == "") {
                modal_show({title: 'Password', content: 'Password empty'});
                obj.find(".vPassword").focus();
                return false;
            }
            if (obj.find(".vPassword_new").val() == "") {
                modal_show({title: 'Password', content: 'Password empty'});
                obj.find(".vPassword_new").focus();
                return false;
            }
            if (obj.find(".vPassword_new").val() != obj.find(".vPassword_confirm").val()) {
                modal_show({title: 'ConfirmPassword', content: 'Confirm Password Not Good'});
                return false;
            }
            var data = {"_token": "{{ csrf_token() }}"};
            data.vPassword = CryptoJS.MD5(obj.find(".vPassword").val()).toString(CryptoJS.enc.Base64);
            data.vPasswordNew = CryptoJS.MD5(obj.find(".vPassword_new").val()).toString(CryptoJS.enc.Base64);
            $.ajax({
                url: "{{url('member/dosavepassword')}}",
                data: data,
                type: "POST",
                resetForm: true,
                success: function (rtndata) {
                    if (rtndata.status) {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        setTimeout(function () {
                            location.href = rtndata.rtnurl;
                        }, 1000)
                    } else {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                    }
                }
            });
        });
        //
        $('.btn-invest-images').click(function () {
            var id = $(this).data('invest_num');
            modal = $("#uploadModal");
            modal.data('id', id);
            modal.modal();
        })

        //
        $(".btn-send-invest").click(function () {
            var data = {"_token": "{{ csrf_token() }}"};
            data.iId = modal.data('id');
            if (document.getElementById('inputUpload').files[0]) {
                upload_file(document.getElementById('inputUpload').files[0]);
                data.vInvestNum = modal.data('id');
                data.vImages = vPassportUrl;
                $.ajax({
                    url: "{{url('member/dosaveinvest')}}",
                    data: data,
                    type: "POST",
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                            setTimeout(function () {
                                location.reload();
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            }
        })
        //
        $(".btn-send-message").click(function () {
            if ($('.vSummary').val() == '') {
                toastr.info("{{trans('member.summary_empty')}}", "{{trans('_web_alert.notice')}}")
                return false;
            }
            //
            swal({
                title: "{{trans('member.message_title')}}",
                text: "{{trans('member.message_content')}}",
                type: "warning",
                showCancelButton: true,
                cancelButtonText: "{{trans('_web_alert.cancel')}}",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "{{trans('_web_alert.ok')}}",
                closeOnConfirm: true
            }, function () {
                var data = {"_token": "{{ csrf_token() }}"};
                data.vSummary = $(".vSummary").val();
                $.ajax({
                    url: "{{url('member/doaddmessage')}}",
                    type: "POST",
                    data: data,
                    success: function (rtndata) {
                        switch (rtndata.status) {
                            case 1:
                                toastr.success(rtndata.message, "{{trans('_web_alert.notice')}}");
                                location.href = "{{url('member')}}/#p3";
                                break;
                            default:
                                toastr.error(rtndata.message, "{{trans('_web_alert.notice')}}");
                                break
                        }
                    }
                });
            });
        });
        //
        $('.btn-send-lightenpaper').click(function () {
            var data = {"_token": "{{ csrf_token() }}"};
            data.iMissionId = $(this).data('id');
            data.vValue = $(".inputID").val();
            $.ajax({
                url: "{{url('coin_airdrops/dosend')}}",
                data: data,
                type: "POST",
                success: function (rtndata) {
                    if (rtndata.status) {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        setTimeout(function () {
                            //location.reload();
                        }, 1000)
                    } else {
                        modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                    }
                }
            });
        })
    });
    function upload_file(file) {
        var data = new FormData();
        data.append("_token", "{{ csrf_token() }}");
        data.append("files", file);
        $.ajax({
            data: data,
            type: "POST",
            url: "{{url('upload_image')}}",
            async: false,
            contentType: false,
            processData: false,
            success: function (rtndata) {
                if (rtndata.status) {
                    vPassportUrl = rtndata.files[0].url;
                } else {
                    vPassportUrl = "";
                }
            }
        });
    }
</script>