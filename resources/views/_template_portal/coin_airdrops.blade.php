@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/airDrop.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!-- 內容區塊 -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="coin text-center"><img src="portal_assets/htm/img/airdrop.png" alt=""></div>
                    <div class="title"><span>ALLN空投大撒币</span></div>
                    <div class="desc">限量2,000,000，4/15 22：00 (香港时区 GMT+8) 开始放送！</div>
                    <div class="row cubeCnt">
                        <div class="col-sm-4">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="title">任务一</div>
                                <div class="desc">完成注册并登入<br><span>即可获得 ALLN {{$coin_airdrops[1]->iBonus or 0}}枚</span></div>
                                @if($coin_airdrops[1])
                                    <div class="goBtn target btn-register">马上行动</div>
                                @else
                                    <div class="goBtn">活动已结束</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="title">任务二</div>
                                <div class="desc">订阅 ALLN 电报(Telegram)<br><span>即可获得 ALLN {{$coin_airdrops[2]->iBonus or 0}}枚</span></div>
                                @if($coin_airdrops[2])
                                    <div class="goBtn target btn-member">马上行动</div>
                                @else
                                    <div class="goBtn">活动已结束</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="title">任务三</div>
                                <div class="desc">邀请朋友注册<br><span>每位可获得 ALLN {{$coin_airdrops[3]->iBonus or 0}}枚</span></div>
                                @if($coin_airdrops[3])
                                    <div class="goBtn target btn-member">马上行动</div>
                                @else
                                    <div class="goBtn">活动已结束</div>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="text">如您未完成KYC资料填写，将无法领取奖励。</div>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            $(".btn-register").click(function () {
                location.href = "{{url('register')}}"
            })
            //
            $(".btn-member").click(function () {
                location.href = "{{url('member')}}#p3"
            })
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->