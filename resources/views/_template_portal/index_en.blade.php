@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="/portal_assets/htm/css/index.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <div class="canvasArea">
        <div id="particle-canvas"></div>
    </div>
    <!-- 內容區塊 -->
    <div class="content">
        <!-- 區塊／輪播／01-大圖輪播 -->
        <section class="section" id="sec1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="leftText">
                            <div class="title">{{trans('aln.sec1.title')}}</div>
                            {{--<div class="activityTime">--}}
                            {{--<div class="titleS"><span>{{$activity->iBonus or 0}}%</span> Time up for reward in</div>--}}
                            {{--</div>--}}
                            <div class="desc"><strong>PRE-ICO has been finished.</strong><br>
                                <small>Let's make ALLN the Visa of Cryptocurrency.</small>
                            </div>

                            <div class="btnGroup">
                                <div class="btn lPaperBtn" data-toggle="modal" data-target="#lightPaperModal"> Light Paper</div>
                                <div class="btn wPaperBtn" data-toggle="modal" data-target="#whitePaperModal"> White Paper</div>
                                <div class="btn buyBtn btn-invest"><i class="fas fa-arrow-right"></i> Sign Up</div>
                            </div>
                            <div class="btnGroup center share">
                                <div class="title">Follow us：</div>
                                <div class="btn"><a href="javascript:void(0);" data-toggle="modal" data-target="#weChat"><i class="fab fa-weixin"></i></a></div>
                                <div class="btn"><a href="https://twitter.com/ALLNTOKEN" target="_blank"><i class="fab fa-twitter"></i></a></div>
                                <div class="btn"><a href="https://t.me/allntoken" target="_blank"><i class="fab fa-telegram-plane"></i></a></div>
                                <div class="btn"><a href="https://www.facebook.com/ALLNTOKEN" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
                                <div class="btn"><a href="https://medium.com/@allntoken" target="_blank"><i class="fab fa-medium-m"></i></a></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="rightText">
                            <div class="iframeCnt">
                                <iframe src="https://player.vimeo.com/video/265489249" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- 區塊／入口／02-類別入口 -->
        <section class="section" id="sec3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="leftText">
                            <div class="coin"><img src="/portal_assets/htm/img/coin.png" alt=""></div>
                            <div class="title">ALLN</div>
                            <div class="desc">Airline and Life Networking Token is a decentralized application platform for real consumption created by Huafu Enterprise Holdings
                                Limited
                            </div>
                            <div class="btn seeBtn"><i class="fas fa-arrow-right"></i> see Benefits</div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="rightText">
                            <ul class="fa-ul">
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    The ALLN system aims to interconnecting with the flight service of Far Eastern Air Transport Corporation (hereafter referred to as "FAT"),
                                    enhancing the possibility of further densifying and broadening its navigation network in various regions.
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    Through associating the cryptocurrency with the solid consumption economy supported by large enterprises.
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    Intending to be a leading blockchain-based system for real consumption.
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    ALLN system can serve as a transaction channel for smart contracts, consumption and asset allocation.
                                </li>
                                <li><span class="fa-li">
                                    <i class="fas fa-plane"></i></span>
                                    Realize infinite application of the cryptocurrency.
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="iframeCnt">
                            <iframe src="https://player.vimeo.com/video/265489249" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row cubeCnt">
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>9</span>years</div>
                                    <div class="desc">ALLN was built at HK for 9 years</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>61</span>years</div>
                                    <div class="desc">Major strategic partner Fat has established for 61 years</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>20</span>K</div>
                                    <div class="desc">FAT provides more than 20,000 flights each year</div>
                                </div>
                            </div>
                        </div>
                        <div class="row cubeCnt">
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>4</span>years</div>
                                    <div class="desc">FAT has been certified as the best professional maintenance company in Taiwan area, from 2015 to 2018</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>3</span>million</div>
                                    <div class="desc">FAT carries more than 3 million passengers among major cities</div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="cube">
                                    <div class="title"><span>429</span>%</div>
                                    <div class="desc">FAT's profit has increased by 429% in 2014</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="whiteArraw"></div>
        </section>
    {{--<section class="section" id="secA">--}}
    {{--<div class="container">--}}
    {{--<div class="articleCnt">--}}
    {{--<div class="title text-left"><span>ALLN</span> Acquire Aircrafts From</div>--}}
    {{--<div class="row">--}}
    {{--<div class="col-xs-12 col-sm-4">--}}
    {{--<div class="cube c1">--}}
    {{--<div class="buyIcon">--}}
    {{--<img src="/portal_assets/htm/img/ALLN_aircraft.png">&nbsp;&nbsp;Buy--}}
    {{--</div>--}}
    {{--<div class="circleCnt"></div>--}}
    {{--<table class="table">--}}
    {{--<tr>--}}
    {{--<td>Model</td>--}}
    {{--<td>737 MAX-8</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Cost</td>--}}
    {{--<td>117.1M USD</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-12 col-sm-4">--}}
    {{--<div class="cube c2">--}}
    {{--<div class="buyIcon">--}}
    {{--<img src="/portal_assets/htm/img/ALLN_aircraft.png">&nbsp;&nbsp;Buy--}}
    {{--</div>--}}
    {{--<div class="circleCnt"></div>--}}
    {{--<table class="table">--}}
    {{--<tr>--}}
    {{--<td>Model</td>--}}
    {{--<td>ATR72-600</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Cost</td>--}}
    {{--<td>27M USD</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-xs-12 col-sm-4">--}}
    {{--<div class="cube c3">--}}
    {{--<div class="buyIcon">--}}
    {{--<img src="/portal_assets/htm/img/ALLN_aircraft.png">&nbsp;&nbsp;Buy--}}
    {{--</div>--}}
    {{--<div class="circleCnt"></div>--}}
    {{--<table class="table">--}}
    {{--<tr>--}}
    {{--<td>Model</td>--}}
    {{--<td>A320 NEO<br>A321 NEO</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>Cost</td>--}}
    {{--<td>110.6M USD<br>129.5M USD</td>--}}
    {{--</tr>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</section>--}}
    <!-- 區塊／入口／03-類別入口 -->
        <section class="section" id="secE">
            <div class="container">
                <div class="articleCnt">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="itemCnt">
                                <img src="/portal_assets/htm/img/plan_en.png">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="itemCnt">
                                <div class="title"><span>ALLN</span> Application</div>
                                <div class="desc">ALLN takes the consumption style of credit card as a reference to establish a clearing center(Settlement center) for procedures in
                                    order to reduce the waiting time for exchanging commodities and forming business cooperation efficiently. To make Cryptocurrency a practical
                                    use.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- 區塊／入口／03-類別入口 -->
        <section class="section" id="secB">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">The benefit of <span>ALLN</span> Token holder</div>
                    <div class="row cubeCnt">
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">1</div>
                                <div class="title">Discount</div>
                                <div class="desc">Offer the exchange of tickets, merchandise and service in all service systems of Far Eastern Air Transport with a discount of 12%~45% off.</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">2</div>
                                <div class="title">Ticket Exchange</div>
                                <div class="desc">The ALLN can be used to exchange for the FAT tickets and its peripheral services.</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">3</div>
                                <div class="title">Shopping Discount</div>
                                <div class="desc">The ALLN can be used to purchase on the plane with high discount.</div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="cube">
                                <div class="coinCircle"></div>
                                <div class="numberCircle">4</div>
                                <div class="title">Quickly Expansion</div>
                                <div class="desc">ALLN will be accepted by subsidiary companies of the FAT's affiliate companies, so as to acquire a large number of transnational users .</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    @if(count($start_time))
        <!-- 區塊／入口／03-類別入口 -->
            <section class="section" id="secF">
                <div class="container">
                    <div class="title text-center">ALLN Promotion Program</div>
                    <div class="articleCnt">
                        @foreach( $start_time as $key => $item )
                            <div class="row items">
                                <div class="col-xs-4">{{$start_time[$key]}} - {{$end_time[$key]}}</div>
                                <div class="col-xs-4">{{($bonus[$key] > 0 )? $bonus[$key]:'??'}}% Bonus</div>
                                <div class="col-xs-4 status">
                                    <div class="sale">SALE ENDS IN</div>
                                    <div class="time">{{((strtotime($end_time[$key]) - strtotime($start_time[$key]))/3600/24)+1}} days</div>
                                    <div class="icon"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>
    @endif
    <!-- 區塊／入口／03-類別入口 -->
        {{--<section class="section" id="secC">--}}
            {{--<div class="container">--}}
                {{--<div class="articleCnt">--}}
                    {{--<div class="title"><span>ALLN</span> Schedule</div>--}}
                    {{--<div class="row">--}}
                        {{--<div class="itemCnt">--}}
                            {{--<img src="/portal_assets/htm/img/time_en.png">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}

        <!-- 區塊／入口／03-類別入口 -->
        <section class="section" id="secD">
            <div class="container">
                <div class="articleCnt a1">
                    <div class="title">Introduction to ALLN Team</div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail boss">
                                <img src="/portal_assets/htm/img/CEO.jpg">
                                <div class="caption text-center">
                                    <h3 class="text-center">TSENG,CHIN-CHIH
                                        <small>CEO</small>
                                    </h3>
                                    <div class="item">
                                        Founder of the Issuer<br>
                                        <strong>Education:</strong>
                                        <br>Master of Applied Chemistry, National Chiao Tung University<br><br>
                                        <strong>Experience:</strong>
                                        <br>COO, Far Eastern Air Transport Corporation<br>Managing the group's aviation, finance, hotel, and tourism business, assessing
                                        real estate development investment, and planning operations
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CFO.jpg">
                                <div class="caption text-center">
                                    <h3 class="text-center">JOSHUA CHEN
                                        <small>CFO</small>
                                    </h3>
                                    <div class="item">
                                        <strong>Education:</strong>
                                        <br>Master of Business Administration, Northumbria University, UK<br><br>
                                        <strong>Experience:</strong>
                                        <br>Chief of Financial Section, EVA AIRWAYS CORPORATION<br>Assistant Vice President of Financial Section, Far Eastern Air
                                        Transport Corporation
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/GC.jpg">
                                <div class="caption text-center">
                                    <h3 class="text-center">MARLENE DU
                                        <small>GC</small>
                                    </h3>
                                    <div class="item">
                                        <strong>Education:</strong>
                                        <br>Bachelor of Law, National Taiwan University<br><br>
                                        <strong>Experience:</strong>
                                        <br>Managing Partner, Mingzhan Commercial Law Firm,<br>Deputy Minister, Financial Law Commission, Taiwan Bar Association<br>Junior
                                        Partner, PCL TransAsia Law Offices
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="col-xs-12 col-sm-6 col-md-6">--}}
                            {{--<div class="thumbnail">--}}
                                {{--<img src="/portal_assets/htm/img/CMO.jpg">--}}
                                {{--<div class="caption text-center">--}}
                                    {{--<h3 class="text-center">JAMES YANG--}}
                                        {{--<small>CMO</small>--}}
                                    {{--</h3>--}}
                                    {{--<div class="item">--}}
                                        {{--<strong>Education:</strong>--}}
                                        {{--<br>Master of Design, National Taiwan Normal University<br><br>--}}
                                        {{--<strong>Experience:</strong>--}}
                                        {{--<br>CSO(Chief Strategy Officer), Far Eastern Air Transport Corporation <br>General Manager, Shopping Mall Business Group,<br>Specializing--}}
                                        {{--in developing, operating and brand marketing for department stores and shopping malls--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CPO.jpg">
                                <div class="caption text-center">
                                    <h3 class="text-center">PATTY LU
                                        <small>CPO</small>
                                    </h3>
                                    <div class="item">
                                        <strong>Education:</strong>
                                        <br>Bachelor of Industrial Engineering and Management, National Taipei University of Technology<br>Maintaining relationships
                                        with the media, holding press conferences, releasing news, conducting dynamic analysis on the media, interviewing with
                                        reporters, arranging special reports on enterprises, engaging in other communication with the media, and monitoring the effects
                                        of advertisements<br><br>
                                        <strong>Current Position:</strong>
                                        <br>Vice President, Far Eastern Air Transport Corporation
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CAO.jpg">
                                <div class="caption text-center">
                                    <h3 class="text-center">JOSEPH LEE
                                        <small>CAO</small>
                                    </h3>
                                    <div class="item">
                                        <strong>Education:</strong>
                                        <br>Bachelor of German Language and Culture, Fu Jen Catholic University<br><br>
                                        <strong>Experience:</strong>
                                        <br>Lecturer of English and German, YMCA,<br>Representative in Europe, Japan, and Mainland China, <br>President, Cambodia
                                        Airlines,<br>President, Great Star Travel,<br>Representative, YKT CORPORATION, Japan, Chairman, Shuo Li Interior Design and
                                        Engineering Co., Ltd.<br><br>
                                        <strong>Current Position:</strong>
                                        <br>President, Far Eastern Air Transport Corporation
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CDO.jpg">
                                <div class="caption text-center">
                                    <h3 class="text-center">LORITA WANG
                                        <small>CDO</small>
                                    </h3>
                                    <div class="item">
                                        <strong>Education:</strong>
                                        <br>Bachelor of Computer Science and Information Engineering, Ming Chuan University<br><br>
                                        <strong>Experience:</strong>
                                        <br>Vice President of Services, TransAsia Airways,<br>Specializing in air and ground crews’ operations and the according
                                        standard operation procedure, ground handling agent business, and private operation of business aircraft<br><br>
                                        <strong>Current Position:</strong>
                                        <br>Assistant Vice President of Information Department, Far Eastern Air Transport Corporation
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="thumbnail">
                                <img src="/portal_assets/htm/img/CTO.jpg">
                                <div class="caption">
                                    <h3 class="text-center">EDDY CHEN
                                        <small>CTO</small>
                                    </h3>
                                    <div class="item">
                                        <strong>Education:</strong>
                                        <br>Master of Computer Science, Institute of Data Science and Engineering, National Chiao Tung University<br><br>
                                        <strong>Experience:</strong>
                                        <br>Manager, ORACLE CORPORATION,<br>Manager, International Integrated Systems, Inc.<br>Manager, Sun Microsystems, Inc.<br>Manager,
                                        Deloitte Touche Tohmatsu Limited<br>Specializing in integration and development of information system, <br>information security,
                                        and aviation e-commerce<br><br>
                                        <strong>Current Position:</strong>
                                        <br>Assistant Vice President of Information Department, Far Eastern Air Transport Corporation
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="articleCnt a2">--}}
                {{--<div class="row">--}}
                {{--<div class="col-sm-offset-3 col-sm-6">--}}
                {{--<div class="title">Adviser</div>--}}
                {{--<div class="row">--}}
                {{--<div class="col-xs-6 col-sm-6">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="/portal_assets/htm/img/professor.jpg" class="img-circle">--}}
                {{--<div class="caption text-center">--}}
                {{--<h3>Ruei-Shan Lu Blockchain Professor</h3>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xs-6 col-sm-6">--}}
                {{--<div class="thumbnail">--}}
                {{--<img src="/portal_assets/htm/img/lawyer.jpg" class="img-circle">--}}
                {{--<div class="caption text-center">--}}
                {{--<h3>Warren Pan Lawer</h3>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-sm-12">--}}
                {{--<div class="title">Strategic Partners</div>--}}
                {{--<div class="thumbnail">--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners1.png"></div>--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners2.png"></div>--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners3.png"></div>--}}
                {{--<div class="col-xs-6 col-sm-3"><img src="/portal_assets/htm/img/partners4.png"></div>--}}
                {{--<div class="caption text-center">--}}
                {{--<p>Based on the development plan, ALLN will select and invite the leading enterprises in global emerging industries to build--}}
                {{--strategic cooperation relationships and participate in the ALLN application system. It is the strategic cooperative partners--}}
                {{--that develop and execute the projects for practical operations of the ALLN.</p>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                <div class="articleCnt a3">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="title">Consultants</div>
                            <br><br>
                        </div>
                        <div class="col-sm-12 photo">
                            <div class="row">
                                <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-2">
                                    <div class="thumbnail">
                                        <img src="/portal_assets/htm/img/p5.jpg" class="img-circle">
                                        <div class="caption text-center">
                                            <h3>Eric Gu  Metaverse Founder &CEO</h3>
                                            <p>20 years working experience in Canada and U.S.A.<br>Senior Programmer and Project Manager in large enterprise of Ontario, Canada.<br>2013 joined communities of Bitcoin and blockchain, and participated international projects and high-tech research of blockchain in early development stage.<br>Wrote and translated articles and books about blockchain, included Melanie Swan’s “Blockchain: Blueprint for a New Economy”.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0">
                                    <div class="thumbnail">
                                        <img src="/portal_assets/htm/img/p6.png" class="img-circle">
                                        <div class="caption text-center">
                                            <h3>Chandler Guo</h3>
                                            <p>Famous blockchain investor.<br>Bitcoin early investor.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- 區塊／資訊／01-固定六個資訊 -->
        <section class="section information num-1" id="sec4">
            <div class="container">
                <div class="title-region">{{trans('aln.sec4.title')}}</div>
                <div class="row">
                    @foreach($news as $key =>$item)
                        <div class="col-sm-4">
                            <div class="itemBox thumbnail">
                                <a href="{{$item->url}}" target="_blank">
                                    <div class="imgBox rImg"><img src="{{$item->vImages}}" alt=""></div>
                                    <div class="text-box">
                                        <div class="descBox ellipsis">{{$item->vTitle}}</div>
                                        <div class="more-box"><span><i class="fa fa-caret-right" aria-hidden="true"></i></span>{{trans('aln.sec4.link')}}</div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <!-- 區塊／入口／08-類別入口 -->
        <section class="section" id="sec8">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">Get the Updates Earlier</div>
                    <div class="desc">Intend to get updates earlier including ways to get rewards, news and activities.</div>
                    <div class="input-group input-group-lg">
                        <input type="email" class="form-control vEmail" placeholder="Email addres">
                        <span class="input-group-btn">
                            <button class="btn joinBtn" type="button">Willing to Get</button>
                        </span>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- modal -->
    <div class="modal fade" id="warningModel" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">重要信息澄清</h3>
                </div>
                <div class="modal-body text-white text-left">
                    近日发现不肖人士假借ALLN官方名义于中文社群公告ALLN发展情形，今与策略合作伙伴远东航空共同予以澄清如下：<br><br>
                    <ol>
                        <li>「远航币」、「远航ALLN币」、「航空币」等皆为诈欺行为，远东航空于2018年5月7日晚间由律师具状向台北市警察局刑事警察大队科技犯罪侦察队提出诈骗刑事告诉。</li>
                        <li>不实消息指称ALLN已获得包括：火币、币安签约投资并非事实。</li>
                        <li>ALLN尚未确认于任何交易所挂牌，相关事务仍在洽谈阶段。</li>
                        <li>ALLN为Huafu Enterprise Holdings Limited (Hong Kong)所发行之区块链应用项目，ALLN不保证任何倍数获利，参与者应了解区块链技术应用前提下理性参与。</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script type="text/javascript" src="/portal_assets/dist/timers/jquery.timers.min.js"></script>
    {{--<script type="text/javascript" src="/portal_assets/htm/js/index.js"></script>--}}
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <!--  -->
    @include('_template_portal._js.index')
@endsection
<!-- ================== /inline-js ================== -->