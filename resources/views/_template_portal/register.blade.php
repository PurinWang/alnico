@extends('_template_portal._layouts._main')
<!-- ================== page-css ================== -->
@section('page-css')
    <!--  -->
    <link rel="stylesheet" type="text/css" href="portal_assets/htm/css/register.css">
@endsection
<!-- ================== /page-css ================== -->
<!-- content -->
@section('content')
    <!--  -->
    <div class="content">
        <!-- 區塊／入口／01-類別入口 -->
        <div class="section" id="sec1">
            <div class="container">
                <div class="articleCnt">
                    <div class="title">{!! trans('register.title') !!}</div>
                    <div class="desc">{{trans('register.desc')}}</div>
                    <form>
                        <div class="form-group">
                            <input type="text" class="form-control email" placeholder="{{trans('register.email')}}">
                        </div>
                        <div class="form-group">
                            <select class="form-control vCountry" id="country"></select>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control password" placeholder="{{trans('register.password')}}">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control confirm_password" placeholder="{{trans('register.confirm_password')}}">
                        </div>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="cb2" class="i-agree-riskDesc">{!! trans('register.riskDesc') !!}
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="cb2" class="i-agree-privacyDesc">{!! trans('register.privacyDesc') !!}
                                </label>
                            </div>
                        </div>
                        <div class="note">
                            <div class="text">{{trans('register.note')}}：<br><br>
                                <ol class="text-left">
                                    <li>{{trans('register.note_1')}}</li>
                                    <li>{{trans('register.note_2')}}</li>
                                    <li>{!! trans('register.note_3') !!}</li>
                                </ol>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="btn sendBtn btn-register"><i class="fas fa-arrow-right"></i> {{trans('register.register')}}</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
<!-- /content -->

<!-- ================== page-js ================== -->
@section('page-js')
    <!--  -->
    <script src="/_assets/country-state-select.js"></script>
    <!-- Plugin Customer-->
    <script type="text/javascript" src="/_assets/CryptoJS/rollups/md5.js"></script>
@endsection
<!-- ================== /page-js ================== -->
<!-- ================== inline-js ================== -->
@section('inline-js')
    <!--  -->
    <script>
        $(document).ready(function () {
            //
            populateCountries("country");
            //
            $(".btn-goLogin").click(function () {
                location.href = "{{url('login')}}";
            });
            //
            $(".btn-register").click(function () {
                //
                if ($('.i-agree-riskDesc').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('register.riskDesc_title')}}', content: '{{trans('register.riskDesc_content')}}'});
                    return false;
                }
                if ($('.i-agree-privacyDesc').prop("checked")) {

                } else {
                    modal_show({title: '{{trans('register.privacyDesc_title')}}', content: '{{trans('register.privacyDesc_content')}}'});
                    return false;
                }

                if ($(".email").val() == "") {
                    modal_show({title: 'Email', content: $(".email").attr('placeholder') + '{{trans('register.error')}}'});
                    $(".email").focus();
                    return false;
                } else {
                    if (!reg_Email.test($(".email").val())) {
                        modal_show({title: 'Email', content: '{{trans('register.email_content')}}'});
                        $(".email").val().focus();
                        return false;
                    }
                }
                if ($(".vCountry").val() == -1) {
                    modal_show({title: 'Country', content: '{{trans('register.country_empty')}}'});
                    return false;
                }
                if ($(".password").val() == "") {
                    modal_show({title: 'Password', content: $(".password").attr('placeholder') + '{{trans('register.error')}}'});
                    $(".password").focus();
                    return false;
                }
                if ($(".password").val() != $(".confirm_password").val()) {
                    modal_show({title: 'ConfirmPassword', content: '{{trans('register.password_content')}}'});
                    return false;
                }

                var data = {"_token": "{{ csrf_token() }}"};
                data.vAccount = $(".email").val();
                data.vCountry = $(".vCountry").val();
                data.vPassword = CryptoJS.MD5($(".password").val()).toString(CryptoJS.enc.Base64);
                $.ajax({
                    url: "{{url('doRegister')}}",
                    data: data,
                    type: "POST",
                    resetForm: true,
                    success: function (rtndata) {
                        if (rtndata.status) {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: "{{trans('register.success_content')}}"});
                            setTimeout(function () {
                                location.href = rtndata.rtnurl;
                            }, 1000)
                        } else {
                            modal_show({title: '{{trans('_web_alert.notice')}}', content: rtndata.message});
                        }
                    }
                });
            });
        });
    </script>
@endsection
<!-- ================== /inline-js ================== -->