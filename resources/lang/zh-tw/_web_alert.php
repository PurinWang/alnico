<?php
return [ 
		"agree_err" => "請勾選我同意",
		'cancel' => '取消',
		"cropper_success" => "圖片裁切成功",
		"cropper_image_too_big" => "檔案過大，不得超過",
		"cropper_image" => "圖片裁切",
		"del" => [ 
				'title' => '刪除',
				'note' => '刪除後將無法復原' 
		],
		'logout' => [ 
				'title' => '確定登出?',
				'note' => '若資料尚未儲存，登出後將會遺失',
				'cancel' => '取消',
				'ok' => '確定',
				'success' => '登出成功' ,
				'content' => '登出成功'
		],
		"not_work" => "功能尚未開放",
		"notice" => "提示訊息",
		'ok' => '確認',
		"register" => [ 
				"repassword_err" => "確認密碼與密碼不符" 
		],
		'upload' => [ 
				'image' => '上傳圖片',
				'image_preview' => '預覽圖片',
				'image_new' => '上傳新圖片',
				'image_crop' => '裁切' 
		] 
];
