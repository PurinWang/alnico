<?php
return [
    "add_success" => "新增成功",
    "add_fail" => "新增失敗",
    "checkPassportTotal_success" => "訂購金額確認完成",
    "checkPassportTotal_fail" => "訂購金額已達上限",
    'already_done' => '您已經回答過了',
    'cart_existed' => '已在購物車',
    'create_position' => '請先建立對應工位',
    "delete_success" => "刪除成功",
    "delete_fail" => "刪除失敗",
    "empty_id" => "資料不存在",
    "cart_is_empty" => "購物車已無商品",
    "error_password" => "密碼錯誤",
    "keep_remove" => "取消收藏",
    "keep_remove_fail" => "取消收藏失敗",
    "login" =>
        [
            "success" => "成功登入",
            "error_account" => "帳號不存在",
            "error_password" => "密碼錯誤",
            "error_access" => "帳號權限異常，請聯絡管理員",
            "error_active" => "帳號尚未啟用",
            "error_status" => "帳號停權，請聯絡客服人員"
        ],
    "logout" =>
        [
            "success" => "成功登出",
            "content" => "成功登出"
        ],
    "register" =>
        [
            "success" => "註冊成功",
            "fail" => "註冊失敗",
            "account_not_empty" => "帳號已存在",
            "error_account" => "帳號格式錯誤"
        ],
    "save_success" => "修改成功",
    "save_fail" => "修改失敗",
    "upload" =>
        [
            "success" => "上傳成功",
            "fail" => "上傳失敗",
            "account_not_empty" => "帳號已存在",
            "error_account" => "帳號格式錯誤"
        ],
    "verification" =>
        [
            "forgot_password" => "忘記密碼",
            "old_member_login" => "舊會員登入",
            "success" => "已送出驗證碼",
            "fail" => "系統錯誤",
            "error" => "輸入資料有誤，請再次確認"
        ],
];
