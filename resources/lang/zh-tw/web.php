<?php
return [
    'account' => '帳號',
    'account_input' => '請輸入帳號 / Email',
    'account_type' => '帳號類型',
    'active' => '帳號啟用',
    'active_false' => '未啟用',
    'active_true' => '啟用',
    'add' => '新增',
    'add_area' => '新增區塊',
    'add_question' => '新增題目',
    'advance_mode' => '進階模式',
    'admin' =>
        [
            'category' =>
                [
                    'add' => '新增類別',
                    'edit' => '編輯類別',
                    'category_value' => '類別代碼',
                    'category_name' => '類別名稱',
                    'sub_add' => '新增子項目',
                    'sub_edit' => '編輯子項目',
                ],
            'announcement' =>
                [
                    'add' => '新增公告',
                    'edit' => '編輯公告',
                ],
            'store' =>
                [
                    'add' => '新增特店',
                    'edit' => '編輯特店',
                ],
            'advertising' =>
                [
                    'add' => '新增廣告',
                    'edit' => '編輯廣告',
                ],
            'product' =>
                [
                    'add' => '新增公告',
                    'edit' => '編輯公告',
                ],
        ],
    'agree' => '同意',
    'cancel' => '取消',
    'credit_card' =>
        [
            'banner' =>
                [
                    'add' => '新增',
                    'edit' => '編輯',
                ],
            'member_area' =>
                [
                    'promo' =>
                        [
                            'add' => '新增',
                            'edit' => '編輯',
                        ],
                    'store' =>
                        [
                            'add' => '新增',
                            'edit' => '編輯',
                        ],
                ],
        ],
    'doadd' => '新增',
    'dosave' => '儲存',
    'duty_free' =>
        [
            'rank' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'banner' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'advertising' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'advertising2' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'recommend' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
        ],
    'editor' => '編輯內文',
    'forgotpassword' =>
        [
            'title' => '忘記密碼-密碼重設',
            'verification' => '請至信箱收取驗證碼',
            'new_password' => '設置新密碼',
            'confirm_password' => '確認新密碼',
            'reset_password' => '確定重設密碼',
            'register' => '需要一組帳號?',
            'create_account' => '建立帳號',
            'reset_password' => '重設密碼',
            'account' => '請輸入帳號 / Account',
            'password' => '請輸入密碼 / Password',
            'forgot_password' => '忘記密碼?',
            'account_empty' => '請輸入帳號',
            'account_fail' => 'Email格式錯誤',
            'email' => '請輸入註冊時，所填的Email',
            'remembered' => '我想起密碼了!'
        ],
    'goback' => '回上一層',
    'images' => '圖片',
    'include_article' => '載入文章',
    'include_images' => '載入圖片',
    'include_group_manager' => '選擇負責人',
    'include_video' => '載入影片',
    'interview' =>
        [
            'index_position' => '應用的工位',
            'index_summary' => '題庫簡介',
            'index_title' => '題庫名稱',
            'index_add_question' => '新增題目',
            'index_edit_question' => '編輯題目',
            'index_question_title' => '題目',
            'index_question_summary' => '題目提示',
            'index_question_type' => '題目類型',
            'index_question_must' => '是否必填',
            'index_question_option' => '選項'
        ],
    'japan' =>
        [
            'rank' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'banner' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'advertising' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'advertising2' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
            'recommend' =>
                [
                    'edit' => '修改',
                    'add' => '新增',
                    'sub' =>
                        [
                            'add' => '新增',
                            'edit' => '修改',
                        ],
                ],
        ],
    'login' =>
        [
            'register' => '需要一組帳號?',
            'create_account' => '建立帳號',
            'login' => '登入',
            'account' => '請輸入帳號 / Account',
            'password' => '請輸入密碼 / Password',
            'forgot_password' => '忘記密碼?',
            'stay_signed_in' => '保持登入',
            'account_empty' => '請輸入帳號',
            'account_fail' => 'Email格式錯誤',
            'password_empty' => '請輸入密碼',
            'password_more_4' => '密碼需大於4字元',
            'password_less_20' => '密碼需少於20字元',
            'footer' => 'Pin2wall',
            'none' => '請登入會員'
        ],
    'material' =>
        [
            'article_title' => '文章標頭',
            'article_summary' => '文章簡介',
            'video_title' => '影片標頭',
            'video_summary' => '影片簡介'
        ],
    'member_image' => '頭像',
    'news' =>
        [
            'index_author' => '發佈者',
            'index_date' => '公告日期',
            'index_show_date' => '刊登日期',
            'index_start_date' => '公告開始時間',
            'index_end_date' => '公告結束時間',
            'index_store' => '店家',
            'index_store_select' => '請選擇店家',
            'index_summary' => '公告簡介',
            'index_title' => '公告標頭',
            'index_type' => '公告屬性'
        ],
    'open' => '是否公開',
    'open_false' => '否',
    'open_true' => '是',
    'password' => '密碼',
    'password_input' => '請輸入密碼 / Password',
    'product' =>
        [
            'source' =>
                [
                    'add' => '新增',
                    'title' => '來源名稱',
                    'url' => '來源路徑',
                ],
        ],
    'register' =>
        [
            'already_register' => '已經註冊了?',
            'login' => '登入',
            'register' => '註冊',
            'username' => '請輸入大名',
            'account' => '請輸入帳號',
            'account_fail' => 'Email格式錯誤',
            'password' => '請輸入密碼',
            'repassword' => '請再次輸入密碼',
            'repassword_check' => '確認密碼與密碼不同',
            'password_more_4' => '密碼需大於4字元',
            'password_less_20' => '密碼需少於20字元',
            'agree' => '我同意 <a href="#" data-toggle="modal" data-target="#myAgree"> 使用者條款 </a>',
            'agree_title' => '註冊須知',
            'agree_content_h1' => '使用者條款',
            'agree_content_h2' => '使用者條款',
            'agree_content' => '使用者條款使用者條款',
            'agree_check' => '您必須同意使用者條款 ',
            'footer' => 'Pin2wall',
            'register_success' => '感謝您的註冊!'
        ],
    'repassword' => '再次確認密碼',
    'repassword_input' => '請再次輸入密碼 / Password',
    'resetwidgets' => "<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings.",
    'save' => '儲存',
    'send' => '送出',
    'show' => '是否上架',
    'show_false' => '否',
    'show_true' => '是',
    'status' => '帳號狀態',
    'status_false' => '停權',
    'status_true' => '正常',
    'store' =>
        [
            'agreement_summary' => '合約簡介',
            'agreement_title' => '合約名稱',
            'index_address' => '分店地址',
            'index_zipcode' => '郵遞區號',
            'index_state' => '州/縣市',
            'index_city' => '城市',
            'index_street' => '街道,門牌',
            'index_contact' => '分店電話',
            'index_summary' => '分店簡介',
            'index_title' => '分店名稱',
            'index_password' => '修改密碼',
            'index_password_old' => '請輸入舊密碼 ( 初次設定無須填寫 )',
            'index_password_new' => '請輸入新密碼',
            'index_repassword' => '請再次輸入新密碼',
            'index_add_position' => '新增分店職位',
            'index_position_title' => '職位名稱',
            'index_position_summary' => '職位簡介',
            'position' => '工位',
            'position_doadd' => '新增',
            'position_summary' => '工位簡介',
            'position_title' => '工位名稱',
            'position_add_checklist' => '新增CheckList',
            'position_checklist_title' => 'CheckList名稱',
            'position_checklist_summary' => 'CheckList簡介'
        ],
    'store_manager' =>
        [
            'member_number' => '員工編號',
            'member_name' => '員工姓名',
            'member_summary' => '員工簡介',
            'birthday' => '生日',
            'first_day' => '到職日',
            'last_day' => '離職日',
            'member_address' => '員工地址',
            'member_zipcode' => '郵遞區號',
            'member_state' => '州/縣市',
            'member_city' => '城市',
            'member_street' => '街道,門牌',
            'member_contact' => '員工電話'
        ],
    'title' => '管理後台',
    'top' => '是否置頂',
    'top_false' => '否',
    'top_true' => '是',
    'training' =>
        [
            'category' => '類別',
            'category_add' => '新增類別',
            'category_value' => '類別代碼',
            'category_name' => '類別名稱',
            'teaching_material_usetype' => '用途',
            'teaching_material_start_date' => '開始時間',
            'teaching_material_end_date' => '結束時間',
            'teaching_material_title' => '標題',
            'teaching_material_summary' => '簡介',
            'examination_usetype' => '用途',
            'examination_summary' => '題庫簡介',
            'examination_title' => '題庫名稱',
            'examination_add_question' => '新增題目',
            'examination_edit_question' => '編輯題目',
            'examination_question_title' => '題目',
            'examination_question_summary' => '題目提示',
            'examination_question_type' => '題目類型',
            'examination_question_must' => '是否必填',
            'examination_question_option' => '選項'
        ],
    'username' => '使用者名稱',
    'username_input' => '請輸入使用者名稱 / UserName',
    'yes' => '是',
    'no' => '否',
    'travel' =>
        [
            'id' => '編號',
            'image' => '圖片',
            'url' => '連結網址',
            'updateTime' => '更新時間',
            'status' => '狀態',
            'action' => '動作',
            'activity_title' => '活動主題',
            'activity_summary' => '活動簡介',
            'travel_type' => '旅遊型態',
            'travel_title' => '行程主題',
            'travel_summary' => '行程簡介',
            'travel_number' => '行程號',
            'schedule_number' => '團號',
            'outset_date' => '出發日期',
            'price' => '售價',
            'price_agent' => '特價',
            'theme_title' => '主題',
            'theme_type' => '主題類別',
            'theme_summary' => '主題簡介',
            'recommand_title' => '推薦區域',
            'viewpoint_summary' => '熱門景點簡介',
            'area' => '區域',
            'city' => '城市',
            'rank' => '排序',
            'save' => '儲存',
            'back' => '返回',
            'banner' =>
                [
                    'add' => '新增',
                    'edit' => '編輯',
                ],
            'activity' =>
                [
                    'title' => '旅遊活動',
                    'manage' =>
                        [
                            'add' => '新增旅遊活動',
                        ],
                ],
            'theme' =>
                [
                    'title' => '主題行程',
                    'category' =>
                        [
                            'add' => '新增主題',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦行程',
                    'category' =>
                        [
                            'add' => '新增推薦區域',
                        ],
                ],
            'viewpoint' =>
                [
                    'title' => '熱門景點',
                    'add' => '新增熱門景點',
                ],
        ],
    'travelticket' =>
        [
            'id' => '編號',
            'image' => '圖片',
            'url' => '連結網址',
            'updateTime' => '更新時間',
            'status' => '狀態',
            'action' => '動作',
            'activity_title' => '活動主題',
            'activity_summary' => '活動簡介',
            'travel_type' => '旅遊型態',
            'travel_title' => '行程主題',
            'travel_summary' => '行程簡介',
            'travel_number' => '行程號',
            'schedule_number' => '團號',
            'outset_date' => '出發日期',
            'price' => '售價',
            'price_agent' => '特價',
            'theme_title' => '主題',
            'theme_type' => '主題類別',
            'theme_summary' => '主題簡介',
            'recommand_title' => '推薦區域',
            'viewpoint_summary' => '熱門景點簡介',
            'iPrice' => '售價',
            'iPriceAgent' => '優惠價',
            'area' => '區域',
            'city' => '城市',
            'rank' => '排序',
            'save' => '儲存',
            'back' => '返回',
            'save_order' =>
                [
                    'success' => '訂購儲存',
                    'fail' => '訂購失敗',
                ],
            'buy' =>
                [
                    'success' => '訂購成功',
                    'fail' => '訂購失敗',
                ],
            'add' =>
                [
                    'success' => '新增成功',
                    'fail' => '新增失敗',
                ],
            'edit' =>
                [
                    'success' => '編輯成功',
                    'fail' => '編輯失敗',
                ],
            'banner' =>
                [
                    'add' => '新增',
                    'edit' => '編輯',
                ],
            'activity' =>
                [
                    'title' => '旅遊活動',
                    'manage' =>
                        [
                            'add' => '新增旅遊活動',
                        ],
                ],
            'theme' =>
                [
                    'title' => '主題行程',
                    'category' =>
                        [
                            'add' => '新增主題',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦行程',
                    'category' =>
                        [
                            'add' => '新增推薦區域',
                        ],
                ],
            'viewpoint' =>
                [
                    'title' => '熱門景點',
                    'add' => '新增熱門景點',
                ],
        ],
];
