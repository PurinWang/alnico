<?php
return [
    "add_success" => "新增成功",
    "add_fail" => "新增失败",
    "checkPassportTotal_success" => "訂購金額確認完成",
    "checkPassportTotal_fail" => "訂購金額已達上限",
    'already_done' => '您已經回答過了',
    'cart_existed' => '已在購物車',
    'create_position' => '請先建立對應工位',
    "delete_success" => "刪除成功",
    "delete_fail" => "刪除失敗",
    "empty_id" => "資料不存在",
    "cart_is_empty" => "購物車已無商品",
    "error_password" => "密碼錯誤",
    "keep_remove" => "取消收藏",
    "keep_remove_fail" => "取消收藏失敗",
    "login" =>
        [
            "success" => "成功登入",
            "error_account" => "帐号不存在",
            "error_password" => "密码错误",
            "error_access" => "帳帐号权限异常，请联络管理员",
            "error_active" => "帐号尚未启用",
            "error_status" => "帐号停权，请联络客服人员"
        ],
    "logout" =>
        [
            "success" => "成功登出",
            "content" => "成功登出"
        ],
    "register" =>
        [
            "success" => "注册成功",
            "fail" => "注册失败",
            "account_not_empty" => "帐号已存在",
            "error_account" => "帐号格式错误"
        ],
    "save_success" => "修改成功",
    "save_fail" => "修改失敗",
    "upload" =>
        [
            "success" => "上傳成功",
            "fail" => "上傳失敗",
            "account_not_empty" => "帳號已存在",
            "error_account" => "帳號格式錯誤"
        ],
    "verification" =>
        [
            "forgot_password" => "忘记密码",
            "old_member_login" => "旧会员登入",
            "success" => "已送出验证码",
            "fail" => "系统错误",
            "error" => "输入资料有误，请再次确认"
        ],
];
