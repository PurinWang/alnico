<?php
return [
    'title' => '忘记密码',
    'desc' => '请输入注册的电子信箱，我们将会发送临时密码给您。',
    'email' => '您的电子邮件信箱',
    'note' => '注意：若您采用的信箱为公用或免费信箱，注册验证信件可能会被归类到垃圾信件。',
    'sendPwBtn' => '送出临时密码',
    'verification' => '请输入临时密码',
    'password' => '请输入新密码',
    'confirm_password' => '再次输入新密码',
    'send' => '送出',
    'error' => '错误',
];
