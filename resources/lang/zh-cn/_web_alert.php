<?php
return [ 
		"agree_err" => "请勾选我同意",
		'cancel' => '取消',
		"cropper_success" => "圖片裁切成功",
		"cropper_image_too_big" => "檔案過大，不得超過",
		"cropper_image" => "圖片裁切",
		"del" => [ 
				'title' => '删除',
				'note' => '删除后将无法复原'
		],
		'logout' => [ 
				'title' => '确定登出?',
				'note' => '若资料尚未储存，登出后将会遗失',
				'cancel' => '取消',
				'ok' => '确定',
				'success' => '登出成功',
				'content' => '登出成功'
		],
		"not_work" => "功能尚未开放",
		"notice" => "提示讯息",
		'ok' => '确认',
		"register" => [ 
				"repassword_err" => "确认密码与密码不符"
		],
		'upload' => [ 
				'image' => '上傳圖片',
				'image_preview' => '預覽圖片',
				'image_new' => '上傳新圖片',
				'image_crop' => '裁切' 
		] 
];
