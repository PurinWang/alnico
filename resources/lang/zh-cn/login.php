<?php
return [
    'title' => '登入',
    'email' => '注册邮箱',
    'password' => '密码',
    'forgotpassword' => '忘记密码',
    'register' => '立即注册',
    'not_sign' => '还不是会员?',
    'login' => '登入',
    'error' => '错误',
    'notice' => '提示讯息',
    'success' => '登入成功',
];
