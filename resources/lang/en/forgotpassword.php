<?php
return [
    'title' => 'Forgot Password',
    'desc' => 'Please input the mail address you used to sign up. We will send you a temporary password to you to reset your password.',
    'email' => 'The Sign-up mail address',
    'note' => 'Note：If you used the free/public mail address to register, the mail used for identity verification might be sent to junk mail box.',
    'sendPwBtn' => 'Send the temporary password',
    'verification' => 'Please input the temporary password',
    'password' => 'Please fill in new password',
    'confirm_password' => 'Reconfirm the new password',
    'send' => 'Send',
    'error' => 'Error',
];
