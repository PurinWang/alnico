<?php
return [
    'title' => 'Log in',
    'email' => 'Mail address',
    'password' => 'Password',
    'forgotpassword' => 'Forgot password',
    'register' => 'Sign up',
    'not_sign' => 'A new account?',
    'login' => 'Log in',
    'error' => 'Error',
    'notice' => 'Message',
    'success' => 'Success',
];
