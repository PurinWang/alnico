<?php
return [
    "add_success" => "Success",
    "add_fail" => "Fail",
    "checkPassportTotal_success" => "訂購金額確認完成",
    "checkPassportTotal_fail" => "訂購金額已達上限",
    'already_done' => '您已經回答過了',
    'cart_existed' => '已在購物車',
    'create_position' => '請先建立對應工位',
    "delete_success" => "刪除成功",
    "delete_fail" => "刪除失敗",
    "empty_id" => "資料不存在",
    "cart_is_empty" => "購物車已無商品",
    "error_password" => "密碼錯誤",
    "keep_remove" => "取消收藏",
    "keep_remove_fail" => "取消收藏失敗",
    "login" =>
        [
            "success" => "Successful",
            "error_account" => "account does not exist",
            "error_password" => "wrong password",
            "error_access" => "Account permissions are abnormal, please contact the administrator",
            "error_active" => "Account is not enabled",
            "error_status" => "Account suspension, please contact customer service"
        ],
    "logout" =>
        [
            "success" => "Successfully logged out",
            "content" => "Successfully logged out"
        ],
    "register" =>
        [
            "success" => "registration success",
            "fail" => "registration failed",
            "account_not_empty" => "Account already exists",
            "error_account" => "account format error"
        ],
    "save_success" => "修改成功",
    "save_fail" => "修改失敗",
    "upload" =>
        [
            "success" => "上傳成功",
            "fail" => "上傳失敗",
            "account_not_empty" => "帳號已存在",
            "error_account" => "帳號格式錯誤"
        ],
    "verification" =>
        [
            "forgot_password" => "忘記密碼",
            "old_member_login" => "舊會員登入",
            "success" => "Sent verification code",
            "fail" => "系統錯誤",
            "error" => "Incorrect data, please confirm again"
        ],
];
