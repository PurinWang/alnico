<?php
return [
    'title' => 'Payment confirmation',
    'allnVal' => 'Please confirm the amount you need to convert',
    'titleS' => 'Wallet address',
    'note' => 'Note',
    'note_1' => 'Please use ETH to deposit funds',
    'note_2' => 'Please confirm the wallet address is correct',
    'note_3' => 'If you use ERC20 purse deposit, the miner\'s fee is recommended to fill in the gas price (usually 20 Gwei), fill in the gas (200000, the excess will be returned), fill in the data (according to the situation)',
    'note_4' => 'After completing the remittance, please be sure to upload the screenshot as a certificate upload',
    'sendBtn' => 'Upload Remittance Screenshot',
];
