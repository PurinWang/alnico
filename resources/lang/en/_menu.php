<?php
return [
    'member' =>
        [
            'title' => '會員',
            'userinfo' =>
                [
                    'title' => '個人資料',
                ]
        ],
    'admin' =>
        [
            'title' => '商城首頁管理',
            'setting' =>
                [
                    'title' => '首頁設置',
                    'header' =>
                        [
                            'title' => 'header設置',
                        ],
                    'footer' =>
                        [
                            'title' => 'footer設置',
                        ],
                    'information' =>
                        [
                            'title' => '資訊設置',
                        ],
                ],
            'browers' =>
                [
                    'title' => '網站流量點擊',
                    'pageview' =>
                        [
                            'title' => '網站流量',
                        ],
                    'click' =>
                        [
                            'title' => '頁面點擊',
                        ],
                ],
            'announcement' =>
                [
                    'title' => '商城公告管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'recommend' =>
                        [
                            'title' => '推薦商品管理',
                            'sub' =>
                                [
                                    'title' => '推薦商品',
                                ],
                        ],
                ],
            'credit_card' =>
                [
                    'title' => '區塊管理',
                    'banner1' =>
                        [
                            'title' => '大圖輪播',
                        ],
                    'banner2' =>
                        [
                            'title' => '功能區',
                        ],
                    'banner3' =>
                        [
                            'title' => '靜態圖',
                        ],
                ],
            'activity' =>
                [
                    'title' => '活動管理',
                ],
            'store' =>
                [
                    'title' => '資訊專區管理',
                ],
            'advertising' =>
                [
                    'title' => '優惠專區管理',
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                ],
            'seo' =>
                [
                    'title' => '網站SEO設置',
                ],
            'ga' =>
                [
                    'title' => 'GA追蹤碼嵌入',
                ],
        ],
    'product' =>
        [
            'title' => '商品管理',
            'japan' =>
                [
                    'title' => '日本完稅館',
                ],
            'duty_free' =>
                [
                    'title' => '馬祖免稅館',
                ],
            'travel' =>
                [
                    'title' => '旅遊淘寶館',
                ],
            'digital_life' =>
                [
                    'title' => '數位生活館',
                ],
            'specialty' =>
                [
                    'title' => '台灣名品館',
                ],
            'fresh_food' =>
                [
                    'title' => '生鮮急送館',
                ],
            'travel_smart' =>
                [
                    'title' => '旅遊配件館',
                ],
        ],
    'intro' =>
        [
            'title' => '商城使用指南',
            'banner' =>
                [
                    'title' => 'Banner管理',
                ],
            'index' =>
                [
                    'title' => '商城介紹',
                ],
        ],
    'credit_card' =>
        [
            'title' => '信用卡館',
            'banner' =>
                [
                    'title' => 'Banner管理',
                ],
            'intro' =>
                [
                    'title' => '信用卡介紹',
                ],
            'member_area' =>
                [
                    'title' => '會員專區',
                    'promo' =>
                        [
                            'title' => '優惠活動',
                        ],
                    'store' =>
                        [
                            'title' => '特約商店',
                        ],
                ],
            'manual' =>
                [
                    'title' => '信用卡使用指南',
                ],
            'feedback' =>
                [
                    'title' => '信用卡理財專區',
                ],
            'helper' =>
                [
                    'title' => '線上小幫手',
                ],
        ],
    'travel' =>
        [
            'title' => '佳品旅遊館',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => 'Banner管理',
                ],
            'service' =>
                [
                    'title' => '旅遊超市管理',
                ],
            'expiring' =>
                [
                    'title' => '即期行程管理',
                    'add' =>
                        [
                            'title' => '新增即期行程',
                        ],
                    'edit' =>
                        [
                            'title' => '編輯即期行程',
                        ],
                ],
            'activity' =>
                [
                    'title' => '旅遊活動管理',
                    'manage' =>
                        [
                            'title' => '旅遊活動',
                            'add' =>
                                [
                                    'title' => '新增旅遊活動',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯旅遊活動',
                                ],
                            'content' =>
                                [
                                    'title' => '編輯活動內容',
                                ],
                        ],
                    'product' =>
                        [
                            'title' => '活動行程',
                            'add' =>
                                [
                                    'title' => '新增活動行程',
                                ],
                        ],
                ],
            'theme' =>
                [
                    'title' => '主題行程管理',
                    'category' =>
                        [
                            'title' => '主題類別',
                            'add' =>
                                [
                                    'title' => '新增主題類別',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯主題類別',
                                ],
                        ],
                    'product' =>
                        [
                            'title' => '主題行程',
                            'add' =>
                                [
                                    'title' => '新增主題行程',
                                ],
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦行程管理',
                    'category' =>
                        [
                            'title' => '區域類別',
                            'add' =>
                                [
                                    'title' => '新增區域類別',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯區域類別',
                                ],
                        ],
                    'product' =>
                        [
                            'title' => '推薦行程',
                            'add' =>
                                [
                                    'title' => '新增推薦行程',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯推薦行程',
                                ],
                        ],

                ],
            'viewpoint' =>
                [
                    'title' => '熱門景點管理',
                    'add' =>
                        [
                            'title' => '新增熱門景點',
                        ],
                    'edit' =>
                        [
                            'title' => '編輯熱門景點',
                        ],
                ],
        ],
    'travelticket' =>
        [
            'title' => '佳品旅遊館',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => 'Banner管理',
                ],
            'service' =>
                [
                    'title' => '旅遊超市管理',
                ],
            'expiring' =>
                [
                    'title' => '即期行程管理',
                    'add' =>
                        [
                            'title' => '新增即期行程',
                        ],
                    'edit' =>
                        [
                            'title' => '編輯即期行程',
                        ],
                ],
            'activity' =>
                [
                    'title' => '旅遊活動管理',
                    'manage' =>
                        [
                            'title' => '旅遊活動',
                            'add' =>
                                [
                                    'title' => '新增旅遊活動',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯旅遊活動',
                                ],
                            'content' =>
                                [
                                    'title' => '編輯活動內容',
                                ],
                        ],
                    'product' =>
                        [
                            'title' => '活動行程',
                            'add' =>
                                [
                                    'title' => '新增活動行程',
                                ],
                        ],
                ],
            'theme' =>
                [
                    'title' => '主題行程管理',
                    'category' =>
                        [
                            'title' => '主題類別',
                            'add' =>
                                [
                                    'title' => '新增主題類別',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯主題類別',
                                ],
                        ],
                    'product' =>
                        [
                            'title' => '主題行程',
                            'add' =>
                                [
                                    'title' => '新增主題行程',
                                ],
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦行程管理',
                    'category' =>
                        [
                            'title' => '區域類別',
                            'add' =>
                                [
                                    'title' => '新增區域類別',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯區域類別',
                                ],
                        ],
                    'product' =>
                        [
                            'title' => '推薦行程',
                            'add' =>
                                [
                                    'title' => '新增推薦行程',
                                ],
                            'edit' =>
                                [
                                    'title' => '編輯推薦行程',
                                ],
                        ],

                ],
            'viewpoint' =>
                [
                    'title' => '熱門景點管理',
                    'add' =>
                        [
                            'title' => '新增熱門景點',
                        ],
                    'edit' =>
                        [
                            'title' => '編輯熱門景點',
                        ],
                ],
        ],
    'museum' =>
        [
            'title' => '商城分館管理',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a01' =>
        [
            'title' => '商城分館A01',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a02' =>
        [
            'title' => '商城分館A02',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a03' =>
        [
            'title' => '商城分館A03',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a04' =>
        [
            'title' => '商城分館A04',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a05' =>
        [
            'title' => '商城分館A05',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a06' =>
        [
            'title' => '商城分館A06',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a07' =>
        [
            'title' => '商城分館A07',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a08' =>
        [
            'title' => '商城分館A08',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a09' =>
        [
            'title' => '商城分館A09',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a10' =>
        [
            'title' => '商城分館A10',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a11' =>
        [
            'title' => '商城分館A11',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
    'museum_a12' =>
        [
            'title' => '商城分館A12',
            'material' =>
                [
                    'title' => '素材圖片管理',
                ],
            'product' =>
                [
                    'title' => '商品庫',
                    'manage' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'banner' =>
                [
                    'title' => '滑動廣告管理',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'hot' =>
                [
                    'title' => '熱門商品專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'rank' =>
                [
                    'title' => '熱銷排行榜',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '全館熱銷排行',
                        ],
                ],
            'recommend' =>
                [
                    'title' => '推薦商品列表',
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                    'sub' =>
                        [
                            'title' => '推薦商品',
                        ],
                ],
            'popular' =>
                [
                    'title' => '熱門人氣專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'popular2' =>
                [
                    'title' => '熱門類別專區',
                    'category' =>
                        [
                            'title' => '類別設置',
                        ],
                    'product' =>
                        [
                            'title' => '商品管理',
                        ],
                ],
            'advertising' =>
                [
                    'title' => '廣告區塊管理',
                ],
            'advertising2' =>
                [
                    'title' => '綜合廣告區塊管理',
                    'banner1' =>
                        [
                            'title' => '左上靜態圖',
                        ],
                    'banner2' =>
                        [
                            'title' => '右上靜態圖',
                        ],
                    'banner3' =>
                        [
                            'title' => '下方滑動區塊',
                        ],
                    'banner4' =>
                        [
                            'title' => '全館熱銷圖',
                        ],
                ],
            'category_banner' =>
                [
                    'title' => '類別頁滑動廣告',
                    'common' =>
                        [
                            'title' => '通用廣告管理'
                        ],
                    'sub' =>
                        [
                            'title' => '廣告管理',
                        ],
                ],
        ],
];
