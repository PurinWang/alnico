<?php
return [ 
		"agree_err" => "請勾選我同意",
		'cancel' => '取消',
		"cropper_success" => "圖片裁切成功",
		"cropper_image_too_big" => "檔案過大，不得超過",
		"cropper_image" => "圖片裁切",
		"del" => [ 
				'title' => '刪除',
				'note' => '刪除後將無法復原' 
		],
		'logout' => [ 
				'title' => 'Log out?',
				'note' => 'Please assure you have completed what you are doing to prevent from losing any data.',
				'cancel' => 'Back',
				'ok' => 'ok',
				'success' => 'Signed off',
				'content' => 'Signed off'
		],
		"not_work" => "功能尚未開放",
		"notice" => "Notice",
		'ok' => 'ok',
		"register" => [ 
				"repassword_err" => "Confirm password and password do not match"
		],
		'upload' => [ 
				'image' => '上傳圖片',
				'image_preview' => '預覽圖片',
				'image_new' => '上傳新圖片',
				'image_crop' => '裁切' 
		] 
];
