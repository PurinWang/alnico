<?php
return [
    'exchange_rate' =>
        [
            'title' => '商城現行匯率',
            'country' => '國家',
            'coin_type' => '幣別',
            'exchange' => '匯率',
            'flag' => '幣別國旗',
        ],
    'fbar'  =>
        [
            /*
             * must be short
             */
            'lang' => '語言',
            'keep' => '收藏',
            'member' => '會員',
            'exchange' => '匯率',
            'share' => '分享',
        ],
    'header' =>
        [
            'search' => "搜尋",
            'about_us' => "商城使用指南",
            'wish' => '祈願神社',
            'coupon_gallery' => '特店優惠',
            'member_center' => "會員中心",
            'login' => "登入",
            'logout' => "登出",
            'register' => "註冊",
            'my_fly_money' => "我的飛幣",
            'app' => "手機APP",
            'home' => "商城首頁",
            'credit_card' => "EZ信用卡館",
            'games' => "數位娛樂館",
            'japan' => "日本完稅館",
            'travel' => "佳品旅遊館",
            'duty_free' => "馬祖免稅館",
            'specialty' => "台灣名物館",
            'logoTitle' => 'MARKET',
            'storeType' => '商城館別',
            'about' => '關於商城'
        ],
    'home_main' =>
        [
            'pageTitle' => '飛買家TraveltoBuy',
            'dataLoading' => '資料讀取中...',
            'description' => '飛買家TraveltoBuy 給你旅遊購物新體驗！出國前下訂人氣商品，回程機場取貨，免扛貨又省時間，出國前必逛!',
        ],
    'home' =>
        [
            'title' => '商城首頁',
            'index' =>
                [
                    'logoTitle' => 'MARKET',
                    'login' => "登入",
                    'register' => "註冊",
                    'forgot' => "忘記密碼",
                    'games' => "數位娛樂館",
                    'japan' => "日本完稅館",
                    'travel' => "旅遊淘寶館",
                    'duty_free' => "馬袓免稅館",
                    'specialty' => "台灣名物館",
                    'fly_money_notice' => "每筆消費皆可累積飛幣(Fly Money),不可兌現、僅使用於ezFly商城兌換相對應商品/折抵金額",
                    'tip1_1' => '中文服務',
                    'tip1_2' => '溝通無障礙',
                    'tip2_1' => '保證正貨',
                    'tip2_2' => '眾多熱銷商品',
                    'tip3_1' => '機場取貨',
                    'tip3_2' => '省時好便利',
                    'tip4_1' => '台幣付款',
                    'tip4_2' => '免海外手續費',
                    'title1' => '最新特店',
                    'title2' => '專屬優惠',
                ],
            'forgotpassword' =>
                [
                    'login' => "登入",
                    'register' => "註冊",
                    'forgot' => "忘記密碼",
                ],
        ],
    'japan' =>
        [
            'all_category' => '全部類別',
            'category' => '商品分類',
            'activity' => '優惠活動',
            'hot_sale' => '熱門商品',
            'credit_promo' => '刷卡優惠',
            'more' => '看更多',
            'price' => '原價',
            'promo_price' => '特價',
            'process' =>
                [
                    'title' => '訂購流程',
                    'tip1' => '選擇預購商品',
                    'tip2_1' => '日幣計價',
                    'tip2_2' => '台幣結帳',
                    'tip2_3' => '線上付款',
                    'tip3' => '日本機場取貨',
                ],
            'detail' =>
                [
                    'pay_tip1' => '* 商品價格以日幣顯示，結帳時依當日匯率自動換算。',
                    'pay_tip2' => '* 商品退換貨請見購物須知。',
                    'unable_to_buy' => '無法購買',
                    'direct_to_buy' => '直接購買',
                    'cart' => '購物車',
                    'share' => '分享',
                    'out_of_stock' => '無商品庫存',
                    'spec' => '規格',
                    'count' => '數量',
                    'calculate' => '小計',
                    'payment' => '付款方式',
                    'keep' => '收藏',
                    'product_notes' => '商品說明',
                    'price' => '原價',
                    'promo_price' => '特價',
                ]
        ],
    'duty_free' =>
        [
            'all_category' => '全部類別',
            'category' => '商品分類',
            'activity' => '優惠活動',
            'hot_sale' => '熱門商品',
            'credit_promo' => '刷卡優惠',
            'more' => '看更多',
            'price' => '原價',
            'promo_price' => '特價',
            'process' =>
                [
                    'title' => '訂購流程',
                    'tip1' => '選擇預購商品',
                    'tip2_1' => '日幣計價',
                    'tip2_2' => '台幣結帳',
                    'tip2_3' => '線上付款',
                    'tip3' => '北竿機場取貨',
                    'tip4' => '白沙碼頭取貨',
                    'tip5' => '南竿碼頭取貨',
                ],
            'detail' =>
                [
                    'pay_tip1' => '* 商品價格以日幣顯示，結帳時依當日匯率自動換算。',
                    'pay_tip2' => '* 商品退換貨請見購物須知。',
                    'unable_to_buy' => '無法購買',
                    'direct_to_buy' => '直接購買',
                    'cart' => '購物車',
                    'share' => '分享',
                    'out_of_stock' => '無商品庫存',
                    'spec' => '規格',
                    'count' => '數量',
                    'calculate' => '小計',
                    'payment' => '付款方式',
                    'keep' => '收藏',
                    'product_notes' => '商品說明',
                    'price' => '原價',
                    'promo_price' => '特價',
                ]
        ],
    'footer' =>
        [
            'title' => '關於商城',
            'about' => '關於EZ全球樂購網',
            'announcement' => '商城公告',
            'userGuide' => '商城使用指南',
            'alert' => '防詐騙須知',
            'policy' => '政策與規範',
            'FB' => 'FB粉絲專頁',
            'faqList' => '常見問題',
            'websiteMap' => '網站地圖',
            'contactUs' => '聯絡我們'
        ],
    'nav' =>
        [
            'language' => '語言',
            'member' => '會員',
            'exchange_rate' => '匯率',
            'share' => '分享',
        ],
    'search' =>
        [
            'note' => '請輸入關鍵字搜尋',
            'hot' => '熱門搜尋',
        ],
    'slidedown' =>
        [
            'searchNote' => '請輸入關鍵字搜尋',
            'searchHot' => '熱門搜尋',
        ],
    'member_center' =>
        [
            'title' => '會員中心',
            'menu' =>
                [
                    'mycard' => '我的會員卡',
                    'information' => '會員資訊',
                    'cart' => '購物車',
                    'order' => '訂單管理',
                    'success' => '已完成購物紀錄',
                    'keep' => '我的收藏',
                    'fly_money' => '我的飛幣',
                    'coupon' => '優惠券收藏夾',
                    'blog' => '部落客代碼查看',
                    'logout' => '登出',
                ]
        ]

];
