<?php
return [
    'title' => 'I want to join',
    'title_1' => 'I did',
    'title_2' => 'Upload the screenshot',
    'title_3' => 'I want',
    'title_4' => 'join',
    'note_1' => 'Note：You need to fill in all personal date in member center.',
    'note_2' => 'Note：Before uploading the screenshot, please log-in.',
    'note_3' => 'Note：After remitting, please must take a screenshot of the report as a proof to upload.',
];
