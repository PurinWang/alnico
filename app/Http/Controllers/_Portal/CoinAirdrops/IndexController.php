<?php

namespace App\Http\Controllers\_Portal\CoinAirdrops;

use App\Http\Controllers\_Portal\_PortalController;
use App\ModCoinAirdrops;
use App\ModCoinAirdropsUser;
use Illuminate\Http\Request;

class IndexController extends _PortalController
{
    public $module = [ 'coin_airdrops', 'index' ];

    /*
    *
    */
    public function doSend ( Request $request )
    {
        $CoinAirdropsTotal = ModCoinAirdropsUser::where( 'bDel', 0 )->sum( 'iGetBonus' );
        if ($CoinAirdropsTotal >= env( 'COIN_AIRDROP_LIMIT' )) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = "Coin Airdrop was closed";

            return response()->json( $this->rtndata );
        }

        $mission_id = $request->exists( 'iMissionId' ) ? $request->input( 'iMissionId' ) : 0;
        //Coin Airdrop
        $date_time = time();
        $mapCoinAirdrops['iStatus'] = 1;
        $mapCoinAirdrops['bDel'] = 0;
        $DaoCoinAirdrops = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->find( $mission_id );
        if ( !$DaoCoinAirdrops) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );

            return response()->json( $this->rtndata );
        }

        $map['iMissionId'] = $DaoCoinAirdrops->iId;
        $map['iMemberId'] = session( 'shop_member.iId' );
        $map['bDel'] = 0;
        $Dao = ModCoinAirdropsUser::where( $map )->first();
        if ($Dao) {
            $Dao->vValue = $request->exists( 'vValue' ) ? $request->input( 'vValue' ) : "";
            $Dao->iUpdateTime = time();
            $Dao->save();
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'edit', json_encode( $Dao ) );
        } else {
            $Dao = new ModCoinAirdropsUser();
            $Dao->iMissionId = $DaoCoinAirdrops->iId;
            $Dao->iMemberId = session( 'shop_member.iId' );
            $Dao->iGetBonus = $DaoCoinAirdrops->iBonus;
            $Dao->vValue = $request->exists( 'vValue' ) ? $request->input( 'vValue' ) : "";
            $Dao->iCreateTime = $Dao->iUpdateTime = time();
            $Dao->save();
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iId, 'add', json_encode( $Dao ) );
        }
        $this->rtndata ['status'] = 1;
        $this->rtndata ['message'] = trans( '_web_message.save_success' );

        return response()->json( $this->rtndata );
    }
}
