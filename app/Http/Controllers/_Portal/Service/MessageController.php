<?php

namespace App\Http\Controllers\_Portal\Service;

use App\Http\Controllers\_Portal\_PortalController;
use App\Http\Controllers\FuncController;
use App\ModServiceMessage;
use Illuminate\Http\Request;

class MessageController extends _PortalController
{
    public $module = [ 'service', 'message', 'index' ];

    /*
     *
     */
    public function index ()
    {
        $this->_init();
        //
        return $this->view;
    }

    public function doAdd ( Request $request )
    {
        $vSummary = $request->exists( 'vSummary' ) ? $request->input( 'vSummary' ) : "";
        if ($vSummary == "") {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );

            return response()->json( $this->rtndata );
        }

        $Dao = new ModServiceMessage();
        $Dao->vSessionId = session()->getId();
        $Dao->iMemberId = session()->has( 'shop_member' ) ? session()->get( 'shop_member.iId' ) : 0;
        $Dao->vServiceNum = $this->_getOrderNum( 'SC' );
        $Dao->iType = 1;//Message
        $Dao->iParentId = 0;
        $Dao->vTitle = "Service Message";
        $Dao->vSummary = $vSummary;
        $Dao->vName = $request->exists( 'vName' ) ? $request->input( 'vName' ) : "";
        $Dao->vGender = $request->exists( 'vGender' ) ? $request->input( 'vGender' ) : "";
        $Dao->vEmail = $request->exists( 'vEmail' ) ? $request->input( 'vEmail' ) : "";
        $Dao->vContact = $request->exists( 'vContact' ) ? $request->input( 'vContact' ) : "";
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        $Dao->iStatus = 0;
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }
}
