<?php

namespace App\Http\Controllers\_Portal;

use App\ModServiceMessage;
use Illuminate\Http\Request;

class FeedbackController extends _PortalController
{
    public $module = [ 'feedback', 'index' ];

    /*
     *
     */
    public function index ()
    {
        switch (session( 'locale' )) {
            case 'en':
                $this->module =  [ 'feedback', 'index_en' ];
                break;
            default:
                $this->module =  [ 'feedback', 'index' ];
        }

        $this->_init();

        //
        return $this->view;
    }

    public function doSendFeedback ( Request $request )
    {
        $vSummary = $request->exists( 'vSummary' ) ? $request->input( 'vSummary' ) : "";
        if ($vSummary == "") {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );

            return response()->json( $this->rtndata );
        }

        $Dao = new ModServiceMessage();
        $Dao->vSessionId = session()->getId();
        $Dao->iMemberId = session()->has( 'shop_member' ) ? session()->get( 'shop_member.iId' ) : 0;
        $Dao->vServiceNum = $this->_getOrderNum( 'SC' );
        $Dao->iType = 1;//Message
        $Dao->iParentId = 0;
        $Dao->vTitle = "Service Message";
        $Dao->vSummary = $vSummary;
        $Dao->vName = $request->exists( 'vName' ) ? $request->input( 'vName' ) : "";
        $Dao->vGender = $request->exists( 'vGender' ) ? $request->input( 'vGender' ) : "";
        $Dao->vEmail = $request->exists( 'vEmail' ) ? $request->input( 'vEmail' ) : "";
        $Dao->vContact = $request->exists( 'vContact' ) ? $request->input( 'vContact' ) : "";
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        $Dao->iStatus = 0;
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }
}
