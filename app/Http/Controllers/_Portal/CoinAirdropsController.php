<?php

namespace App\Http\Controllers\_Portal;


use App\ModCoinAirdrops;
use App\ModCoinAirdropsUser;

class CoinAirdropsController extends _PortalController
{
    public $module = [ 'coin_airdrops' ];

    /*
     *
     */
    public function index ()
    {
        switch (session( 'locale' )) {
            case 'en':
                $this->module = [ 'coin_airdrops_en' ];
                break;
            default:
                $this->module = [ 'coin_airdrops' ];
        }

        $this->_init();

        $CoinAirdropsTotal = ModCoinAirdropsUser::where( 'bDel', 0 )->sum( 'iGetBonus' );
        if ($CoinAirdropsTotal >= env( 'COIN_AIRDROP_LIMIT' )) {
            session()->put( 'ico.message', 'Coin Airdrop was closed' );

            return redirect()->guest( '' );
        }

        //Coin Airdrop
        $DaoCoinAirdrops = [];
        $date_time = time();
        $mapCoinAirdrops['iStatus'] = 1;
        $mapCoinAirdrops['bDel'] = 0;
        $DaoCoinAirdrops[1] = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->where( 'iType', 1 )->first();
        $DaoCoinAirdrops[2] = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->where( 'iType', 2 )->first();
        $DaoCoinAirdrops[3] = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->where( 'iType', 3 )->first();
        $this->view->with( 'coin_airdrops', $DaoCoinAirdrops );

        return $this->view;
    }

}
