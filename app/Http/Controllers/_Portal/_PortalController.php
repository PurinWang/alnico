<?php

namespace App\Http\Controllers\_Portal;

use App\Http\Controllers\Controller;
use App\LogAction;
use App\ModAnnouncement;
use App\ModCart;
use App\ModMallProduct;
use App\ModMuseum;
use App\SysCenterConfig;
use App\SysConfig;
use App\SysSetting;
use Jenssegers\Agent\Agent;

class _PortalController extends Controller
{
    protected $agent;
    protected $device;

    public function _init ()
    {
        $this->agent = new Agent();
        if ($this->agent->isMobile() && !$this->agent->isTablet()) {
            $this->view = View()->make( "_template_portal." . implode( '.', $this->module ) );
        } else {
            $this->view = View()->make( "_template_portal." . implode( '.', $this->module ) );
        }
        //WhitePaper
        $DaoWhitePaper = SysCenterConfig::where( 'iType', 601 )->get();
        $this->view->with( 'white_paper', $DaoWhitePaper );
        //LightPaper
        $DaoLightPaper = SysCenterConfig::where( 'iType', 604 )->get();
        $this->view->with( 'light_paper', $DaoLightPaper );
    }

    /*
     * $action : 1.add 2.edit 3.delete
     * $value : field->value
     */
    public function _saveLogAction ( $table_name, $table_id, $action, $value )
    {
        $DaoLogAction = new LogAction();
        $DaoLogAction->iMemberId = session( 'shop_member.iId' );
        $DaoLogAction->vTableName = $table_name;
        $DaoLogAction->iTableId = $table_id;
        $DaoLogAction->vAction = $action;
        $DaoLogAction->vValue = $value;
        $DaoLogAction->iDateTime = time();
        $DaoLogAction->save();
    }

    /*
     * $addChars : TTB 1494314960
     */
    public function _getOrderNum ( $addChars )
    {
        $order_num = substr( time(), -5 ) . sprintf( '%02d', rand( 0, 99 ) );
        if ($addChars && is_string( $addChars )) {
            $order_num = $addChars . $order_num;
        }

        return $order_num;
    }
//    public function _getOrderNum ( $addChars )
//    {
//        $order_num = ( Config()->get( 'config.str_arr' ) [intval( date( 'Y' ) ) - 2017] ) . strtoupper( dechex( date( 'm' ) ) ) . date( 'd' ) . substr( time(), -5 ) .
//            substr( microtime(), 2, 3 ) . sprintf( '%02d', rand( 0, 99 ) );
//        if ($addChars && is_string( $addChars )) {
//            $order_num = $addChars . $order_num;
//        }
//
//        return $order_num;
//    }
}
