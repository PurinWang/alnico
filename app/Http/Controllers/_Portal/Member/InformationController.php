<?php

namespace App\Http\Controllers\_Portal\Member;

use App\Http\Controllers\FuncController;
use App\ModBonus;
use App\ModCoinAirdrops;
use App\ModCoinAirdropsUser;
use App\ModInvest;
use App\ModServiceMessage;
use App\SysCenterMember;
use App\SysCenterMemberInfo;
use Illuminate\Http\Request;

class InformationController extends _MemberController
{
    public $module = [ 'member', 'index' ];

    /*
     *
     */
    public function index ()
    {
        switch (session( 'locale' )) {
            case 'en':
                $this->module = [ 'member', 'index_en' ];
                break;
            default:
                $this->module = [ 'member', 'index' ];
        }

        $this->_init();
        //
        FuncController::_checkOrderDisable();
        //
        $DaoInvest = ModInvest::where( 'iMemberId', session( 'member.iId' ) )->where( 'iStatus', '<>', 2 )->orderBy( 'iCreateTime', 'DESC' )->get();
        foreach ($DaoInvest as $item) {
            $mapBonus['vBonusNum'] = $item->vInvestNum;
            $mapBonus['bDel'] = 0;
            $bonus = ModBonus::where( $mapBonus )->value( 'iBonus' );
            $item->bonus = $bonus ? $bonus : 0;
        }
        $this->view->with( 'invest', $DaoInvest );

        //Coin Airdrop
        $DaoCoinAirdrops = [];
        $date_time = time();
        $mapCoinAirdrops['iStatus'] = 1;
        $mapCoinAirdrops['bDel'] = 0;
        $DaoCoinAirdrops[1] = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->where( 'iType', 1 )->first();
        if ($DaoCoinAirdrops[1]) {
            $mapUser['iMissionId'] = $DaoCoinAirdrops[1]->iId;
            $mapUser['iMemberId'] = session( 'shop_member.iId' );
            $mapUser['bDel'] = 0;
            $DaoCoinAirdrops[1]->vValue = ModCoinAirdropsUser::where( $mapUser )->value( 'vValue' );
            $DaoCoinAirdrops[1]->iGetBonus = ModCoinAirdropsUser::where( $mapUser )->value( 'iGetBonus' );
        }
        $DaoCoinAirdrops[2] = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->where( 'iType', 2 )->first();
        if ($DaoCoinAirdrops[2]) {
            $mapUser['iMissionId'] = $DaoCoinAirdrops[2]->iId;
            $mapUser['iMemberId'] = session( 'shop_member.iId' );
            $mapUser['bDel'] = 0;
            $DaoCoinAirdrops[2]->bonus = ModCoinAirdropsUser::where( $mapUser )->first();
        }
        $DaoCoinAirdrops[3] = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapCoinAirdrops )->where( 'iType', 3 )->first();
        if ($DaoCoinAirdrops[3]) {
            $mapUser['iMissionId'] = $DaoCoinAirdrops[3]->iId;
            $mapUser['iMemberId'] = session( 'shop_member.iId' );
            $mapUser['iStatus'] = 1;
            $mapUser['bDel'] = 0;
            $DaoCoinAirdrops[3]->iGetBonus = ModCoinAirdropsUser::where( $mapUser )->sum('iGetBonus');
            $DaoCoinAirdrops[3]->friend = ModCoinAirdropsUser::where( $mapUser )->count();
        }
        $this->view->with( 'coin_airdrops', $DaoCoinAirdrops );


        return $this->view;
    }

    /*
    *
    */
    public function doSave ( Request $request )
    {
        $id = ( session()->has( 'shop_member.iId' ) ) ? session( 'shop_member.iId' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = SysCenterMemberInfo::find( $id );
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        if ($request->exists( 'vUserName' )) {
            $Dao->vUserName = $request->input( 'vUserName' );
        }
        if ($request->exists( 'vUserNameE' )) {
            $Dao->vUserNameE = $request->input( 'vUserNameE' );
        }
        if ($request->exists( 'vUserTitle' )) {
            $Dao->vUserTitle = $request->input( 'vUserTitle' );
        }
        if ($request->exists( 'vUserID' )) {
            $Dao->vUserID = $request->input( 'vUserID' );
        }
        if ($request->exists( 'iUserBirthday' )) {
            $Dao->iUserBirthday = strtotime( $request->input( 'iUserBirthday' ) );
        }
        if ($request->exists( 'vUserEmail' )) {
            $Dao->vUserEmail = $request->input( 'vUserEmail' );
        }
        if ($request->exists( 'vUserContact' )) {
            $Dao->vUserContact = $request->input( 'vUserContact' );
        }
        if ($request->exists( 'vUserZipCode' )) {
            $Dao->vUserZipCode = $request->input( 'vUserZipCode' );
        }
        if ($request->exists( 'vUserCity' )) {
            $Dao->vUserCity = $request->input( 'vUserCity' );
        }
        if ($request->exists( 'vUserArea' )) {
            $Dao->vUserArea = $request->input( 'vUserArea' );
        }
        if ($request->exists( 'vUserAddress' )) {
            $Dao->vUserAddress = $request->input( 'vUserAddress' );
        }
        if ($request->exists( 'vCountryCode' )) {
            $Dao->vCountryCode = $request->input( 'vCountryCode' );
        }
        if ($request->exists( 'vCountry' )) {
            $Dao->vCountry = $request->input( 'vCountry' );
        }
        if ($request->exists( 'vERC' )) {
            $Dao->vERC = $request->input( 'vERC' );
        }
        if ($request->exists( 'vPassportF' )) {
            $Dao->vPassportF = $request->input( 'vPassportF' );
        }
        if ($request->exists( 'vPassportB' )) {
            $Dao->vPassportB = $request->input( 'vPassportB' );
        }
        if ($request->exists( 'vPassportT' )) {
            $Dao->vPassportT = $request->input( 'vPassportT' );
        }
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            $this->rtndata ['rtnurl'] = url( 'member' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iMemberId, 'edit', json_encode( $Dao ) );

            $DaoMember = SysCenterMember::find( $id );
            $DaoMember->iUpdateTime = time();
            $DaoMember->save();
            // Member
            session()->put( 'shop_member', json_decode( json_encode( $DaoMember ), true ) );
            // MemberInfo
            session()->put( 'shop_member.info', json_decode( json_encode( $Dao ), true ) );

        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }


    /*
     *
     */
    public function doSavePassword ( Request $request )
    {
        $vPassword = ( $request->exists( 'vPassword' ) ) ? $request->input( 'vPassword' ) : time();
        $vPasswordNew = ( $request->exists( 'vPasswordNew' ) ) ? $request->input( 'vPasswordNew' ) : time() . "new";

        $DaoMember = SysCenterMember::find( session( 'shop_member.iId' ) );
        if ( !$DaoMember) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.register.account_not_empty' );

            return response()->json( $this->rtndata );
        }
        if ($DaoMember->vPassword != hash( 'sha256', $DaoMember->vAgentCode . $vPassword . $DaoMember->vUserCode )) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_password' );

            return response()->json( $this->rtndata );
        }
        $DaoMember->vPassword = hash( 'sha256', $DaoMember->vAgentCode . $vPasswordNew . $DaoMember->vUserCode );
        $DaoMember->iUpdateTime = time();
        if ($DaoMember->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            $this->rtndata ['rtnurl'] = url( 'login' );
            //Logs
            $this->_saveLogAction( $DaoMember->getTable(), $DaoMember->iId, 'edit', json_encode( $DaoMember ) );
            session()->forget( 'shop_member' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
   *
   */
    public function doSaveInvest ( Request $request )
    {
        $id = $request->exists( 'vInvestNum' ) ? $request->input( 'vInvestNum' ) : 0;
        if ( !$id) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModInvest::where( 'iMemberId', session( 'member.iId' ) )->find( $id );
        if ( !$Dao) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.empty_id' );

            return response()->json( $this->rtndata );
        }
        if ($request->exists( 'vImages' )) {
            $Dao->vImages = $request->input( 'vImages' );
        }
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
            $this->rtndata ['rtnurl'] = url( 'member' );
            //Logs
            $this->_saveLogAction( $Dao->getTable(), $Dao->iMemberId, 'edit', json_encode( $Dao ) );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doAddMessage ( Request $request )
    {
        $vSummary = $request->exists( 'vSummary' ) ? $request->input( 'vSummary' ) : "";
        if ($vSummary == "") {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );

            return response()->json( $this->rtndata );
        }

        $Dao = new ModServiceMessage();
        $Dao->vSessionId = session()->getId();
        $Dao->iMemberId = session( 'shop_member.iId' );
        $Dao->vServiceNum = $this->_getOrderNum( 'SC' );
        $Dao->iType = 1;//Message
        $Dao->iParentId = 0;
        $Dao->vTitle = "Service Message";
        $Dao->vSummary = $vSummary;
        $Dao->vName = session( 'shop_member.info.vUserName' );
        $Dao->vGender = session( 'shop_member.info.vUserTitle' );
        $Dao->vEmail = ( session( 'shop_member.info.vUserEmail' ) ) ? session( 'shop_member.info.vUserEmail' ) : session( 'shop_member.vAccount' );
        $Dao->vContact = "";
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        $Dao->iStatus = 0;
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }

}
