<?php

namespace App\Http\Controllers\_Portal;

use App\Http\Controllers\CoinController;
use App\Http\Controllers\FuncController;
use App\Http\Controllers\CoinAirdropsController;
use App\SysCenterGroupMember;
use App\SysCenterMember;
use App\SysCenterMemberInfo;
use App\SysCenterMemberVerification;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class LoginController extends _PortalController
{
    public $module = [ 'login' ];

    /*
     *
     */
    public function index ()
    {
        $this->_init();

        return $this->view;
    }

    /*
     *
     */
    public function forgotpassword ()
    {
        $this->module = [ 'forgotpassword' ];
        $this->_init();

        return $this->view;
    }

    /*
     *
     */
    public function doLogin ( Request $request )
    {
        $vAccount = ( $request->exists( 'vAccount' ) ) ? $request->input( 'vAccount' ) : "";
        $vPassword = ( $request->exists( 'vPassword' ) ) ? $request->input( 'vPassword' ) : time();
        if ( !FuncController::_isValidEmail( $vAccount )) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.register.error_account' );

            return response()->json( $this->rtndata );
        }
        $mapMember ['vAccount'] = $vAccount;
        $DaoMember = SysCenterMember::where( $mapMember )->first();
        if ( !$DaoMember) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_account' );

            return response()->json( $this->rtndata );
        }
        if ($DaoMember->vPassword != hash( 'sha256', $DaoMember->vAgentCode . $vPassword . $DaoMember->vUserCode )) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_password' );

            return response()->json( $this->rtndata );
        }
        //帳號是否有啟用
        if ( !$DaoMember->bActive) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_active' );

            return response()->json( $this->rtndata );
        }
        //帳號狀態是否正常
        if ($DaoMember->iStatus == 0) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_status' );

            return response()->json( $this->rtndata );
        }
        // Member
        session()->put( 'member', json_decode( json_encode( $DaoMember ), true ) );
        session()->put( 'shop_member', json_decode( json_encode( $DaoMember ), true ) );
        // MemberInfo
        $DaoMemberInfo = SysCenterMemberInfo::find( $DaoMember->iId );
        session()->put( 'member.info', json_decode( json_encode( $DaoMemberInfo ), true ) );
        session()->put( 'shop_member.info', json_decode( json_encode( $DaoMemberInfo ), true ) );

        //
        FuncController::_addLog( 'login' );
        //
        CoinAirdropsController::doCheck( 1 );
        //
        CoinAirdropsController::doCheck( 3, $vAccount );

        $this->rtndata ['status'] = 1;
        $this->rtndata ['message'] = trans( '_web_message.login.success' );
        $this->rtndata ['rtnurl'] = ( session()->has( 'rtnurl' ) ) ? session()->pull( 'rtnurl' ) : url( '' );

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doLoginOther ( Request $request )
    {
        $vAccount = ( $request->exists( 'vAccount' ) ) ? $request->input( 'vAccount' ) : "";
        $vPassword = ( $request->exists( 'vPassword' ) ) ? $request->input( 'vPassword' ) : "";;
        $type = ( $request->exists( 'type' ) ) ? $request->input( 'type' ) : "other";
        if ($vAccount == "") {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_account' );

            return response()->json( $this->rtndata );
        }
        $mapMember ['vAccount'] = $vAccount;
        $DaoMember = SysCenterMember::where( $mapMember )->first();
        if ( !$DaoMember) {
            $str = md5( uniqid( mt_rand(), true ) );
            $uuid = substr( $str, 0, 8 ) . '-';
            $uuid .= substr( $str, 8, 4 ) . '-';
            $uuid .= substr( $str, 12, 4 ) . '-';
            $uuid .= substr( $str, 16, 4 ) . '-';
            $uuid .= substr( $str, 20, 12 );
            do {
                $userid = rand( 1000000001, 1099999999 );
                $check = SysCenterMember::where( "iUserId", $userid )->first();
            } while ($check);
            $date_time = time();
            $DaoMember = new SysCenterMember ();
            $DaoMember->iUserId = $userid;
            $DaoMember->vUserCode = $uuid;
            $DaoMember->vAgentCode = config( '_config.agent_code' ) . "-" . $type;
            $DaoMember->iAcType = 999; //
            $DaoMember->vAccount = $vAccount;
            $DaoMember->vPassword = hash( 'sha256', $DaoMember->vAgentCode . $vPassword . $DaoMember->vUserCode );
            $DaoMember->iCreateTime = $DaoMember->iUpdateTime = $date_time;
            $DaoMember->vCreateIP = $request->ip();
            $DaoMember->bActive = 1;
            $DaoMember->iStatus = 1;
            if ($DaoMember->save()) {
                $DaoMemberInfo = new SysCenterMemberInfo();
                $DaoMemberInfo->iMemberId = $DaoMember->iId;
                $DaoMemberInfo->vUserImage = "/images/empty.jpg";
                $DaoMemberInfo->vUserName = ( $request->exists( 'vUserName' ) ) ? $request->input( 'vUserName' ) : $vAccount;
                $DaoMemberInfo->vUserID = ( $request->exists( 'vUserID' ) ) ? $request->input( 'vUserID' ) : "";
                $DaoMemberInfo->iUserBirthday = time();
                $DaoMemberInfo->vUserContact = ( $request->exists( 'vUserContact' ) ) ? $request->input( 'vUserContact' ) : "";
                $DaoMemberInfo->vUserEmail = "";
                $DaoMemberInfo->save();
                $this->rtndata ['status'] = 1;
                $this->rtndata ['message'] = trans( '_web_message.register.success' );
                $this->rtndata ['rtnurl'] = ( session()->has( 'rtnurl' ) ) ? session()->pull( 'rtnurl' ) : url( 'login' );

                $DaoGroupMember = new SysCenterGroupMember();
                $DaoGroupMember->iGroupId = 5;
                $DaoGroupMember->iMemberId = $DaoMember->iId;
                $DaoGroupMember->iCreateTime = $DaoGroupMember->iUpdateTime = time();
                $DaoGroupMember->iStatus = 1;
                $DaoGroupMember->save();
                //
                CoinController::_CheckActivityRegister( $DaoMember->iId );

            } else {
                $this->rtndata ['status'] = 0;
                $this->rtndata ['message'] = trans( '_web_message.register.fail' );

                return response()->json( $this->rtndata );
            }
        } else {
            switch ($type) {
                case 'FB':
                    $access_token = $request->input( 'accessToken' );
                    $fb = new Facebook( [
                        'app_id' => config( '_config.fb_appid' ),
                        'app_secret' => config( '_config.fb_secret' ),
                        'default_graph_version' => config( '_config.fb_ver' ),
                    ] );
                    try {
                        $response = $fb->get( '/me', $access_token );
                    } catch (FacebookResponseException $e) {
                        // When Graph returns an error
                        $this->rtndata ['status'] = 0;
                        $this->rtndata ['message'] = trans( '_web_message.login.error_account' );

                        return response()->json( $this->rtndata );
                    } catch (FacebookSDKException $e) {
                        // When validation fails or other local issues
                        $this->rtndata ['status'] = 0;
                        $this->rtndata ['message'] = trans( '_web_message.login.error_account' );

                        return response()->json( $this->rtndata );
                    }
                    //$me = $response->getGraphUser();
                    break;
                case 'GPLUS':
                    break;
                default:
            }
        }
        //帳號是否有啟用
        if ( !$DaoMember->bActive) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_active' );

            return response()->json( $this->rtndata );
        }
        //帳號狀態是否正常
        if ($DaoMember->iStatus == 0) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_status' );

            return response()->json( $this->rtndata );
        }
        // Member
        session()->put( 'shop_member', json_decode( json_encode( $DaoMember ), true ) );
        // MemberInfo
        $DaoMemberInfo = SysCenterMemberInfo::find( $DaoMember->iId );
        session()->put( 'shop_member.info', json_decode( json_encode( $DaoMemberInfo ), true ) );

        //Activity
        CoinController::_CheckActivityLogin();

        $this->rtndata ['status'] = 1;
        $this->rtndata ['message'] = trans( '_web_message.login.success' );
        $this->rtndata ['rtnurl'] = ( session()->has( 'rtnurl' ) ) ? session()->pull( 'rtnurl' ) : url( 'member_center/information' );

        return response()->json( $this->rtndata );
    }

    /*
	 *
	 */
    public function doSendVerification ( Request $request )
    {
        $vAccount = ( $request->exists( 'vAccount' ) ) ? $request->input( 'vAccount' ) : "";
        if ($vAccount == "") {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_account' );

            return response()->json( $this->rtndata );
        }
        $mapMember ['vAccount'] = $vAccount;
        $DaoMember = SysCenterMember::where( $mapMember )->first();
        if ( !$DaoMember) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.login.error_account' );

            return response()->json( $this->rtndata );
        }
        $email = $vAccount;
        $verification = "";
        for ($i = 0 ; $i < config( '_config.verification.limit' ) ; $i++) {
            if (rand( 0, 1 )) {
                $verification .= rand( 1, 9 );
            } else {
                $str_arr = config( '_parameter.str_arr' );
                $verification .= $str_arr [rand( 0, count( $str_arr ) - 1 )];
            }
        }

        $DaoMemberVerification = SysCenterMemberVerification::find( $DaoMember->iId );
        if ( !$DaoMemberVerification) {
            $DaoMemberVerification = new SysCenterMemberVerification ();
            $DaoMemberVerification->iMemberId = $DaoMember->iId;
            $DaoMemberVerification->vVerification = $verification;
            $DaoMemberVerification->iStartTime = time();
            $DaoMemberVerification->iEndTime = $DaoMemberVerification->iStartTime + config( '_config.verification.time' );
            $DaoMemberVerification->iStatus = 0;
        } else {
            $DaoMemberVerification->vVerification = $verification;
            $DaoMemberVerification->iStartTime = time();
            $DaoMemberVerification->iEndTime = $DaoMemberVerification->iStartTime + config( '_config.verification.time' );
            $DaoMemberVerification->iStatus = 0;
        }

        if ($DaoMemberVerification->save()) {
            //
            Mail::send( '_template_email.forgot', [ 'verification' => $verification ], function( $message ) use ( $email ) {
                $message->to( $email )->subject( trans( '_web_message.verification.forgot_password' ) );
            } );
            session()->put( 'verification.memberid', $DaoMember->iId );
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.verification.success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.verification.dail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
      *
      */
    public function doResetPassword ( Request $request )
    {
        $vVerification = ( $request->exists( 'vVerification' ) ) ? $request->input( 'vVerification' ) : "";
        $vPassword = ( $request->exists( 'vPassword' ) ) ? $request->input( 'vPassword' ) : "";

        $mapMemberVerification['vVerification'] = $vVerification;
        $mapMemberVerification['iStatus'] = 0;
        $DaoMemberVerification = SysCenterMemberVerification::where( $mapMemberVerification )->find( session( 'verification.memberid' ) );
        if ( !$DaoMemberVerification) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.verification.error' );

            return response()->json( $this->rtndata );
        }

        $DaoMember = SysCenterMember::find( $DaoMemberVerification->iMemberId );
        if ( !$DaoMember) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.verification.error' );

            return response()->json( $this->rtndata );
        }
        $DaoMember->iUpdateTime = time();
        $DaoMember->vPassword = hash( 'sha256', $DaoMember->vAgentCode . $vPassword . $DaoMember->vUserCode );
        if ($DaoMember->save()) {
            $DaoMemberVerification->iStatus = 1;
            $DaoMemberVerification->save();
            session()->flush();
            $this->rtndata ['status'] = 1;
            $this->rtndata ['rtnurl'] = url( 'login' );
            $this->rtndata ['message'] = trans( '_web_message.save_success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.save_fail' );
        }

        return response()->json( $this->rtndata );
    }
}
