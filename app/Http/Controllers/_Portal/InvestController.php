<?php

namespace App\Http\Controllers\_Portal;

use App\ModActivity;
use App\ModBonus;
use App\ModInvest;
use App\SysCenterMemberSign;
use App\SysExchangeRate;
use Illuminate\Http\Request;

class InvestController extends _PortalController
{
    public $module = [ 'invest' ];

    /*
     *
     */
    public function index ()
    {
        $this->_init();

        return $this->view;
    }

    /*
   *
   */
    public function create ()
    {
        $this->module = [ 'invest_create' ];
        $this->_init();

        $ETH = SysExchangeRate::where( 'vRateName', 'ETH' )->value( 'fRateValue' );
        $this->view->with( 'ETH_Rate', $ETH );

        $BTC = SysExchangeRate::where( 'vRateName', 'BTC' )->value( 'fRateValue' );
        $this->view->with( 'BTC_Rate', $BTC );

        //Bonus
        $date_time = time();
        $mapActivity['iStatus'] = 1;
        $mapActivity['bDel'] = 0;
        $bonus = ModActivity::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapActivity )->value( 'iBonus' );
        $this->view->with( 'bonus', $bonus );

        return $this->view;
    }

    /*
     *
     */
    public function doInvest ( Request $request )
    {
        $type = $request->exists( 'type' ) ? $request->input( 'type' ) : 'ETH';
        $invest_value = $request->exists( 'invest_value' ) ? $request->input( 'invest_value' ) : 0;

        $fRateValue = 10;
        $total = 0;
        switch ($type) {
            case 'ETH':
                $fRateValue = SysExchangeRate::where( 'vRateName', 'ETH' )->value( 'fRateValue' );
                $total = $fRateValue * $invest_value * 10;
                $vOrderNum = $this->_getOrderNum( "AE" );
                break;
            case 'BTC':
                $fRateValue = SysExchangeRate::where( 'vRateName', 'BTC' )->value( 'fRateValue' );
                $total = $fRateValue * $invest_value * 10;
                $vOrderNum = $this->_getOrderNum( "AB" );
                break;
            case 'USD':
                $fRateValue = 10;
                $total = $invest_value * 10;
                $vOrderNum = $this->_getOrderNum( "AU" );
                break;
            default:
                $vOrderNum = $this->_getOrderNum( "AC" );
        }


        //開始寫訂單資料
        $Dao = new ModInvest();
        $Dao->iMemberId = session()->get( 'shop_member.iId' );
        $Dao->vInvestNum = $vOrderNum;
        $Dao->vType = $type;
        $Dao->iCount = $invest_value;
        $Dao->fExchangeRate = $fRateValue;
        $Dao->iTotal = $total;
        $Dao->iPayStatus = 0;
        $Dao->iStatus = 0;
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            //
            $DaoSign = new SysCenterMemberSign();
            $DaoSign->iMemberId = session()->get( 'shop_member.iId' );
            $DaoSign->vSessionId = session()->getId();
            $DaoSign->vUserName = session()->get( 'shop_member.info.vUserName' );
            $DaoSign->iSignId = 201;
            $DaoSign->vSignContent = $vOrderNum . ":风险说明與发行方案中的规范";
            $DaoSign->iCreateTime = time();
            $DaoSign->vIP = $request->ip();
            $DaoSign->save();
            //
            $DaoSign = new SysCenterMemberSign();
            $DaoSign->iMemberId = session()->get( 'shop_member.iId' );
            $DaoSign->vSessionId = session()->getId();
            $DaoSign->vUserName = session()->get( 'shop_member.info.vUserName' );
            $DaoSign->iSignId = 202;
            $DaoSign->vSignContent = $vOrderNum . ":隐私权条款與服务条款中的规范";
            $DaoSign->iCreateTime = time();
            $DaoSign->vIP = $request->ip();
            $DaoSign->save();
            //
            $DaoSign = new SysCenterMemberSign();
            $DaoSign->iMemberId = session()->get( 'shop_member.iId' );
            $DaoSign->vSessionId = session()->getId();
            $DaoSign->vUserName = session()->get( 'shop_member.info.vUserName' );
            $DaoSign->iSignId = 203;
            $DaoSign->vSignContent = $vOrderNum . ":我已下载并阅读ALLN-Token白皮书";
            $DaoSign->iCreateTime = time();
            $DaoSign->vIP = $request->ip();
            $DaoSign->save();
            //Bonus
            $date_time = time();
            $mapActivity['iStatus'] = 1;
            $mapActivity['bDel'] = 0;
            $DaoActivity = ModActivity::where( function( $query ) use ( $date_time ) {
                $query->where( 'iStartTime', '<', $date_time );
                $query->where( 'iEndTime', '>', $date_time );
            } )->where( $mapActivity )->first();
            if ($DaoActivity) {
                $DaoBonus = new ModBonus();
                $DaoBonus->iMemberId = session( 'shop_member.iId' );
                $DaoBonus->vBonusNum = $vOrderNum;
                $DaoBonus->vType = $DaoActivity->iBonus;
                $DaoBonus->iBonus = ( $total * $DaoActivity->iBonus ) / 100;
                $DaoBonus->iCreateTime = $DaoBonus->iUpdateTime = time();
                $DaoBonus->save();
            }
            $this->rtndata ['invest_num'] = $vOrderNum;
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail8' );
        }

        return response()->json( $this->rtndata );
    }

    /*
  *
  */
    public function invest_pay ( $invest_num )
    {
        $this->module = [ 'invest_pay' ];
        $this->_init();
        //
        $Dao = ModInvest::where( 'vInvestNum', $invest_num )->first();
        if ( !$Dao) {
            return redirect( '' );
        }
        $this->view->with( 'info', $Dao );

        return $this->view;
    }
}
