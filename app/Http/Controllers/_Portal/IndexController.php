<?php

namespace App\Http\Controllers\_Portal;

use App\ModActivity;
use App\ModEmail;
use App\ModNews;
use App\SysCenterConfig;
use Illuminate\Http\Request;

class IndexController extends _PortalController
{
    public $module = [ 'index' ];

    /*
     *
     */
    public function index ()
    {
        switch (session( 'locale' )) {
            case 'en':
                $this->module = [ 'index_en' ];
                break;
            default:
                $this->module = [ 'index' ];
        }

        $this->_init();
        //Bonus
        $date_time = time();
        $mapActivity['iStatus'] = 1;
        $mapActivity['bDel'] = 0;
        $DaoActivity = ModActivity::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapActivity )->first();
        if ($DaoActivity) {
            $activity['bonus'] = $DaoActivity->iBonus;
            $activity['end_time'] = $DaoActivity->iEndTime;
        } else {
            $activity['bonus'] = 0;
            $activity['end_time'] = time();
        }
        $this->view->with( 'activity', $DaoActivity );
        $this->view->with( 'current_activity', $activity );

        $mapActivity['iStatus'] = 1;
        $mapActivity['bDel'] = 0;
        $start_time = ModActivity::where( $mapActivity )->orderBy( 'iStartTime', 'ASC' )->pluck( 'iStartTime' );
        foreach ($start_time as $key => $item) {
            $start_time[$key] = date( 'm/d', $item );
        }
        $end_time = ModActivity::where( $mapActivity )->orderBy( 'iEndTime', 'ASC' )->pluck( 'iEndTime' );
        foreach ($end_time as $key => $item) {
            $end_time[$key] = date( 'm/d', $item );
        }
        $bonus = ModActivity::where( $mapActivity )->orderBy( 'iStartTime', 'ASC' )->pluck( 'iBonus' );
        $this->view->with( 'start_time', $start_time );
        $this->view->with( 'end_time', $end_time );
        $this->view->with( 'bonus', $bonus );

        //
        $DaoProcess = SysCenterConfig::where( 'iId', 22 )->value( 'vValue' );
        $this->view->with( 'process', $DaoProcess );
        //News
        $date_time = time();
        $mapNews['iStatus'] = 1;
        $DaoNews = ModNews::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $mapNews )->orderBy( 'iRank', 'ASC' )->get();
        foreach ($DaoNews as $item) {
            $item->url = ( $item->vUrl ) ? $item->vUrl : url( 'news/detail/' . $item->iId );
        }
        $this->view->with( 'news', $DaoNews );

        return $this->view;
    }

    /*
     *
     */
    public function doAddEmail ( Request $request )
    {
        $vEmail = $request->exists( 'vEmail' ) ? $request->input( 'vEmail' ) : "";
        if ($vEmail == "") {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );

            return response()->json( $this->rtndata );
        }
        $Dao = ModEmail::where('vEmail',$vEmail)->first();
        if($Dao){
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = "Email is already exists!";

            return response()->json( $this->rtndata );
        }

        $Dao = new ModEmail();
        $Dao->vSessionId = session()->getId();
        $Dao->vEmail = $vEmail;
        $Dao->iCreateTime = $Dao->iUpdateTime = time();
        if ($Dao->save()) {
            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.add_success' );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.add_fail' );
        }

        return response()->json( $this->rtndata );
    }
}
