<?php

namespace App\Http\Controllers\_Portal;

class ReportController extends _PortalController
{
    public $module = [ 'report' ];

    /*
     *
     */
    public function index ()
    {
        $this->_init();

        return $this->view;
    }
}
