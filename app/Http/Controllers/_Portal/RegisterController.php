<?php

namespace App\Http\Controllers\_Portal;

use App\Http\Controllers\CoinAirdropsController;
use App\Http\Controllers\CoinController;
use App\Http\Controllers\FuncController;
use App\SysCenterGroupMember;
use App\SysCenterMember;
use App\SysCenterMemberInfo;
use App\SysCenterMemberSign;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends _PortalController
{
    public $module = [ 'register' ];

    /*
     *
     */
    public function index ()
    {
        $this->_init();

        return $this->view;
    }

    /*
     *
     */
    public function doRegister ( Request $request )
    {
        $vAccount = ( $request->exists( 'vAccount' ) ) ? $request->input( 'vAccount' ) : "";
        $vPassword = ( $request->exists( 'vPassword' ) ) ? $request->input( 'vPassword' ) : "";
        if ( !FuncController::_isValidEmail( $vAccount )) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.register.error_account' );

            return response()->json( $this->rtndata );
        }
        $map ['vAccount'] = $vAccount;
        $DaoMember = SysCenterMember::where( $map )->first();
        if ($DaoMember) {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.register.account_not_empty' );

            return response()->json( $this->rtndata );
        }
        $str = md5( uniqid( mt_rand(), true ) );
        $uuid = substr( $str, 0, 8 ) . '-';
        $uuid .= substr( $str, 8, 4 ) . '-';
        $uuid .= substr( $str, 12, 4 ) . '-';
        $uuid .= substr( $str, 16, 4 ) . '-';
        $uuid .= substr( $str, 20, 12 );
        do {
            $userid = rand( 1000000001, 1099999999 );
            $check = SysCenterMember::where( "iUserId", $userid )->first();
        } while ($check);
        $date_time = time();
        $DaoMember = new SysCenterMember ();
        $DaoMember->iUserId = $userid;
        $DaoMember->vUserCode = $uuid;
        $DaoMember->vAgentCode = config( '_config.agent_code' );
        $DaoMember->iAcType = 999; //
        $DaoMember->vAccount = $vAccount;
        $DaoMember->vPassword = hash( 'sha256', $DaoMember->vAgentCode . $vPassword . $DaoMember->vUserCode );
        $DaoMember->iCreateTime = $DaoMember->iUpdateTime = $date_time;
        $DaoMember->vCreateIP = ( FuncController::getIp() ) ? FuncController::getIp() : "";
        $DaoMember->bActive = 0;
        $DaoMember->iStatus = 1;
        if ($DaoMember->save()) {
            $DaoMemberInfo = new SysCenterMemberInfo();
            $DaoMemberInfo->iMemberId = $DaoMember->iId;
            $DaoMemberInfo->vUserImage = "/images/empty.jpg";
            $DaoMemberInfo->vUserName = ( $request->exists( 'vUserName' ) ) ? $request->input( 'vUserName' ) : $vAccount;
            $DaoMemberInfo->vUserID = ( $request->exists( 'vUserID' ) ) ? $request->input( 'vUserID' ) : "";
            $DaoMemberInfo->iUserBirthday = time();
            $DaoMemberInfo->vUserContact = ( $request->exists( 'vUserContact' ) ) ? $request->input( 'vUserContact' ) : "";
            $DaoMemberInfo->vUserEmail = $vAccount;
            $DaoMemberInfo->vCountry = ( $request->exists( 'vCountry' ) ) ? $request->input( 'vCountry' ) : "";;
            $DaoMemberInfo->save();

            $DaoGroupMember = new SysCenterGroupMember();
            $DaoGroupMember->iGroupId = 5;
            $DaoGroupMember->iMemberId = $DaoMember->iId;
            $DaoGroupMember->iCreateTime = $DaoGroupMember->iUpdateTime = time();
            $DaoGroupMember->iStatus = 1;
            $DaoGroupMember->save();
            //
            $DaoSign = new SysCenterMemberSign();
            $DaoSign->iMemberId = $DaoMember->iId;
            $DaoSign->vSessionId = session()->getId();
            $DaoSign->vUserName = $DaoMemberInfo->vUserName;
            $DaoSign->iSignId = 101;
            $DaoSign->vSignContent = "风险说明與发行方案中的规范";
            $DaoSign->iCreateTime = time();
            $DaoSign->vIP = $request->ip();
            $DaoSign->save();
            //
            $DaoSign = new SysCenterMemberSign();
            $DaoSign->iMemberId = $DaoMember->iId;
            $DaoSign->vSessionId = session()->getId();
            $DaoSign->vUserName = $DaoMemberInfo->vUserName;
            $DaoSign->iSignId = 102;
            $DaoSign->vSignContent = "隐私权条款與服务条款中的规范";
            $DaoSign->iCreateTime = time();
            $DaoSign->vIP = $request->ip();
            $DaoSign->save();

            $this->rtndata ['status'] = 1;
            $this->rtndata ['message'] = trans( '_web_message.register.success' );
            $this->rtndata ['rtnurl'] = ( session()->has( 'rtnurl' ) ) ? session()->pull( 'rtnurl' ) : url( 'login' );
            //
            Mail::send( '_template_email.welcome', [ 'url' => url( 'doActive' ) . '/' . $uuid ], function( $message ) use ( $vAccount ) {
                $message->to( $vAccount, '會員' )->subject( '[ALLN] Please complete verifying your email for your ALLN-Token account.' );
            } );
            //
            CoinAirdropsController::doCheck( 3, $vAccount );
        } else {
            $this->rtndata ['status'] = 0;
            $this->rtndata ['message'] = trans( '_web_message.register.fail' );
        }

        return response()->json( $this->rtndata );
    }

    /*
     *
     */
    public function doActive ( $usercode )
    {
        $map['vUserCode'] = $usercode;
        $Dao = SysCenterMember::where( $map )->first();
        if ( !$Dao) {
            return View()->make( "errors.empty" );
        }
        if ($Dao->bActive) {
            return View()->make( "errors.active" );
        }
        $Dao->bActive = 1;
        $Dao->iUpdateTime = time();
        $Dao->save();

        return redirect( 'login' );
    }
}
