<?php

namespace App\Http\Controllers\_Portal;

use App\Http\Controllers\Controller;
use App\Http\Controllers\FuncController;

class LogoutController extends Controller
{
    /*
     *
     */
    public function __construct ()
    {
    }

    /*
     *
     */
    public function index ()
    {
        //session()->flush();
        session()->forget( 'member' );
        session()->forget( 'shop_member' );

        return redirect()->guest( '' );
    }

    /*
     *
     */
    public function doLogout ()
    {
        //
        FuncController::_addLog( 'logout' );
        //session()->flush();
        session()->forget( 'member' );
        session()->forget( 'shop_member' );
        $this->rtndata ['status'] = 1;
        $this->rtndata ['message'] = trans( '_web_message.logout.content' );

        return response()->json( $this->rtndata );
    }
}
