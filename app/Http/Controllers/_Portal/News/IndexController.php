<?php

namespace App\Http\Controllers\_Portal\News;

use App\Http\Controllers\_Portal\_PortalController;
use App\ModNews;
use App\SysCenterCategory;

class IndexController extends _PortalController
{
    public $module = [ 'news', 'index' ];

    /*
     *
     */
    public function index ()
    {
        $this->_init();
        //
        $map['iStatus'] = 1;
        $map['bDel'] = 0;
        $category_arr = ModNews::where( $map )->pluck( 'iCategoryType' );
        $DaoCategory = SysCenterCategory::whereIn( 'iId', $category_arr )->get();
        $this->view->with( 'category', $DaoCategory );

        return $this->view;
    }

    /*
    *
    */
    public function detail ( $id )
    {
        $this->module = [ 'news', 'detail' ];
        $this->_init();
        //
        $mapNews['iStatus'] = 1;
        $mapNews['bDel'] = 0;
        $DaoNews = ModNews::where( $mapNews )->get();
        foreach ($DaoNews as $key => $item) {
            if ($item->iId == $id) {
                $pre = isset( $DaoNews[$key - 1] ) ? $DaoNews[$key - 1] : "";
                $current = $item;
                $next = isset( $DaoNews[$key + 1] ) ? $DaoNews[$key + 1] : "";
                break;
            }
        }
        $this->view->with( 'pre', $pre );
        $this->view->with( 'info', $current );
        $this->view->with( 'next', $next );
        //
        $breadcrumb = [
            '商城首頁' => url( '' ),
            '公告訊息' => url( 'news' ),
            $current->vTitle => 'active'
        ];
        $this->view->with( 'breadcrumb', $breadcrumb );

        return $this->view;
    }
}
