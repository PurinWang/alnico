<?php

namespace App\Http\Controllers;

use App\ModCoinAirdrops;
use App\ModCoinAirdropsUser;
use App\SysCenterMember;

class CoinAirdropsController
{
    /*
    * $type: 1.新註冊/登入 2.加入電報 3.邀請好友
    */
    static function doCheck ( $type, $vAccount = 'admin@pin2wall.com' )
    {

        $CoinAirdropsTotal = ModCoinAirdropsUser::where( 'bDel', 0 )->sum( 'iGetBonus' );
        if ($CoinAirdropsTotal > env( 'COIN_AIRDROP_LIMIT' )) {
            return false;
        }
        $date_time = time();
        $map['iType'] = $type;
        $map['iStatus'] = 1;
        $map['bDel'] = 0;
        $Dao = ModCoinAirdrops::where( function( $query ) use ( $date_time ) {
            $query->where( 'iStartTime', '<', $date_time );
            $query->where( 'iEndTime', '>', $date_time );
        } )->where( $map )->first();

        if ($Dao) {
            switch ($Dao->iType) {
                case 1:
                    $mapUser['iMissionId'] = $Dao->iId;
                    $mapUser['iMemberId'] = session( 'member.iId' );
                    $DaoUser = ModCoinAirdropsUser::where( $mapUser )->first();
                    if ( !$DaoUser) {
                        $DaoUser = new ModCoinAirdropsUser();
                        $DaoUser->iMissionId = $Dao->iId;
                        $DaoUser->iMemberId = session( 'member.iId' );
                        $DaoUser->iGetBonus = $Dao->iBonus;
                        $DaoUser->vValue = $Dao->vTitle;
                        $DaoUser->iCreateTime = $DaoUser->iUpdateTime = time();
                        $DaoUser->iStatus = 0;
                        $DaoUser->save();
                    }
                    break;
                case 2:
                    break;
                case 3:
                    $DaoMember = SysCenterMember::where( 'iUserId', session( 'member.recommend_user_id' ) )->first();
                    if ($DaoMember) {
                        $mapUser['iMissionId'] = $Dao->iId;
                        $mapUser['vValue'] = $vAccount;
                        $DaoUser = ModCoinAirdropsUser::where( $mapUser )->first();
                        if ( !$DaoUser) {
                            $DaoUser = new ModCoinAirdropsUser();
                            $DaoUser->iMissionId = $Dao->iId;
                            $DaoUser->iMemberId = $DaoMember->iId;
                            $DaoUser->iGetBonus = $Dao->iBonus;
                            $DaoUser->vValue = $vAccount;
                            $DaoUser->iCreateTime = $DaoUser->iUpdateTime = time();
                            $DaoUser->iStatus = 0;
                            $DaoUser->save();
                        }
                    } else {
                        $mapUser['iMissionId'] = $Dao->iId;
                        $mapUser['vValue'] = session( 'shop_member.vAccount' );
                        $DaoUser = ModCoinAirdropsUser::where( $mapUser )->first();
                        if ($DaoUser) {
                            $DaoUser->iStatus = 1;
                            $DaoUser->iUpdateTime = time();
                            $DaoUser->save();
                        }
                    }
                    break;
                default:
                    break;
            }
        }
    }
}
