<?php

namespace App\Http\Middleware;

use App\SysCenterMember;
use App\SysMember;
use Closure;

class CheckCoinAirdrops
{
    public function __construct ()
    {
    }

    public function handle ( $request, Closure $next )
    {
        if (isset( $request->userid )) {
            $Dao = SysCenterMember::where( 'iUserId', $request->userid )->first();
            if ($Dao) {
                session()->put( 'member.recommend_user_id', $request->userid );

                return redirect()->guest( 'register' );
            }

        }
        return redirect()->guest( '' );
    }
}
