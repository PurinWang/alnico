<?php

namespace App\Http\Middleware;

use App\ModMuseum;
use Closure;

class CheckLang
{
    public function __construct ()
    {
    }

    public function handle ( $request, Closure $next )
    {
        if ($request->lang) {
            app()->setLocale( $request->lang );
            session()->put( 'locale', $request->lang );

            return redirect( '' );
        } else {
            if ( !session()->has( 'locale' )) {
                $DaoLang = ModMuseum::find( '201' );
                app()->setLocale( $DaoLang->vMuseumCode );
                session()->put( 'locale', $DaoLang->vMuseumCode );
            } else {
                if ( !in_array( session( 'locale' ), config( '_config.lang' ) )) {
                    app()->setLocale( 'zh-cn' );
                    session()->put( 'locale', 'zh-cn' );
                } else {
                    app()->setLocale( session( 'locale' ) );
                }

            }
        }

        return $next ( $request );
    }
}
