<?php

namespace App\Http\Middleware;

use App\SysCenterMemberInfo;
use Closure;

class CheckMemberInfo
{
    public function __construct ()
    {
    }

    public function handle ( $request, Closure $next )
    {
        $Dao = SysCenterMemberInfo::find( session( 'member.iId' ) );
        if (
            $Dao->vUserName == ""
            || $Dao->vUserID == ""
            || $Dao->iUserBirthday == ""
            || $Dao->vUserEmail == ""
            || $Dao->vUserContact == ""
            || $Dao->vUserAddress == ""
            || $Dao->vUserAddress == ""
            || $Dao->vCountryCode == ""
            || $Dao->vCountry == ""
            || $Dao->vERC == ""
            || $Dao->vPassportF == ""
            || $Dao->vPassportB == ""
            || $Dao->vPassportT == ""
        ) {
            session()->put( 'ico.message', 'Incomplete member information' );

            return redirect( 'member' );
        }

        return $next ( $request );
    }
}
