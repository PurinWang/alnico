<?php

namespace App\Http\Middleware;

use Closure;

class CheckMallLogin
{
    public function __construct ()
    {
    }

    public function handle ( $request, Closure $next )
    {
        if ( !session()->has( 'shop_member' )) {
            if ($_SERVER ['REQUEST_METHOD'] == "POST") {
                $this->rtndata ['status'] = 0;
                $this->rtndata ['message'] = "請先登入";

                return response()->json( $this->rtndata );
            }
            session()->put( 'rtnurl', url( $_SERVER ['REQUEST_URI'] )."#p2" );

            return redirect()->guest( 'login' );
        }

        return $next ( $request );
    }
}
