<?php

namespace App\Http\Middleware;

use Closure;

class CheckICODateTime
{
    public function __construct ()
    {
    }

    public function handle ( $request, Closure $next )
    {
        if (in_array( session( 'member.vAccount' ), config( '_config.test_account' ) )) {
            return $next ( $request );
        }

        if (time() < strtotime( '2018-03-28' )) {
            session()->put( 'ico.message', 'Coming Soon' );

            return redirect()->guest( '' );
        }

        return $next ( $request );
    }
}
