<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysGroupMember extends Model
{
    public $timestamps = false;
    protected $connection = '';
    protected $table = 'sys_group_member';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
