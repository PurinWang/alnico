<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModServiceWishingPool extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_service_wishing_pool';
    protected $primaryKey = 'vWishingNum';
    public $incrementing = false;

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( 'config.center_connection' );
                break;
            default:
                $this->connection = config( 'config.mall_connection' );
        }
    }

}
