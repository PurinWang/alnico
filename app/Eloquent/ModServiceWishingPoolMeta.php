<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModServiceWishingPoolMeta extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_service_wishing_pool_meta';
    protected $primaryKey = 'iSubId';
    public $incrementing = true;

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( 'config.center_connection' );
                break;
            default:
                $this->connection = config( 'config.mall_connection' );
        }
    }

}
