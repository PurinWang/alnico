<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModActivityCoupon extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_activity_coupon';
    protected $primaryKey = 'iId';


    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }

    /*
    *
    */
    static function _doCheck ( $vCode )
    {
        $mapCoupon['vCode'] = $vCode;
        $mapCoupon['iStatus'] = 1;
        $mapCoupon['bDel'] = 0;
        $DaoCoupon = ModActivityCoupon::where( $mapCoupon )->first();
        if ( !$DaoCoupon) {
            return false;
        }
        if ($DaoCoupon->iStartTime > time() || time() > $DaoCoupon->iEndTime) {
            return false;
        }

        switch ($DaoCoupon->iUseType) {
            case 1://不限次數
                break;
            case 2://每人限用一次
                $mapActivityCouponLog['iMemberId'] = session()->get( 'shop_member.iId' );
                $mapActivityCouponLog['iCouponId'] = $DaoCoupon->iId;
                $DaoActivityCouponLog = ModActivityCouponLog::where( $mapActivityCouponLog )->first();
                if ($DaoActivityCouponLog) {
                    return false;
                }
                break;
            case 3://一次性
                $mapActivityCouponLog['iCouponId'] = $DaoCoupon->iId;
                $DaoActivityCouponLog = ModActivityCouponLog::where( $mapActivityCouponLog )->first();
                if ($DaoActivityCouponLog) {
                    return false;
                }
                break;
        }

        return $DaoCoupon;
    }

}
