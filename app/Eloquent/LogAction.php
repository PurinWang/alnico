<?php

namespace App;

use function GuzzleHttp\default_ca_bundle;
use Illuminate\Database\Eloquent\Model;

class LogAction extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'log_action';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }

}
