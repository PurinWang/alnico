<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModKeep extends Model
{
    public $timestamps = false;
    protected $connection = '';
    protected $table = 'mod_keep';
    protected $primaryKey = 'iId';


    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
