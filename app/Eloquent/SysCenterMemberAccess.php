<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysCenterMemberAccess extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'sys_member_access';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
