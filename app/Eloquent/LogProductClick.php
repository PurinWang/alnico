<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogProductClick extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'log_product_click';


    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( 'config.center_connection' );
                break;
            default:
                $this->connection = config( 'config.mall_connection' );
        }
    }

}
