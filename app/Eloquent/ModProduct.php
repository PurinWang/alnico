<?php

namespace App;

use App\Http\Controllers\FuncController;
use Illuminate\Database\Eloquent\Model;

class ModProduct extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_product';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }

    /*
     *
     */
    public static function getProductById ( $id, $spec_id = 0 )
    {
        $map['bShow'] = 1;
        $map['bDel'] = 0;
        $product['info'] = self::join( 'mod_product_info', function( $join ) {
            $join->on( 'mod_product_info.iProductId', '=', 'mod_product.iId' );
        } )->join( 'mod_product_price', function( $join ) {
            $join->on( 'mod_product_price.iProductId', '=', 'mod_product.iId' );
        } )->where( $map )->find( $id );
        if ($product['info']) {
            $product['info']->vCategoryName = ModProductCategory::where( 'iId', $product['info']->iCategoryId )->value( 'vCategoryName' );
            $mapAttributes['iProductId'] = $id;
            $product['attributes'] = ModProductAttributes::where( $mapAttributes )->get();
            $mapSpec['iProductId'] = $id;
            $mapSpec['bDel'] = 0;
            if ($spec_id) {
                $product['spec'] = ModProductSpec::where( $mapSpec )->find( $spec_id );
            } else {
                $product['spec'] = ModProductSpec::where( $mapSpec )->first();
            }

            return $product;
        } else {
            return false;
        }


    }

    public static function getProductList ( $iProductType, $id_arr = [] )
    {
        $map['mod_product.iProductType'] = $iProductType;
        $map['mod_product.bDel'] = 0;
        $data_arr = ModProduct::join( 'mod_product_info', function( $join ) {
            $join->on( 'mod_product_info.iProductId', '=', 'mod_product.iId' );
        } )->leftJoin( 'mod_product_category', function( $join ) {
            $join->on( 'mod_product_category.iId', '=', 'mod_product.iCategoryId' );
        } )->join( 'mod_product_price', function( $join ) {
            $join->on( 'mod_product_price.iProductId', '=', 'mod_product.iId' );
        } )->where( $map );

        if ($id_arr) {
            $data_arr = $data_arr->select(
                'mod_product.*',
                'mod_product_price.*',
                'mod_product_info.*',
                'mod_product_category.vCategoryValue',
                'mod_product_category.vCategoryName' )->find( $id_arr );
        } else {
            $data_arr = $data_arr->select(
                'mod_product.*',
                'mod_product_price.*',
                'mod_product_info.*',
                'mod_product_category.vCategoryValue',
                'mod_product_category.vCategoryName' )->get();
        }

        $web = new FuncController();
        foreach ($data_arr as $key => $var) {
            $var->DT_RowId = $var->iId;
            $var->iCreateTime = date( 'Y/m/d H:i:s', $var->iCreateTime );
            //圖片
            $tmp_arr = explode( ';', $var->vImages );
            $tmp_arr = array_filter( $tmp_arr );
            $var->vImages = $tmp_arr;
            //
            $var->vCategoryNum = ( $var->iCategoryId > 0 ) ? $web->_getCategoryNum( $var->iCategoryId ) . str_pad( $var->iId, 6, 0, STR_PAD_LEFT ) : "";
        }

        return $data_arr;

    }

}
