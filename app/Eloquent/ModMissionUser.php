<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModMissionUser extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_mission_user';
    protected $primaryKey = 'iId';
    
    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
