<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LogExchangeRate extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'log_exchange_rate';
    protected $primaryKey = 'iId';


    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( 'config.center_connection' );
                break;
            default:
                $this->connection = config( 'config.mall_connection' );
        }
    }
}
