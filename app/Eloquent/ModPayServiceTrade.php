<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModPayServiceTrade extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_pay_service_trade';
    protected $primaryKey = 'vOrderNum';
    public $incrementing = false;

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }

}
