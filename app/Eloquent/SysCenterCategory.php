<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysCenterCategory extends Model {
	public $timestamps = false;
    protected $connection = 'center';
	protected $table = 'sys_category';
	protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
