<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModOrderCoupon extends Model
{
    public $timestamps = false;
    protected $connection = 'center';
    protected $table = 'mod_order_coupon';
    protected $primaryKey = 'vOrderNum';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }

}
