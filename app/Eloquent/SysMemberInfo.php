<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysMemberInfo extends Model
{
    public $timestamps = false;
    protected $connection = '';
    protected $table = 'sys_member_info';
    protected $primaryKey = 'iMemberId';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
