<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SysMenu extends Model
{
    public $timestamps = false;
    protected $connection = '';
    protected $table = 'sys_menu';
    protected $primaryKey = 'iId';

    /*
     *
     */
    public function __construct ()
    {
        switch ($this->connection) {
            case 'center':
                $this->connection = config( '_config.center_connection' );
                break;
            default:
                $this->connection = config( '_config.mall_connection' );
        }
    }
}
