<?php
return [
    "web" =>
        [
            "index" =>
                [
                    "view" => "_template_web.index"
                ],
            "member" =>
                [
                    "userinfo" =>
                        [
                            "view" => "_template_web._member.userinfo"
                        ]
                ],
            "admin" =>
                [
                    "setting" =>
                        [
                            "header" =>
                                [
                                    "view" => "_template_web._admin.setting.header",
                                    "menu_access" => 111,
                                    "menu_parent" =>
                                        [
                                            1,
                                            11
                                        ],
                                ],
                            "footer" =>
                                [
                                    "view" => "_template_web._admin.setting.footer",
                                    "menu_access" => 112,
                                    "menu_parent" =>
                                        [
                                            1,
                                            11
                                        ],
                                ],
                            "information" =>
                                [
                                    "view" => "_template_web._admin.setting.information",
                                    "menu_access" => 113,
                                    "menu_parent" =>
                                        [
                                            1,
                                            11
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web._admin.advertising.index",
                            "menu_access" => 17,
                            "menu_parent" =>
                                [
                                    1,
                                    0
                                ],
                        ],
                    "banner" =>
                        [
                            "view" => "_template_web._admin.banner.index",
                            "menu_access" => 18,
                            "menu_parent" =>
                                [
                                    1,
                                    0
                                ],
                        ],
                ],
            "intro" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.intro.banner.index",
                            "menu_access" => 70011,
                            "menu_parent" =>
                                [
                                    7001,
                                    0
                                ],
                        ],
                    "index" =>
                        [
                            "view" => "_template_web.intro.index.index",
                            "menu_access" => 70012,
                            "menu_parent" =>
                                [
                                    7001,
                                    0
                                ],
                        ],
                ],
            "museum_a01" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500013,
                            "menu_parent" =>
                                [
                                    50001,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500014,
                            "menu_parent" =>
                                [
                                    50001,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500014,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500015,
                            "menu_parent" =>
                                [
                                    50001,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500015,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500016,
                            "menu_parent" =>
                                [
                                    50001,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000171,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            500017
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000172,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            500017
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000173,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            500017
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000174,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            500017
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500018,
                            "menu_parent" =>
                                [
                                    50001,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500018,
                                    "menu_parent" =>
                                        [
                                            50001,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a02" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500023,
                            "menu_parent" =>
                                [
                                    50002,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500024,
                            "menu_parent" =>
                                [
                                    50002,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500024,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500025,
                            "menu_parent" =>
                                [
                                    50002,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500025,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500026,
                            "menu_parent" =>
                                [
                                    50002,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000271,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            500027
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000272,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            500027
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000273,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            500027
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000274,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            500027
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500028,
                            "menu_parent" =>
                                [
                                    50002,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500028,
                                    "menu_parent" =>
                                        [
                                            50002,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a03" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500033,
                            "menu_parent" =>
                                [
                                    50003,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500034,
                            "menu_parent" =>
                                [
                                    50003,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500034,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500035,
                            "menu_parent" =>
                                [
                                    50003,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500035,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500036,
                            "menu_parent" =>
                                [
                                    50003,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000371,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            500037
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000372,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            500037
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000373,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            500037
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000374,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            500037
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500038,
                            "menu_parent" =>
                                [
                                    50003,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500038,
                                    "menu_parent" =>
                                        [
                                            50003,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a04" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500043,
                            "menu_parent" =>
                                [
                                    50004,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500044,
                            "menu_parent" =>
                                [
                                    50004,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500044,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500045,
                            "menu_parent" =>
                                [
                                    50004,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500045,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500046,
                            "menu_parent" =>
                                [
                                    50004,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000471,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            500047
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000472,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            500047
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000473,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            500047
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000474,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            500047
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500048,
                            "menu_parent" =>
                                [
                                    50004,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500048,
                                    "menu_parent" =>
                                        [
                                            50004,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a05" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500053,
                            "menu_parent" =>
                                [
                                    50005,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500054,
                            "menu_parent" =>
                                [
                                    50005,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500054,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500055,
                            "menu_parent" =>
                                [
                                    50005,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500055,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500056,
                            "menu_parent" =>
                                [
                                    50005,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000571,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            500057
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000572,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            500057
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000573,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            500057
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000574,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            500057
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500058,
                            "menu_parent" =>
                                [
                                    50005,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500058,
                                    "menu_parent" =>
                                        [
                                            50005,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a06" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500063,
                            "menu_parent" =>
                                [
                                    50006,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500064,
                            "menu_parent" =>
                                [
                                    50006,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500064,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500065,
                            "menu_parent" =>
                                [
                                    50006,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500065,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500066,
                            "menu_parent" =>
                                [
                                    50006,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000671,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            500067
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000672,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            500067
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000673,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            500067
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000674,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            500067
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500068,
                            "menu_parent" =>
                                [
                                    50006,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500068,
                                    "menu_parent" =>
                                        [
                                            50006,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a07" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500073,
                            "menu_parent" =>
                                [
                                    50007,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500074,
                            "menu_parent" =>
                                [
                                    50007,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500074,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500075,
                            "menu_parent" =>
                                [
                                    50007,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500075,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500076,
                            "menu_parent" =>
                                [
                                    50007,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000771,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            500077
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000772,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            500077
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000773,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            500077
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000774,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            500077
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500078,
                            "menu_parent" =>
                                [
                                    50007,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500078,
                                    "menu_parent" =>
                                        [
                                            50007,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a08" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500083,
                            "menu_parent" =>
                                [
                                    50008,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500084,
                            "menu_parent" =>
                                [
                                    50008,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500084,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500085,
                            "menu_parent" =>
                                [
                                    50008,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500085,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500086,
                            "menu_parent" =>
                                [
                                    50008,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000871,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            500087
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000872,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            500087
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000873,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            500087
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000874,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            500087
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500088,
                            "menu_parent" =>
                                [
                                    50008,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500088,
                                    "menu_parent" =>
                                        [
                                            50008,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a09" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500093,
                            "menu_parent" =>
                                [
                                    50009,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500094,
                            "menu_parent" =>
                                [
                                    50009,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500094,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500095,
                            "menu_parent" =>
                                [
                                    50009,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500095,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500096,
                            "menu_parent" =>
                                [
                                    50009,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5000971,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            500097
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5000972,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            500097
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5000973,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            500097
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5000974,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            500097
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500098,
                            "menu_parent" =>
                                [
                                    50009,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500098,
                                    "menu_parent" =>
                                        [
                                            50009,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a10" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500103,
                            "menu_parent" =>
                                [
                                    50010,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500104,
                            "menu_parent" =>
                                [
                                    50010,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500104,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500105,
                            "menu_parent" =>
                                [
                                    50010,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500105,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500106,
                            "menu_parent" =>
                                [
                                    50010,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5001071,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            500107
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5001072,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            500107
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5001073,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            500107
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5001074,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            500107
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500108,
                            "menu_parent" =>
                                [
                                    50010,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500108,
                                    "menu_parent" =>
                                        [
                                            50010,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a11" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500113,
                            "menu_parent" =>
                                [
                                    50011,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500114,
                            "menu_parent" =>
                                [
                                    50011,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500114,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500115,
                            "menu_parent" =>
                                [
                                    50011,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500115,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500116,
                            "menu_parent" =>
                                [
                                    50011,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5001171,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            500117
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5001172,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            500117
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5001173,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            500117
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5001174,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            500117
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500118,
                            "menu_parent" =>
                                [
                                    50011,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500118,
                                    "menu_parent" =>
                                        [
                                            50011,
                                            0
                                        ],
                                ],
                        ],
                ],
            "museum_a12" =>
                [
                    "banner" =>
                        [
                            "view" => "_template_web.museum.banner.index",
                            "menu_access" => 500123,
                            "menu_parent" =>
                                [
                                    50012,
                                    0
                                ],
                        ],
                    "rank" =>
                        [
                            "view" => "_template_web.museum.rank.index",
                            "menu_access" => 500124,
                            "menu_parent" =>
                                [
                                    50012,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.rank.sub",
                                    "menu_access" => 500124,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            0
                                        ],
                                ],
                        ],
                    "recommend" =>
                        [
                            "view" => "_template_web.museum.recommend.index",
                            "menu_access" => 500125,
                            "menu_parent" =>
                                [
                                    50012,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.recommend.sub",
                                    "menu_access" => 500125,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            0
                                        ],
                                ],
                        ],
                    "advertising" =>
                        [
                            "view" => "_template_web.museum.advertising.index",
                            "menu_access" => 500126,
                            "menu_parent" =>
                                [
                                    50012,
                                    0
                                ],
                        ],
                    "advertising2" =>
                        [
                            "banner1" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner1",
                                    "menu_access" => 5001271,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            500127
                                        ],
                                ],
                            "banner2" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner2",
                                    "menu_access" => 5001272,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            500127
                                        ],
                                ],
                            "banner3" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner3",
                                    "menu_access" => 5001273,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            500127
                                        ],
                                ],
                            "banner4" =>
                                [
                                    "view" => "_template_web.museum.advertising2.banner4",
                                    "menu_access" => 5001274,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            500127
                                        ],
                                ],
                        ],
                    "category_banner" =>
                        [
                            "view" => "_template_web.museum.category_banner.index",
                            "menu_access" => 500128,
                            "menu_parent" =>
                                [
                                    50012,
                                    0
                                ],
                            "sub" =>
                                [
                                    "view" => "_template_web.museum.category_banner.sub",
                                    "menu_access" => 500128,
                                    "menu_parent" =>
                                        [
                                            50012,
                                            0
                                        ],
                                ],
                        ],
                ],
        ],
];